#include "skel.h"
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <features.h>
#include <sys/types.h>
#include <linux/if_ether.h>
#define BUFF_LEN 45
#define INITIAL_SIZE 1000
#define ARP_IN_SIZE 4


struct route_table *sort_table(struct route_table *rtable, int rtable_size);
void merge(struct route_table *rtable, int left, int middle, int right);
void mergeSort(struct route_table *rtable, int left, int right);
void parse_function();
uint16_t ip_checksum(void* vdata,size_t length);
struct route_table *get_best_route(int l, int r, uint32_t dest_ip);
struct arp_entry *get_arp_entry(__u32 ip);
void add_to_arp_table(uint32_t ip, uint8_t mac[6], int count);
//vectorul tabelei de rutare
struct route_table *rtable;
int rtable_size;

//vectorul tabelei arp
struct arp_entry *arp_table;
int arp_table_len;

//structura unui element din tabela de rutare
struct route_table {
	uint32_t prefix;
	uint32_t next_hop;
	uint32_t mask;
	int interface;
} __attribute__((packed));

//structura unui element din tabela arp
typedef struct arp_entry {
	uint32_t ip;
	uint8_t mac[6];
} arp_entry;

//structura headerului arp
typedef struct arp_struc {
	unsigned short int ar_hrd;		
    unsigned short int ar_pro;
    unsigned char ar_hln;
    unsigned char ar_pln;
    unsigned short int ar_op;
	uint8_t arp_sha[ETH_ALEN];
	uint8_t arp_spa[4];	
	uint8_t arp_tha[ETH_ALEN];	
	uint8_t arp_tpa[4];	
} arp_struc;

//lista pentru elementele din coada
typedef struct list
{
	packet *pkg;
	struct list *next;
} list;

//adaugarea unui pachet in lista
list *add_elem(packet *pkg, list *l) {
	list *node = malloc(sizeof(list));
	node->pkg = pkg;
	node->next = l;

	return node;
}

//cautarea pachetului in coada
packet *search_pkg(list *l, uint32_t ip) {
  list *tmp = l;

  while (tmp != NULL) {
    uint32_t next = get_best_route(0, rtable_size - 1, ((struct iphdr *)(tmp->pkg->payload + sizeof(struct ether_header)))->daddr)->next_hop;
    if (ip == next) {
      return tmp->pkg;
    }
    tmp = tmp->next;
  }
  return NULL;
}

//stergerea pachetului din coada
void delete_pkg(list *l, packet *pkg) {

	list *tmp = l;
	list *prev = NULL;

	while (tmp && tmp->pkg != pkg) {
		prev = tmp;
		tmp = tmp->next;
	}

	if (!prev && l) {
		l = l->next;
	}
	else if (!tmp) {
		prev->next = NULL; 
	}
	else {
		prev->next = tmp->next;
	}

}

//functia de calcul a checksum-ului
//preluata din laborator
uint16_t ip_checksum(void* vdata,size_t length) {
	// Cast the data pointer to one that can be indexed.
	char* data=(char*)vdata;

	// Initialise the accumulator.
	uint64_t acc=0xffff;

	// Handle any partial block at the start of the data.
	unsigned int offset=((uintptr_t)data)&3;
	if (offset) {
		size_t count=4-offset;
		if (count>length) count=length;
		uint32_t word=0;
		memcpy(offset+(char*)&word,data,count);
		acc+=ntohl(word);
		data+=count;
		length-=count;
	}

	// Handle any complete 32-bit blocks.
	char* data_end=data+(length&~3);
	while (data!=data_end) {
		uint32_t word;
		memcpy(&word,data,4);
		acc+=ntohl(word);
		data+=4;
	}
	length&=3;

	// Handle any partial block at the end of the data.
	if (length) {
		uint32_t word=0;
		memcpy(&word,data,length);
		acc+=ntohl(word);
	}

	// Handle deferred carries.
	acc=(acc&0xffffffff)+(acc>>32);
	while (acc>>16) {
		acc=(acc&0xffff)+(acc>>16);
	}

	// If the data began at an odd byte address
	// then reverse the byte order to compensate.
	if (offset&1) {
		acc=((acc&0xff00)>>8)|((acc&0x00ff)<<8);
	}

	// Return the checksum in network byte order.
	return htons(~acc);
}
//identificarea adresei potrivite pentru next hop,
//implementata prin binary search
struct route_table *get_best_route(int l, int r, uint32_t dest_ip) { 
	
	int i = 0;
	struct route_table *best_match = NULL;
    if (r >= l) { 
        int mid = l + (r - l) / 2; 
       	//verificarea in centrul vectorului
        if ((dest_ip & rtable[mid].mask) == rtable[mid].prefix){
  			i = mid;

  			//identificarea celui mai lung prefix
        	while (rtable[i].prefix == rtable[mid].prefix) {
        		i--;
        	}
            return (rtable + i + 1); 
        }
  
        //verificarea in partea stanga a vectorului
        if ((dest_ip & rtable[mid].mask) < rtable[mid].prefix) 
            return get_best_route(l, mid - 1, dest_ip); 
  
         //verificarea in partea dreapta a vectorului
        return get_best_route(mid + 1, r, dest_ip); 
    } 
  
    return best_match; 
} 

//identificarea adresei potrivite din arp table
struct arp_entry *get_arp_entry(__u32 ip) {
    /* TODO 2: Implement */
    struct arp_entry *best_match = NULL;
    for (int i = 0; i < arp_table_len; i++) {
        if (arp_table[i].ip == ip) {
            best_match = &arp_table[i];
        }
    }
    return best_match;
}

//functie utilizata in sortarea tabelei de rutare
void merge(struct route_table *rtable, int left, int middle, int right) { 
    
    int i, j, k; 
    int size1 = middle - left + 1; 
    int size2 =  right - middle; 

    struct route_table L[size1], R[size2]; 

    for (i = 0; i < size1; i++) {
        L[i] = rtable[left + i]; 
    }

    for (j = 0; j < size2; j++) {
        R[j] = rtable[middle + 1 + j]; 
    }
  
    i = 0;
    j = 0;
    k = left; 

    while (i < size1 && j < size2) { 
        if (L[i].prefix < R[j].prefix) { 
           
            rtable[k] = L[i]; 
            i++; 
        } 
        else if (L[i].prefix == R[j].prefix) {
        	if (L[i].mask >= R[j].mask) {
        		rtable[k] = L[i]; 
            	i++; 
        	}
        	else {
        		rtable[k] = R[j]; 
            	j++; 
        	}

        }
        else { 

            rtable[k] = R[j]; 
            j++; 
        } 
        k++; 
    } 

    while (i < size1) { 
        rtable[k] = L[i]; 
        i++; 
        k++; 
    } 
  
    while (j < size2) { 
        rtable[k] = R[j]; 
        j++; 
        k++; 
    } 
} 

//algoritmul mergeSort prin care sortez tabela de rutare  
void mergeSort(struct route_table *rtable, int left, int right) { 
    
    if (left < right) { 

        int middle = left + (right - left)/2; 
  
        mergeSort(rtable, left, middle); 
        mergeSort(rtable, middle + 1, right); 
  		merge(rtable, left, middle, right); 
    } 
} 

//functia de parsare a tabelei de rutare
void parse_function() {

	int size = INITIAL_SIZE;
	rtable_size = 0;
	int count = 0, i = 0, j = 0;
	rtable = malloc(sizeof(struct route_table) * size);

	FILE *fp;
	char buffer[BUFF_LEN];
	fp = fopen("rtable.txt", "r");

	while (1) {
		
		if (fgets(buffer, BUFF_LEN, fp) != NULL) {
			
			count++;
			buffer[strlen(buffer) - 1] = '\0';
			
			if (count > size) {
				size *= 2;
				rtable = realloc(rtable, sizeof(struct route_table) * size);
			}
			
			char *token = strtok(buffer, " "); 
			char *prefix = NULL;
			char *next = NULL;
			char *mask = NULL;
			char *inter = NULL;

			for (j = 0; j < 4; j++) {
				 
				switch (j) {

					case 0:
						prefix = token;
						break;
					case 1:
						next = token;
						break;
					case 2:
						mask = token;
						break;
					case 3:
						inter = token;
						break;
				}

		        token = strtok(NULL, " "); 
		    } 

		    rtable[i].prefix = inet_addr(prefix);
			rtable[i].next_hop = inet_addr(next);
			rtable[i].mask = inet_addr(mask);
			rtable[i].interface = atoi(inter);
			
			rtable_size = count;
			i++;
		}

		else break;
	}
	//sortarea tabelei
	mergeSort(rtable, 0, rtable_size - 1);
	fclose(fp);
}

//functie utilizata in prelucrarea unei tabele arp statice
void populate_arp_table (struct arp_entry* arp_table) {
	FILE *fp;
	char buffer[BUFF_LEN];
	fp = fopen("copie_arp.txt", "r");
	int i = 0, j = 0;
	while (1) {
		
		if (fgets(buffer, BUFF_LEN, fp) != NULL) {
			buffer[strlen(buffer) - 1] = '\0';

			char *token = strtok(buffer, " "); 
			char *ip = NULL;
			char *mac = NULL;
		
			for (j = 0; j < 2; j++) {
				 
				switch (j) {

					case 0:
						ip = token;
						break;
					case 1:
						mac = token;
				}

		        token = strtok(NULL, " "); 
		    } 
		    arp_table[i].ip = inet_addr(ip);
		    hwaddr_aton(mac, arp_table[i].mac);
		    i++;
		}

		else break;
	}

}

//functie de prelucrare a unei tabele arp dinamice
void add_to_arp_table(uint32_t ip, uint8_t mac[6], int count) {
	arp_table_len++;
	arp_table = realloc(arp_table, sizeof(struct arp_entry) * arp_table_len);
	
	arp_table[count - 1].ip = ip;
	memcpy(arp_table[count - 1].mac, mac, 6);
}

int main(int argc, char *argv[]) {
	setvbuf(stdout, NULL, _IONBF, 0);
	packet m;
    arp_table_len = 0;
	int rc;
	init();
	parse_function();	
	
	list *l = NULL;

	//variabila utilizata in realocarea arp_table
	int count = 0;

	while (1) {

		rc = get_packet(&m);
		DIE(rc < 0, "get_message");
		// /* Students will write code here 

		//initializarea headerului ethernet
		struct ether_header *eth_hdr = (struct ether_header *)m.payload;
    	//verificarea daca headerul ethernet e de tip IP
		if (ntohs(eth_hdr->ether_type) == ETHERTYPE_IP) {
			//extragerea headerului IP
			struct iphdr *ip_hdr = (struct iphdr *)(m.payload + sizeof(struct ether_header));
			
			//verificarea checksum-ului 
			uint16_t temporary_check = ip_hdr->check;
			ip_hdr->check = 0;
			ip_hdr->check = ip_checksum(ip_hdr, sizeof(struct iphdr));

			//verificarea ttl-ului
	    	//trimit un raspuns de Time Exceeded inapoi
			if (ip_hdr->ttl <= 1) {

				//crearea unui pachet
				packet icmp_ex;
				icmp_ex.interface = m.interface;
				icmp_ex.len = sizeof(struct ether_header) + sizeof(struct iphdr) +
					sizeof(struct icmphdr);

				//crearea headerelor ethernet, ip si icmp
				struct ether_header *ether_icm = (struct ether_header *)icmp_ex.payload;
				struct iphdr *ip_icm = (struct iphdr *)(icmp_ex.payload + sizeof(struct ether_header));
				struct icmphdr *icmp_icm = (struct icmphdr *)(icmp_ex.payload + sizeof(struct ether_header) +
					sizeof(struct iphdr));

				ether_icm->ether_type = htons(ETHERTYPE_IP);

				//completarea structurii unui ip
				memcpy(ip_icm, ip_hdr, ip_hdr->tot_len);
				ip_icm->daddr = ip_hdr->saddr;
				ip_icm->saddr = inet_addr(get_interface_ip(m.interface));
				ip_icm->id = 0;
				ip_icm->ttl = 64;
				ip_icm->version = 4;
				ip_icm->protocol = 1;
				ip_icm->tot_len = htons(sizeof(struct iphdr) + sizeof(struct icmphdr));
				ip_icm->check = 0;
				ip_icm->check = ip_checksum(ip_icm, sizeof(struct iphdr));

				//completarea unui icmp de tip Time Exceeded
				icmp_icm->type = ICMP_TIME_EXCEEDED;
				icmp_icm->code = 0;
				icmp_icm->un.echo.sequence = 0;
				icmp_icm->un.echo.id = 0;
				
				//calcularea checksum-ului
				icmp_icm->checksum = 0;
				icmp_icm->checksum = ip_checksum(icmp_icm, sizeof(struct icmphdr));

				//in sursa pun adresa mea si in destinatie adresa sursa din ip-ul primit
				memcpy(ether_icm->ether_shost, eth_hdr->ether_dhost, 6);
				memcpy(ether_icm->ether_dhost, eth_hdr->ether_shost, sizeof(eth_hdr->ether_shost));

			    send_packet(icmp_ex.interface, &icmp_ex);
	        	
	        	continue;
	    	}
	    	
			if (temporary_check != ip_hdr->check) {
	        		continue;
	    		}

	        // verificarea protocolului 
	        if (ip_hdr->protocol == 1) {

				struct icmphdr *icmp_hdr = (struct icmphdr*)(m.payload + sizeof(struct ether_header) + 
					sizeof(struct iphdr));
				
				//verificarea protocolului echo request care e destinat adresei mele
				if (icmp_hdr->type == ICMP_ECHO && (ip_hdr->daddr == (inet_addr (get_interface_ip(m.interface))))) {

					//raspund inapoi cu un echo reply
					uint16_t temporary_check = icmp_hdr->checksum;
					//calculez checksum-ul
					icmp_hdr->checksum = 0;
					icmp_hdr->checksum = ip_checksum(icmp_hdr, sizeof(struct icmphdr));

					//creez un pachet nou prin care sa raspund
					packet icmp_ex;
					icmp_ex.interface = m.interface;
					icmp_ex.len = sizeof(struct ether_header) + sizeof(struct iphdr) +
						sizeof(struct icmphdr);

					//crearea headerelor ethernet, ip si icmp
					struct ether_header *ether_icm = (struct ether_header *)icmp_ex.payload;
					struct iphdr *ip_icm = (struct iphdr *)(icmp_ex.payload + sizeof(struct ether_header));
					struct icmphdr *icmp_icm = (struct icmphdr *)(icmp_ex.payload + sizeof(struct ether_header) +
						sizeof(struct iphdr));

					ether_icm->ether_type = htons(ETHERTYPE_IP);

					//completarea structurii unui ip
					memcpy(ip_icm, ip_hdr, ip_hdr->tot_len);
					ip_icm->daddr = ip_hdr->saddr;
					ip_icm->saddr = inet_addr(get_interface_ip(m.interface));
					ip_icm->id = 0;
					ip_icm->ttl = 64;
					ip_icm->version = 4;
					ip_icm->protocol = 1;
					ip_icm->tot_len = htons(sizeof(struct iphdr) + sizeof(struct icmphdr));
					ip_icm->check = 0;
					ip_icm->check = ip_checksum(ip_icm, sizeof(struct iphdr));

					//completarea unui icmp de tip Echo Reply
					icmp_icm->type = ICMP_ECHOREPLY;
					icmp_icm->code = 0;
					icmp_icm->un.echo.sequence = 0;
					icmp_icm->un.echo.id = 0;

					//calcularea checksum-ului
					icmp_icm->checksum = 0;
					icmp_icm->checksum = ip_checksum(icmp_icm, sizeof(struct icmphdr));

					//completarea headerul ethernet
					memcpy(ether_icm->ether_shost, eth_hdr->ether_dhost, 6);
					memcpy(ether_icm->ether_dhost, eth_hdr->ether_shost, sizeof(eth_hdr->ether_shost));

				    send_packet(icmp_ex.interface, &icmp_ex);


					if (temporary_check != icmp_hdr->checksum) {
        				continue;
    				}

    				icmp_hdr->checksum = 0;
					icmp_hdr->checksum = ip_checksum(icmp_hdr, sizeof(struct icmphdr));
				}
				//forwardarea unui icmp care nu e la adresa mea
				else {
	
					struct route_table *next = get_best_route(0, rtable_size - 1, ip_hdr->daddr);
					//verificarea existentei unei astfel de adrese
					if (next == NULL) {

						//transmiterea unui icmp de tip destination unreachable
						packet icmp_ex;
						icmp_ex.interface = m.interface;
						icmp_ex.len = sizeof(struct ether_header) + sizeof(struct iphdr) +
							sizeof(struct icmphdr);

						//crearea headerelor ethernet, ip si icmp
						struct ether_header *ether_icm = (struct ether_header *)icmp_ex.payload;
						struct iphdr *ip_icm = (struct iphdr *)(icmp_ex.payload + sizeof(struct ether_header));
						struct icmphdr *icmp_icm = (struct icmphdr *)(icmp_ex.payload + sizeof(struct ether_header) +
							sizeof(struct iphdr));

						ether_icm->ether_type = htons(ETHERTYPE_IP);

						//completarea structurii unui ip
						memcpy(ip_icm, ip_hdr, ip_hdr->tot_len);
						ip_icm->daddr = ip_hdr->saddr;
						ip_icm->saddr = inet_addr(get_interface_ip(m.interface));
						ip_icm->id = 0;
						ip_icm->ttl = 64;
						ip_icm->version = 4;
						ip_icm->protocol = 1;
						ip_icm->tot_len = htons(sizeof(struct iphdr) + sizeof(struct icmphdr));
						ip_icm->check = 0;
						ip_icm->check = ip_checksum(ip_icm, sizeof(struct iphdr));

						//completarea unui icmp de tip destination unreachable
						icmp_icm->type = ICMP_UNREACH;
						icmp_icm->code = 0;
						icmp_icm->un.echo.sequence = 0;
						icmp_icm->un.echo.id = 0;

						//calcularea checksum-ului
						icmp_icm->checksum = 0;
						icmp_icm->checksum = ip_checksum(icmp_icm, sizeof(struct icmphdr));

						//trebuie sa completez headerul ethernet
						memcpy(ether_icm->ether_shost, eth_hdr->ether_dhost, 6);
						memcpy(ether_icm->ether_dhost, eth_hdr->ether_shost, sizeof(eth_hdr->ether_shost));
					    
					    send_packet(icmp_ex.interface, &icmp_ex);
						continue;
					}
					struct arp_entry *arp = get_arp_entry(next->next_hop);

					//verificarea existentei adresei in tabela arp
			        if (arp == NULL) {
			        	packet *aux_pack = malloc(sizeof(packet));
			        	memcpy(aux_pack, &m, sizeof(packet));
			        	
			        	//adaug pachetul in coada
			        	l = add_elem(aux_pack, l);
			        	
			        	//creez un pachet nou
			        	packet req_pack;
			        	req_pack.interface = m.interface;
						req_pack.len = sizeof(struct ether_header) + 
							sizeof(struct arp_struc);
						
						//instantiez headerele ethernet si arp
						struct ether_header *pack_eth = (struct ether_header *)req_pack.payload;
						struct arp_struc  *arp_req = (struct arp_struc*)(req_pack.payload + 
							sizeof(struct ether_header));
						
						//atribui campurilor din arp valoarea potrivita
						arp_req->ar_hrd = htons(1);
						arp_req->ar_pro = htons(2048);
						arp_req->ar_hln = 6;
						arp_req->ar_pln = 4;
						arp_req->ar_op = ntohs(ARPOP_REQUEST);
						
						//setez adresa sursei hardware cu adresa mac a rutarului
						get_interface_mac(m.interface, arp_req->arp_sha);
						uint8_t my_interface_ip = inet_addr(get_interface_ip(m.interface));
						
						//setez adresa ip a sursei cu adresa ip a rutarului
						memcpy(arp_req->arp_spa, &my_interface_ip, sizeof(my_interface_ip));
						
						//setez adresa hard a targetului  
						hwaddr_aton("00:00:00:00:00:00", arp_req->arp_tha);
						uint32_t next_interface = next->next_hop;
						
						//setez adresa ip a targetului cu adresa 
						memcpy(arp_req->arp_tpa, &next_interface, sizeof(next_interface));
						char *broadcast = "ff:ff:ff:ff:ff:ff";
						
						hwaddr_aton(broadcast, pack_eth->ether_dhost);
						uint8_t mac[6];
						get_interface_mac(m.interface, mac);
						memcpy(pack_eth->ether_shost,  &mac, sizeof(mac));
						pack_eth->ether_type = htons(ETHERTYPE_ARP);

						send_packet(req_pack.interface, &req_pack);

			        	continue;
			        }
			        get_interface_mac(next->interface, eth_hdr->ether_shost);
			        memcpy(eth_hdr->ether_dhost, arp->mac, sizeof(arp->mac));
			       
			        //decrementarea ttl-ului 
		    		ip_hdr->ttl--;

		    		//recalcularea checksum-ului
			        ip_hdr->check = 0;
			        ip_hdr->check = ip_checksum(ip_hdr, sizeof(struct iphdr));	

					send_packet(next->interface, &m);
				}

			}

			//forwardarea unui altfel de pachet
			else {
				//Caut intrarea cea mai specifica din tabela de rutar
				struct route_table *next = get_best_route(0, rtable_size - 1, ip_hdr->daddr);
				if (next == NULL) {
					packet icmp_ex;
					icmp_ex.interface = m.interface;
					icmp_ex.len = sizeof(struct ether_header) + sizeof(struct iphdr) +
						sizeof(struct icmphdr);

					//crearea headerelor ethernet, ip si icmp
					struct ether_header *ether_icm = (struct ether_header *)icmp_ex.payload;
					struct iphdr *ip_icm = (struct iphdr *)(icmp_ex.payload + sizeof(struct ether_header));
					struct icmphdr *icmp_icm = (struct icmphdr *)(icmp_ex.payload + sizeof(struct ether_header) +
						sizeof(struct iphdr));

					ether_icm->ether_type = htons(ETHERTYPE_IP);

					//completarea structurii unui ip
					memcpy(ip_icm, ip_hdr, ip_hdr->tot_len);
					ip_icm->daddr = ip_hdr->saddr;
					ip_icm->saddr = inet_addr(get_interface_ip(m.interface));
					ip_icm->id = 0;
					ip_icm->ttl = 64;
					ip_icm->version = 4;
					ip_icm->protocol = 1;
					ip_icm->tot_len = htons(sizeof(struct iphdr) + sizeof(struct icmphdr));
					ip_icm->check = 0;
					ip_icm->check = ip_checksum(ip_icm, sizeof(struct iphdr));

					//completarea unui icmp de tip destination unreachable
					icmp_icm->type = ICMP_UNREACH;
					icmp_icm->code = 0;
					icmp_icm->un.echo.sequence = 0;
					icmp_icm->un.echo.id = 0;

					//calcularea checksum-ului
					icmp_icm->checksum = 0;
					icmp_icm->checksum = ip_checksum(icmp_icm, sizeof(struct icmphdr));

					//trebuie sa completez headerul ethernet
					memcpy(ether_icm->ether_shost, eth_hdr->ether_dhost, 6);
			
					memcpy(ether_icm->ether_dhost, eth_hdr->ether_shost, sizeof(eth_hdr->ether_shost));
				    send_packet(icmp_ex.interface, &icmp_ex);
					continue;
				}

				struct arp_entry *arp = get_arp_entry(next->next_hop);
		       
		        if (arp == NULL) {

        			  	// adaug pachetul in coada
			        	packet *aux_pack = malloc(sizeof(packet));
			        	memcpy(aux_pack, &m, sizeof(packet));
			        	l = add_elem(aux_pack, l);

			        	struct iphdr *ip_hdr = (struct iphdr *)(aux_pack->payload + sizeof(struct ether_header)); 

			        	//creez un pachet
			        	packet req_pack;
			        	req_pack.interface = next->interface;
						req_pack.len = sizeof(struct ether_header) + sizeof(struct arp_struc);


						//extrag headerele
						struct ether_header *pack_eth = (struct ether_header *)req_pack.payload;
						struct arp_struc  *arp_req = (struct arp_struc *)(req_pack.payload + 
							sizeof(struct ether_header));


						//completez arp
						arp_req->ar_hrd = htons(1);
						arp_req->ar_pro = htons(2048);
						arp_req->ar_hln = 6;
						arp_req->ar_pln = 4;
						arp_req->ar_op = htons(ARPOP_REQUEST);

						// setez adresele din arp
						get_interface_mac(m.interface, arp_req->arp_sha);
						uint32_t my_interface_ip = inet_addr(get_interface_ip(m.interface));
						memcpy(arp_req->arp_spa, &my_interface_ip, sizeof(my_interface_ip));
						
						hwaddr_aton("00:00:00:00:00:00", arp_req->arp_tha);
						uint32_t next_interface = next->next_hop;
						memcpy(arp_req->arp_tpa, &next_interface, sizeof(next_interface));

						//setez headerul ethernet
						char *broadcast = "ff:ff:ff:ff:ff:ff";
						hwaddr_aton(broadcast, pack_eth->ether_dhost);
						
						get_interface_mac(m.interface, pack_eth->ether_shost);

			        	pack_eth->ether_type = htons(ETHERTYPE_ARP);

			        	// trimit pachetul
			        	send_packet(next->interface, &req_pack);
			        	continue;
		        }
		        get_interface_mac(next->interface, eth_hdr->ether_shost);
		        memcpy(eth_hdr->ether_dhost, arp->mac, sizeof(arp->mac));
	    		
	    		ip_hdr->ttl--;
		        ip_hdr->check = 0;
		        ip_hdr->check = ip_checksum(ip_hdr, sizeof(struct iphdr));	

				send_packet(next->interface, &m);

			}

		}
		//verificarea daca pachetul e de tip arp
		else if (ntohs(eth_hdr->ether_type) == ETHERTYPE_ARP) {
			//extragerea headerului arp
			struct arp_struc  *arp_hdr = (struct arp_struc*)(m.payload + sizeof(struct ether_header));
			//verificarea daca e un arp request
			if (ntohs(arp_hdr->ar_op) == ARPOP_REQUEST) {
				//raspunde cu arp reply pentru adresa de mac potrivita.
				u_char aux[6];
				memcpy(arp_hdr->arp_spa, arp_hdr->arp_tpa, sizeof( arp_hdr->arp_tpa));
				memcpy(arp_hdr->arp_tha, arp_hdr->arp_sha, sizeof( arp_hdr->arp_sha));
				get_interface_mac(m.interface, arp_hdr->arp_sha);
				
				//setarea drept Arp Reply
				arp_hdr->ar_op = ntohs(ARPOP_REPLY);
				//actualizarea adreselor
				memcpy(aux, eth_hdr->ether_dhost, sizeof(eth_hdr->ether_dhost));
				memcpy(eth_hdr->ether_dhost, eth_hdr->ether_shost, sizeof(eth_hdr->ether_shost));
				memcpy(eth_hdr->ether_shost, arp_hdr->arp_sha, sizeof(arp_hdr->arp_sha));
				send_packet(m.interface, &m);

        	}
        	// verificarea daca e un arp reply
        	else if (ntohs(arp_hdr->ar_op) == ARPOP_REPLY) {
        		struct arp_struc *arp_hdr = (struct arp_struc*)(m.payload + sizeof(struct ether_header));
        		count++;
        		//adaug in tabela de rutare
        		add_to_arp_table(*((uint32_t *)arp_hdr->arp_spa), arp_hdr->arp_sha, count);

        		packet *new_pack = search_pkg(l, *((uint32_t *)arp_hdr->arp_spa));
        		
        		//verificarea daca exista pachetul potrivit in asteptare
        		if (new_pack != NULL) {

        			struct ether_header *eth_hdr = (struct ether_header *)new_pack->payload;
        			struct iphdr *ip_hdr = (struct iphdr *)(new_pack->payload + sizeof(struct ether_header)); 

        			struct route_table *next = get_best_route(0, rtable_size - 1, ip_hdr->daddr);
					struct arp_entry *arp = get_arp_entry(next->next_hop);

					get_interface_mac(next->interface, eth_hdr->ether_shost);
			        memcpy(eth_hdr->ether_dhost, arp->mac, sizeof(arp->mac));
		    		
		    		ip_hdr->ttl--;
			        ip_hdr->check = 0;
			        ip_hdr->check = ip_checksum(ip_hdr, sizeof(struct iphdr));	

        			send_packet(new_pack->interface, new_pack);
        		}
        	}

		}
	}
}