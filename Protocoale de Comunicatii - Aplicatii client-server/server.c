#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>

#define BUFLEN 1800
#define MAX_CLIENTS 100
#define TOPIC_LEN 50
#define MESSAGE_LEN 1500

// structura unui client TCP
typedef struct subscriber {
	char id[BUFLEN];
	char topics[BUFLEN][TOPIC_LEN];  // vectorul cu teme
	int socketfd;  // file descriptorul clientului
	int num_topics;  // numarul de teme la care e abonat
	int sf[BUFLEN];  // sf-urile specifice temelor
	int active;  // flag care indica daca e online clientul
	char waiting[BUFLEN][MESSAGE_LEN];  // vector cu mesaje in asteptare
	int waiting_top;  // numarul de mesaje in asteptare
}TSubscriber;

// Vectorul de clienti
TSubscriber clients[MAX_CLIENTS];

// structura unui mesaj UDP
typedef struct mess {
    char topic[TOPIC_LEN];
    char type;
    char message[MESSAGE_LEN];
}TMessage;

// functie de identificare a maximului
int max(int a, int b) {
	if (a > b)
		return a;
	else return b;
}

// functie a modului de rulare
void usage(char *file) {
	fprintf(stderr, "Usage: %s server_port\n", file);
	exit(0);
}

// functie de parsare a mesajelor UDP
void messageUDP(char *in, char *out, struct sockaddr_in client) {
	memset(out, 0, sizeof(out));
	TMessage m = *((TMessage *)in);
	/*are loc identificarea tipului de mesaj
	si printarea acestuia in formatul specificat.*/ 
	if (m.type == 0) {
        uint32_t n;
        memcpy(&n, m.message + 1, 4);
        n = ntohl(n);
        if (m.message[0] == 1) {
            sprintf(out, "%s:%d - %s - INT - -%d",
            	inet_ntoa(client.sin_addr), ntohs(client.sin_port), m.topic, n);
        }
        else {
            sprintf(out, "%s:%d - %s - INT - %d",
            	inet_ntoa(client.sin_addr), ntohs(client.sin_port), m.topic, n);
        }
    }
    if (m.type == 1) {
        uint16_t n;
        memcpy(&n, m.message, 4);
        n = ntohs(n);
        float cons = n / 100;
        sprintf(out, "%s:%d - %s - SHORT-REAL - %0.2f",
        	inet_ntoa(client.sin_addr), ntohs(client.sin_port), m.topic, cons);
    }
    if (m.type == 2) {
        uint32_t n;
        uint8_t sign;
        memcpy(&n, m.message + 1, 4);
        memcpy(&sign, m.message + 5, 1);
        n = ntohl(n);
        float cons = pow(10, sign);
        if (m.message[0] == 1) {
            sprintf(out, "%s:%d - %s - FLOAT - -%0.4f",
            	inet_ntoa(client.sin_addr),
            	ntohs(client.sin_port), m.topic, n / cons);
        }
        else {
            sprintf(out, "%s:%d - %s - FLOAT - %0.4f",
            	inet_ntoa(client.sin_addr),
            	ntohs(client.sin_port), m.topic, n / cons);
        }
    }
    if (m.type == 3)
        sprintf(out, "%s:%d - %s - STRING - %s",
        	inet_ntoa(client.sin_addr),
        	ntohs(client.sin_port), m.topic, m.message);
}

int main(int argc, char *argv[]) {
	int sockUDP, sockTCP, newsockfd, portno;
	char buffer[BUFLEN];
	char message_buf[BUFLEN];
	struct sockaddr_in serv_addrTCP, cli_addrTCP;
	struct sockaddr_in serv_addrUDP;
	int n, i, retTCP, retUDP;
	socklen_t clilen;
	TMessage m;
	char *quitToken = NULL;
	fd_set read_fds;	
	fd_set tmp_fds;
	int fdmax;
	int num_clients = 0, count = 0;

	if (argc < 2) {
		usage(argv[0]);
	}

	// golirea descriptorilor
	FD_ZERO(&read_fds);
	FD_ZERO(&tmp_fds);

	// initializarea celor doi socketi
	sockTCP = socket(AF_INET, SOCK_STREAM, 0);
	sockUDP = socket(AF_INET, SOCK_DGRAM, 0);

	// identificarea portului
	portno = atoi(argv[1]);	

	// completarea adresei pentru a asculta TCP
	memset((char *) &serv_addrTCP, 0, sizeof(serv_addrTCP));
	serv_addrTCP.sin_family = AF_INET;
	serv_addrTCP.sin_port = htons(portno);
	serv_addrTCP.sin_addr.s_addr = INADDR_ANY;

	// completarea adresei pentru a asculta UDP
	serv_addrUDP.sin_family = AF_INET;
	serv_addrUDP.sin_port = htons(portno);
	serv_addrUDP.sin_addr.s_addr = htonl(INADDR_ANY);
	
	// legarea socketului TCP
	retTCP = bind(sockTCP,(struct sockaddr *)&serv_addrTCP,
		sizeof(struct sockaddr));

	// legarea socketului UDP
	retUDP = bind(sockUDP, (struct sockaddr *) &serv_addrUDP,
		sizeof(struct sockaddr));
	
	// ascultarea socketului TCP
	retTCP = listen(sockTCP, MAX_CLIENTS);
	
	FD_SET(sockTCP, &read_fds);
	FD_SET(sockUDP, &read_fds);
	FD_SET(0, &read_fds);

	fdmax = max(sockTCP, sockUDP);
	while (1) {

		tmp_fds = read_fds;
		retTCP = select(fdmax + 1, &tmp_fds, NULL, NULL, NULL);

		for (i = 0; i <= fdmax; i++) {
			if (FD_ISSET(i, &tmp_fds)) {
				// client TCP
				if (i == sockTCP) {
					int invalidID = 1;  // flag pentru a vedea daca e valid id
					clilen = sizeof(cli_addrTCP);
					newsockfd = accept(sockTCP, (struct sockaddr *)&cli_addrTCP,
						&clilen);
					FD_SET(newsockfd, &read_fds);
					memset(buffer, 0, BUFLEN);
					recv(newsockfd, buffer, sizeof(buffer), 0);
					if (newsockfd > fdmax) { 
						fdmax = newsockfd;
					}

					for (int j = 0; j < num_clients; j++) {
						if (strcmp(clients[j].id, buffer) == 0) {
							invalidID = 0;
							if (clients[j].active == 0) {
								clients[j].socketfd = newsockfd;
								clients[j].active = 1;
								printf("Client (%s) connected from %s:%d\n",
								buffer, inet_ntoa(cli_addrTCP.sin_addr),
								ntohs(cli_addrTCP.sin_port));
								int k;
								// trimit mesajele care erau in asteptare
								char *to_send;
								for (k = 0; k < clients[j].waiting_top; k++) {
									send(newsockfd, clients[j].waiting[k],
										strlen(clients[j].waiting[k]), 0);
								}

								// eliberez bufferul de mesaje in asteptare
								for (k = 0; k < clients[j].waiting_top; k++) {
									memcpy(clients[j].waiting[k],
										clients[j].waiting[k + 1],
										sizeof(clients[j].waiting[k + 1]));
								}
								// resetez numarul de teme in asteptare
								clients[j].waiting_top = 0;
							}
							else {
								// se incearca conectarea cu acelasi id
								send(newsockfd, "exit", sizeof("exit"), 0);
							}		
						}
					}
					if (invalidID) {
						// se adauga un client nou in vectorul de clienti
						memcpy(clients[num_clients].id, buffer,
							strlen(buffer) + 1);
						clients[num_clients].socketfd = newsockfd;
						clients[num_clients].active = 1;
						num_clients++; 
						printf("New client (%s) connected from %s:%d\n",
							buffer, inet_ntoa(cli_addrTCP.sin_addr),
							ntohs(cli_addrTCP.sin_port));
					}

				}
				// client UDP
				else if (i == sockUDP) {
					n = recvfrom(i, buffer, BUFLEN, 0,
						(struct sockaddr *)&cli_addrTCP, &clilen);
					m =  *((TMessage *)buffer);

					// se parseaza mesajul
					messageUDP(buffer, message_buf, cli_addrTCP);
					for (int k = 0; k < num_clients; k++) {
						for (int j = 0; j < clients[k].num_topics; j++) {

							// se trimite mesajul subscriber-ului activ
							if ((memcmp(clients[k].topics[j], m.topic,
								strlen(m.topic) + 1) == 0) &&
								(clients[k].active == 1)) {

								send(clients[k].socketfd, message_buf,
									strlen(message_buf), 0);
							}
							else if ((memcmp(clients[k].topics[j],
								m.topic, strlen(m.topic) + 1) == 0) && 
								(clients[k].active == 0) &&
								clients[k].sf[j] == 1) {
								// se stocheaza mesajul subscriber-ului inactiv

								memcpy(clients[k].waiting[clients[k].waiting_top],
									message_buf, strlen(message_buf) + 1);
								clients[k].waiting_top++;
							}
						}
					}
				}
				else if (i == 0) {
					//citirea de la tastatura
	               	memset(buffer, '\0', BUFLEN);
					fgets(buffer, BUFLEN, stdin);
					
					// la introducerea "exit", inchid clientii si serverul
					if (strncmp(buffer, "exit", 4) == 0) {
						for (int j = 0; j < num_clients; j++) {
							send(clients[j].socketfd, buffer,
								sizeof(buffer), 0);
						}
						return 0;
					}
				}
				else {
					memset(buffer, 0, BUFLEN);
					n = recv(i, buffer, sizeof(buffer), 0);
					if (n == 0) {
						// conexiunea s-a inchis
						for (int j = 0; j < num_clients; j++) {
							if (clients[j].socketfd == i) {

								// setez clientul inactiv
								clients[j].active = 0;
								printf("Client (%s) disconnected.\n",
									clients[j].id);
							}
						}
						close(i);
						
						// se scoate din multimea de citire socketul inchis 
						FD_CLR(i, &read_fds);
					}
					else {
						char *sub = NULL;  // stochez "(un)subscribed"
						char *top = NULL;  // stochez tema
						char *sf = NULL;  // stochez SF
						int isSubscribed = 1;  // flag de subscriere
						sub = strtok(buffer, " ");
						top = strtok(NULL, " \n");
						if (strcmp(sub, "subscribe") == 0)  {
							sf = strtok(NULL, " ");

							// adaug topic si SF pentru socket
							for (int j = 0; j < num_clients; j++) {
								if (clients[j].socketfd == i) {

									for (int w = 0; w < clients[j].num_topics; w++) {
										if (memcmp(clients[j].topics[w], top,
											strlen(top) + 1) == 0) {
											
											send(clients[j].socketfd, "You are already subscribed.", sizeof("You are already subscribed."), 0);
									
											// setez ca e subscris
											isSubscribed = 0;
										}
									}

									if (isSubscribed) {
										// adaug topicul in lista clientului
										memcpy(clients[j].topics[clients[j].num_topics],
											top, strlen(top) + 1);
										
										// adaug sf-ul in lista
										clients[j].sf[clients[j].num_topics] = atoi(sf);

										// cresc numarul de topicuri
										clients[j].num_topics++;
										send(clients[j].socketfd, "Successfully subscribed.", sizeof("Successfully subscribed."), 0);
									}
								}
							}
						}

						else if (strcmp(sub, "unsubscribe") == 0) {
							
							//scot topic si SF pentru client
							int k = 0;
							for (int j = 0; j < num_clients; j++) {
								if (clients[j].socketfd == i) {
									for (k = 0; k < clients[j].num_topics; k++) {
										if (memcmp(clients[j].topics[k], top, strlen(top) + 1) == 0) {
											//sterg linia respectiva
											for (int p = k; p < clients[j].num_topics - 1 ; p++) {
									            memcpy(clients[j].topics[k], clients[j].topics[k + 1], sizeof(clients[j].topics[k + 1]));
									        }
											for (int y = k; y < clients[j].num_topics; y++) {
											       clients[j].sf[y] = clients[j].sf[y + 1]; 
											}
									       	clients[j].num_topics--;
									       	break;
										}
									} 	
									send(clients[j].socketfd, "Successfully unsubscribed.", sizeof("Successfully unsubscribed."), 0);
								}
							}
						} 
					}
				}
			}
		}
		
	}
	return 0;
}