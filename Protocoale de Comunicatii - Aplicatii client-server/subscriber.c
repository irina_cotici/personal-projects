#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#define BUFLEN 1800
#define MAX_CLIENTS 100

void usage(char *file) {
	fprintf(stderr, "Usage: %s id server_address server_port\n", file);
	exit(0);
}

int main(int argc, char *argv[]){
	int sockfd, n, ret;
	struct sockaddr_in serv_addr;
	char buffer[BUFLEN];
	char buffer_aux[BUFLEN];

	char *sub = NULL;
	char *topic = NULL;
	char *SF = NULL;
	if (argc < 4) {
		usage(argv[0]);
	}

	// initiez socket-ul
	sockfd = socket(PF_INET, SOCK_STREAM, 0);

	// completez datele clientului
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(atoi(argv[3]));
	ret = inet_aton(argv[2], &serv_addr.sin_addr);

	// creez conexiunea
	ret = connect(sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr));

	fd_set read_fds;
    fd_set tmp_fds;
    FD_SET(sockfd, &read_fds);

    // 0 pentru ca citim de la tastatura
    FD_SET(0, &read_fds);

    int fdmax = sockfd;
	memset(buffer, 0, BUFLEN);
	memcpy(buffer, argv[1], strlen(argv[1]) + 1);

	// trimit id-ul clientului
	send(sockfd, buffer, strlen(buffer), 0);

	while (1) {

		tmp_fds = read_fds;
    	select(fdmax + 1, &tmp_fds, NULL, NULL, NULL);

    	int i = 0;

    	for(i = 0; i <= fdmax; i++) {
    		if (FD_ISSET(i, &tmp_fds)) {
				if (i == 0) {

			  		// se citeste de la tastatura
					memset(buffer, 0, BUFLEN);
					fgets(buffer, BUFLEN - 1, stdin);

					// inchid daca e mesaj de "exit"
					if (strncmp(buffer, "exit", 4) == 0) {
						return 0;
					}
					memcpy(buffer_aux, buffer, strlen(buffer) + 1);
					sub = strtok(buffer_aux, " ");
					topic = strtok(NULL, " ");

					// definesc formatul comenzilor pentru client
					if (strcmp(sub, "subscribe") == 0)  {
						SF = strtok(NULL, " ");
						if (SF == NULL) {
							printf("Invalide type of command.\n");
						}
						else if ((strcmp(SF, "0\n") == 0) ||
							(strcmp(SF, "1\n") == 0 )) {
							n = send(sockfd, buffer, strlen(buffer), 0);
						}
						else {
							printf("Invalide type of command.\n");
						}
					}
					else if (strcmp(sub, "unsubscribe") == 0){
						SF = strtok(NULL, " ");
						if (SF == NULL) {
							n = send(sockfd, buffer, strlen(buffer), 0);
						} 
						else {
							printf("Invalide type of command.\n");
						}
					}
					else {
						printf("Invalide type of command.\n");
					}
				}
				else {
    				memset(buffer, 0, BUFLEN);
    				n = recv(i, buffer, sizeof(buffer), 0);
    				printf("%s\n", buffer);
    				// inchid clientul daca serverul s-a inchis
    				if (n = 0 || strncmp(buffer, "exit", 4) == 0) {
						return 0;
					}
    			}
    		}
    	}

	}

	close(sockfd);
	return 0;
}