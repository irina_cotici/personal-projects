TEMA 2
PROTOCOALE DE COMUNICATIE
COTICI IRNA 321CD

Implementarea temei respective a implicat completarea functionalitatii unui
server si a clientilor TCP.

Pentru a asigura serverul, fisierul server.c inturneste actiunile pe care le 
realizeaza acesta. In primul rand, mi-am definit o structura TSubscriber care 
contine informatiile despre un client TCP. Toti subscriber-ii din simulare vor 
fi stocati intr-un vector. De asemenea, mi-am creat structura TMessage care 
contine informatiile depsre un mesaj al unui furnizor UDP.

Functia messageUDP realizeaza parsarea mesajelor, furnizand-o in forma specifica 
tipului acesteia.

In principiu, toata functionalitatea se contine in main. Probabil ar fi trebuit 
sa creez functii specializate, dar mi s-a parut mai fluida activitatea in acest 
fel.

Intr-o bucla, verific daca file descriptor-ul analizar este TCP, UDP sau stdin.

Daca e TCP:
	-accept conexiunea si obtin prin recv id-ul clientului.

	-iterez prin vectorul de clienti si verific daca mai exista vreun client cu 
	un astfel de id. 
		Daca nu exista, completez vectorul cu un client nou, afisand mesajul 
		conectarii clientului.

		Daca exista, verific daca e vorba de un client online. In cazul in care 
		un client cu id-ul respectiv este deja conectat, inchid, pur si simplu, 
		incercarea de conectare. In cazul in care e vorba de un client care a 
		fost pana in acel moment offline, il setez pe active, iterez prin
		vectorul sau de mesaje in asteptare dedicate lui, apoi i le trimit, 
		eliberand vectorul cu acele mesaje.

Daca e UDP:
	-structurez mesajul prin intermediul functiei messageUDP.

	-iterez prin vectorul de clienti, identificandu-i pe cei
	care sunt abonati la topicul respectiv.
		Daca identific un client online, ii trimit mesajul.
		Daca nu este online, ii adaug mesajul in lista de asteptare.

Daca e stdin:
	-citesc de la tastatura si inchid serverul, transmitand un mesaj de exit si
	clientilor.

Daca primesc mesaj:
	-in cazul in care mesajul este gol, afisez un mesaj de deconectare
	a acelui client.

	-daca e mesaj de "subscribe" si clientul este deja subscris la tema
	respectiva, afisez "You are already subscribed.". Daca nu e deja subscris,
	adaug tema in vectorul de topic-uri al clientului, adaug si SF-ul specific
	temei in informatiile clientului si incrementez numarul de teme la care
	este subscris.

	-daca e mesaj de "unsubscribe", identific tema si SF-ul aderent, iar, 
	ulterior le sterg din vectorul de teme si sf, decrementand numarul de
	teme pe care le detine clientul.

In fisierul subscriber.c se afla logica de functionare a unui client TCP.
Acesta, iarasi, inturneste toti pasii in main. Pentru inceput, se initiaza
socket-ul, se completeaza adresa si portul clientului, se realizeaza conexiunea,
trimitandu-se aceste detalii serverului.

Ulterior, intr-un while, realizez citirea de la tastatura a mesajelor
clientului. Primul lucru efectuat este verificarea inputului "exit" care conduce
la inchiderea clientului. Daca nu este vorba de un astfel de mesaj, inspectez
natura acestuia.
	-daca e vorba de un mesaj de "subscribe", urmat de tema si tip, il trimit
	catre server. Daca nu este respectata aceasta structura, afisez "Invalide
	type of command".

	-daca e vorba de un mesaj de "unsubscribed" urmat doar de tema, de asemenea,
	il trimit. Daca mai e urmat de ceva, afisez acelasi mesaj de invaliditate.

La primirea unui mesaj de la server, clientul il afiseaza. Respectiv, daca
serverul a trimis "exit", clientul se inchide.

