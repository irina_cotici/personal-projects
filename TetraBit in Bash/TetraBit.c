#include <stdio.h>
#include <math.h>

/*
	Functia de afisare a hartii, care primeste ca parametru nurmarul hartii
*/
void AfisareaHartii(unsigned long long int s) {
	int c;
	unsigned long long int k;

	for (c = 63; c >= 0; c--) {
		k = s >> c;
		if (k & 1)
			printf("#");
		else
			printf(".");
		if (c % 8 == 0) {
			printf("\n");
		}
	}
	printf("\n");
}

/*
	Functia de calculare a scorului
*/
void Score(long long unsigned int s, int y) { 
	int i = 0, c, k, q = 0;
	for (c = 63; c >= 0; c--) {
		k = s >> c;
		if (!(k & 1)) {
			i +=1;
		} else {
			q++;
		}

	}
	printf("GAME OVER!\nScore:%.2f\n", sqrt( i) + pow(1.25, y));
}
/*
	Functia de eliminare a liniilor complete
*/
long long unsigned int StergereLinii(long long unsigned int s) {
	long long unsigned int jum_masca1, jum_masca2;
	int linie;
	/*
		Se parcurg liniile hartii, fiind comparate 
		cu masca ce contine o linie completa
	*/
	for ( linie = 7; linie >= 0; linie--) {
		if (((s >> (8 * linie)) & 255) == 255) {
			if (linie == 0) {
				s = s >> 8;
				continue;
			}

			jum_masca1 = s << ((8 - linie) * 8) >> ((8 - linie) * 8);
			jum_masca2 = s >> ((linie + 1) * 8) << ((linie) * 8);
			s = (jum_masca1 | jum_masca2);
		}
	}
	return s;
}

/*
	Functia de determinare a numarului de linii complete
*/
int numarLiniiComplete(long long unsigned int s) {
	int count = 0;
	int linie;
	for ( linie = 7; linie >= 0; linie--) {
		if (((s >> (8 * linie)) & 255) == 255) {
			count++;
		}
	}
	return count;
}

int main() {
	int gameover = 0;
	long long unsigned int numHarta;						
	long long unsigned int numPiesa;								 
	int numMutari, ultimNivel;

	int i = 0, liniiSparte = 0, ult;
	int mutari[8];

	scanf("%llu", &numHarta );
	scanf("%d", &numMutari);
	
	if (numMutari == 0) {
		AfisareaHartii(numHarta);
	}
    
	if (numMutari > 0) {
		AfisareaHartii(numHarta);
	}

	/*
		Se verifica numarul de mutari, executand 
		conditiile de deplasare ale piesei
	*/ 
	while (numMutari > 0) {
		scanf("%llu", &numPiesa);
		i = 0;  
		ult = -1;                                                        
		while (i < 8) {
			ultimNivel = i;
			scanf("%d", &mutari[i]);

			/* 
				Se verifica coliziunea pe verticala, declansand mutarea piesei
			*/			
			if ((numHarta & (numPiesa << (56 - 8 * i))) == 0) {
				while (mutari[i] != 0) {
					/* 
						Se executa deplasarea piesei la dreapta,
						in cazul introducerii unei mutari de semn pozitiv
					*/
					if (mutari[i] > 0) {
						/*
							Se realizeaza deplasarea la dreapta, verificandu-se 
							coliziunea pe orizontala si indeplinindu-se oprirea pe linie
						*/
						if ((numHarta & (i == 7 ? (numPiesa >> 1) : (numPiesa << (56 - 8 * i - 1)))) == 0 &&
							(numPiesa  & 257) == 0) {
							numPiesa >>= 1; 
						} else {
							break;
						}
						mutari[i]--;
					/*
						Se executa deplasarea piesei la stanga,
						in cazul introducerii unei mutari de semn negativ
					*/
					} else if (mutari[i] < 0) {
						/*
							Se realizeaza deplasarea la stanga, verificandu-se 
							coliziunea pe orizontala si indeplinindu-se oprirea pe linie
						*/
						if ((numHarta & (numPiesa << (56 - 8 * i + 1))) == 0 &&
							(numPiesa & 32896) == 0) {
							numPiesa <<= 1; 
						} else {
							break;
						}
						mutari[i]++;
					}
				}
			} else {
				if (i == 0 || (i == 1 && numPiesa >= (1 << 8))) {
					AfisareaHartii( numHarta | numPiesa << (56 - 8 * i));
					gameover = 1;
				}
				ult = i - 1;
				while (i < 7) {
					scanf("%d", &mutari[i]);
					i++;
				}
				break;
			}

			AfisareaHartii( numHarta | numPiesa << (56 - 8 * i));
			i++;

		}
		numMutari--;
		if (ult != -1) {
			numHarta |= (numPiesa << (56 - 8 * ult));
		} else {
			numHarta |= (numPiesa << (56 - 8 * ultimNivel));
		}
		/*
			Se afiseaza noua harta in urma stergerii liniilor complete
		*/
		if (numHarta != StergereLinii(numHarta)) {
			liniiSparte += numarLiniiComplete(numHarta);
			numHarta = StergereLinii(numHarta);
			AfisareaHartii(numHarta);
		}
		if (gameover) {
			break;
		}
	}
	/*
		Se afiseaza scorul final
	*/
	Score(numHarta, liniiSparte);
	return 0;
}

