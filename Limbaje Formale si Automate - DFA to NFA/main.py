import string
import sys
EPSILON = 'eps'
nfa_initial = 0

# Functie ce returneaza inchiderea eps
# def epsilonClosure(state, nfa_delta, nfa_states):
#         a = [state]
#         for i in range(0, nfa_states):
#         	if ((state, EPSILON) in nfa_delta.keys()):
# 	        	for j in nfa_delta[(state, EPSILON)]:
# 	        		a.append(j)
# 	        		state = j
#         return set(a)

def epsilonClosure(state, nfa_delta, nfa_states):
	a = set()
	a.add(state)
	b = set()
	
	while(len(b) != len(a)):
		b = set()
		b.update(list(a))
		for j in b:
			if (j, EPSILON) in nfa_delta.keys():
				a.update(list(nfa_delta[j, EPSILON]))
	return a


# Starile posibile intr-un pas
def step(state, ch, nfa_delta, nfa_states):
    	a = set()
    	if ((state, ch) in nfa_delta.keys()):
    		a.update(list(nfa_delta[state,ch]))
    		b = set()
    		
    		while(len(b) != len(a)):
    			b = set()
    			b.update(list(a))
    			for j in b:
    				a.update(list(epsilonClosure(j, nfa_delta, nfa_states)))
    	return a

# Starile posibile dintr-o lista de stari
def multipleSteps(states, ch, nfa_delta, nfa_states):
	a = [] 
	for i in states:
		a.extend(step(i, ch, nfa_delta, nfa_states))
	return a

# Adaugarea starii sink in tranzitii
def createSinkState(dfa_delta, alphabet, idx, states, dfa_states):
	need_sink = False
	for i in dfa_delta.keys():
		if (dfa_delta[i] == -1):
			dfa_delta[i] = idx
			need_sink = True
	
	dfa_states = len(states)

	if (need_sink):
		for i in alphabet:
			dfa_delta[idx, i] = idx
		dfa_states += 1
	return [dfa_delta, dfa_states]

# Functia de convertire nfa-to-dfa
def nfaToDfa(nfa_states, nfa_final_st, nfa_delta, alphabet):
	initial_state = list(epsilonClosure(nfa_initial, nfa_delta, nfa_states))
	initial_state.sort()
	a = [initial_state]
	current = initial_state
	count = 0
	dfa_delta = {}

	while(count in range(0, len(a))):
		current = a[count]
		for i in alphabet:
			states = set(multipleSteps(current, i, nfa_delta, nfa_states))
			states = list(states)
			states.sort()

			if states:
				if (states not in a):
					a.append(states)
				dfa_delta[(count, i)] = a.index(states)

			elif (states == []):
			 	dfa_delta[(count, i)] = -1
		count += 1
		print(len(current))

	print(dfa_delta)
	[dfa_delta, dfa_states] = createSinkState(dfa_delta, alphabet, count, a, len(a))

	dfa_final_st = set()
	for i in nfa_final_st:
		for j in a:
			if (i in j):
				dfa_final_st.add(a.index(j))

	writeToFile(dfa_states, dfa_final_st, dfa_delta)
	return dfa_delta

def writeToFile(dfa_states, dfa_final_st, dfa_delta):
	g = open(sys.argv[2], "w")

	out_final_st = ' '.join(str(i) for i in dfa_final_st)
	g.write(str(dfa_states) + '\n')
	g.write(out_final_st + '\n')

	for i, j in dfa_delta.items():
		out_source = i[0]
		g.write(str(i[0]) + ' ' + str(i[1]) + ' ' + str(j) + '\n')


def main():
	f = open(sys.argv[1], "r")
	lines = f.readlines()
	f.close()

	nfa_states = int(lines[0].rstrip('\n'))
	nfa_final_st = [int(i) for i in (lines[1].rstrip('\n').split(' '))]

	alphabet = set()
	nfa_delta = {}
	for i in range(2, len(lines)):
		transitions = lines[i].rstrip('\n').split(' ')
		if (transitions[1] != 'eps'):
			alphabet.add(transitions[1])
		nfa_delta[(int(transitions[0]), transitions[1])] = set([int(j) for j in transitions[2:]])

	dfa_delta = nfaToDfa(nfa_states, nfa_final_st, nfa_delta, alphabet)
if __name__ == '__main__':
	main()