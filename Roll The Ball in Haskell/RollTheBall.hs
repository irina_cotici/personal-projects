{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE EmptyDataDecls, MultiParamTypeClasses,
             TypeSynonymInstances, FlexibleInstances,
             InstanceSigs #-}
module RollTheBall where
import Pipes
import ProblemState
import Data.Array as A

{-
    Direcțiile în care se poate mișca o piesa pe tablă
-}

data Directions = North | South | West | East
    deriving (Show, Eq, Ord)

{-
    Sinonim tip de date pentru reprezetarea unei perechi (Int, Int)
    care va reține coordonatele celulelor de pe tabla de joc
-}

type Position = (Int, Int)

{-
    Tip de date pentru reprezentarea celulelor tablei de joc
-}
data Cell = Cell Char
	deriving (Eq, Ord)

{-
    Tip de date pentru reprezentarea nivelului curent
-}
data Level = Level (A.Array Position Cell)
    deriving (Eq, Ord)
{-
    *** Optional *** 
  
    Dacă aveți nevoie de o funcționalitate particulară,
    instantiați explicit clasele Eq și Ord pentru Level.
    În cazul acesta, eliminați deriving (Eq, Ord) din Level.
-}

{-
    *** TODO ***

    Instanțiati Level pe Show. 
    Atenție! Fiecare linie este urmată de \n (endl in Pipes).
-}

instance Show Level 
    where show (Level arr) = endl : (map (\(Cell x) -> x) (foldl (++) [] (map (++ [Cell endl]) (takeWhile (not . null) $ map (take (1 + (snd $ snd $ A.bounds arr))) $ iterate (drop (1 + (snd $ snd $ A.bounds arr))) (A.elems arr)))))

instance Show Cell
	where show (Cell a) = [a]
{-
    *** TODO ***
    Primește coordonatele colțului din dreapta jos a hărții.
    Intoarce un obiect de tip Level în care tabla este populată
    cu EmptySpace. Implicit, colțul din stânga sus este (0,0)
-}
emptyLevel :: Position -> Level
emptyLevel (row, column) = Level (A.array ((0, 0), (row, column)) [((x, y), Cell (emptySpace)) | x <- [0..row], y <- [0..column]]) 

{-
    *** TODO ***

    Adaugă o celulă de tip Pipe în nivelul curent.
    Parametrul char descrie tipul de tile adăugat: 
        verPipe -> pipe vertical
        horPipe -> pipe orizontal
        topLeft, botLeft, topRight, botRight -> pipe de tip colt
        startUp, startDown, startLeft, startRight -> pipe de tip initial
        winUp, winDown, winLeft, winRight -> pipe de tip final
    Parametrul Position reprezintă poziția de pe hartă la care va fi adaugată
    celula, dacă aceasta este liberă (emptySpace).
-}

addCell :: (Char, Position) -> Level -> Level
addCell (cell_type, (i,j)) (Level arr)
	| (j <= m) && (j >= 0) && (i <= n) && (i >= 0) && ((arr A.! (i,j)) == Cell (emptySpace)) = Level (arr A.//[((i, j), (Cell cell_type))])
	| otherwise =  (Level arr)
	where (n, m) = (snd $ A.bounds arr)

{-
    *** TODO *** 

    Primește coordonatele colțului din dreapta jos al hărții și o listă de 
    perechi de tipul (caracter_celulă, poziția_celulei).
    Întoarce un obiect de tip Level cu toate celeule din listă agăugate pe
    hartă.
    Observatie: Lista primită ca parametru trebuie parcursă de la dreapta 
    la stanga.
-}
 
createLevel :: Position -> [(Char, Position)] -> Level
createLevel (n, m) l = foldr addCell (emptyLevel (n, m)) l

{-
    *** TODO ***

    Mișcarea unei celule în una din cele 4 direcții 
    Schimbul se poate face doar dacă celula vecină e goală (emptySpace).
    Celulele de tip start și win sunt imutabile.

    Hint: Dacă nu se poate face mutarea puteți lăsa nivelul neschimbat.
-}

checkCell :: Char -> Bool
checkCell a = if (elem a (startCells ++ winningCells)) 
	then True
	else False

moveCell :: Position -> Directions -> Level -> Level
moveCell (i, j) d (Level arr)
	| not ((j <= m) && (j >= 0) && (i <= n) && (i >= 0)) = (Level arr)
	| checkCell a = (Level arr)
	| d == North && (i > 0) && ((arr A.! (i - 1, j)) == Cell emptySpace) = addCell (a, (i - 1, j)) (Level (arr A.//[((i, j), Cell emptySpace)]))
	| d == South && (i < n) && ((arr A.! (i + 1, j)) == Cell emptySpace) = addCell (a, (i + 1, j)) (Level (arr A.//[((i, j), Cell emptySpace)]))
	| d == East && (j < m) && ((arr A.! (i, j + 1)) == Cell emptySpace) = addCell (a, (i, j + 1)) (Level (arr A.//[((i, j), Cell emptySpace)]))
	| d == West && (j > 0) && ((arr A.! (i, j - 1)) == Cell emptySpace) = addCell (a, (i, j - 1)) (Level (arr A.//[((i, j), Cell emptySpace)]))
	| otherwise = (Level arr)
	where
		Cell a = (arr A.! (i,j))
		(n, m) = snd $ A.bounds arr

{-
    *** HELPER ***

    Verifică dacă două celule se pot conecta.
    Atenție: Direcția indică ce vecin este a
    doua celulă pentru prima.

    ex: connection botLeft horPipe East = True (╚═)
        connection horPipe botLeft East = False (═╚)
-}

dirConnection :: Directions -> Char -> Bool
dirConnection d a
    | d == East && (elem a [horPipe, botRight, topRight, startLeft, winLeft]) = True
    | d == North && (elem a [verPipe, topLeft, topRight, startDown, winDown]) = True
    | d == West && (elem a [horPipe, topLeft, botLeft, startRight, winRight]) = True
    | d == South && (elem a [verPipe, botLeft, botRight, startUp, winUp]) = True
    | otherwise = False

{-
	0 - iesire pe dreapta
	1 - iesire pe stanga
	2 - iesire in sus
	3 - iesire in jos
-}

connectionType :: Char -> Int
connectionType a
	| (elem a [horPipe, topLeft, botLeft, startRight, winRight]) = 0
	| (elem a [horPipe, botRight, topRight, startLeft, winLeft]) = 1
	| (elem a [verPipe, botLeft, botRight, startUp, winUp]) = 2
	| (elem a [verPipe, topLeft, topRight, startDown, winDown]) = 3
	| otherwise = -1


connection :: Cell -> Cell -> Directions -> Bool
connection (Cell c1) (Cell c2) d
	| (connectionType c1 == 0) && (dirConnection d c2) = True
	| (connectionType c1 == 1) && (dirConnection d c2) = True
	| (connectionType c1 == 2) && (dirConnection d c2) = True
	| (connectionType c1 == 3) && (dirConnection d c2) = True
	| otherwise = False

{-
    *** TODO ***

    Va returna True dacă jocul este câștigat, False dacă nu.
    Va verifica dacă celulele cu Pipe formează o cale continuă de la celula
    de tip inițial la cea de tip final.
    Este folosită în cadrul Interactive.
-}

getStartPosition :: Level -> (Int, Int)
getStartPosition (Level arr) = head $ [(i, j) | i <- [0..n], j <-[0..m], ((elem  ((arr A.! (i, j))) (map Cell startCells)))]
	where
		(n, m) = snd $ A.bounds arr

getWinPosition :: Level -> (Int, Int)
getWinPosition (Level arr) = head $ [(i, j) | i <- [0..n], j <-[0..m], ((elem  ((arr A.! (i, j))) (map Cell winningCells)))]
	where
		(n, m) = snd $ A.bounds arr

checkPosition :: (Int, Int) -> (Int, Int) -> Int
checkPosition (a, b) (c, d)
	| a > c = 1
	| (a == c) && (b > d) = 1
	| (a == c) && (b == d) = 2
	| otherwise = 0

nextHopDir :: Cell -> Position -> Directions -> (Position, Directions)
nextHopDir (Cell a) (i, j) d
	| (d /= West) && (elem a [horPipe, topLeft, botLeft, startRight, winRight]) = ((i, j + 1), East)
	| (d /= East) && (elem a [horPipe, botRight, topRight, startLeft, winLeft]) = ((i, j - 1), West)
	| (d /= South) && (elem a [verPipe, botLeft, botRight, startUp, winUp]) = ((i - 1, j), North)
	| otherwise = ((i + 1, j), South)

auxLevel :: A.Array Position Cell -> Cell -> Position -> Directions -> Bool
auxLevel arr c (i, j) d
	| (elem c (map Cell winningCells)) = True
	| (connection c (arr A.! pos) dir) = (auxLevel arr (arr A.! pos) pos dir)
	| otherwise = False
	where
		(pos, dir) = nextHopDir c (i, j) d

wonLevel :: Level -> Bool
wonLevel (Level arr)
	| a == startLeft = auxLevel arr (Cell a) pos North
	| a == startRight =  auxLevel arr (Cell a) pos North
	| a == startUp = auxLevel arr (Cell a) pos East
	| otherwise = auxLevel arr (Cell a) pos East
	where
		pos = getStartPosition (Level arr)
		Cell a = arr A.! pos

northSucc :: Level -> [((Position, Directions), Level)]
northSucc (Level arr) = []++[(((i, j), North), (moveCell (i, j) North (Level arr))) | i <- [0..n], j <- [0..m], ((Level arr) /= (moveCell (i, j) North (Level arr)))]
	where
		(n, m) = snd $ A.bounds arr

southSucc :: Level -> [((Position, Directions), Level)]
southSucc (Level arr) = []++[(((i, j), South), (moveCell (i, j) South (Level arr))) | i <- [0..n], j <- [0..m], ((Level arr) /= (moveCell (i, j) South (Level arr)))]
	where
		(n, m) = snd $ A.bounds arr

eastSucc :: Level -> [((Position, Directions), Level)]
eastSucc (Level arr) = []++[(((i, j), East), (moveCell (i, j) East (Level arr))) | i <- [0..n], j <- [0..m], ((Level arr) /= (moveCell (i, j) East (Level arr)))]
	where
		(n, m) = snd $ A.bounds arr

westSucc :: Level -> [((Position, Directions), Level)]
westSucc (Level arr) = []++[(((i, j), West), (moveCell (i, j) West (Level arr))) | i <- [0..n], j <- [0..m], ((Level arr) /= (moveCell (i, j) West (Level arr)))]
	where
		(n, m) = snd $ A.bounds arr

instance ProblemState Level (Position, Directions) where
    successors (Level arr) = (northSucc (Level arr)) ++ (southSucc (Level arr)) ++ (eastSucc (Level arr)) ++ (westSucc (Level arr))
    isGoal (Level arr) = wonLevel (Level arr)
    reverseAction (((i, j), d), (Level arr))
    	| d == North = (((i - 1, j), South), (moveCell (i - 1, j) South (Level arr)))
    	| d == South = (((i + 1, j), North), (moveCell (i + 1, j) North (Level arr)))
    	| d == East = (((i, j + 1), West), (moveCell (i, j + 1) West (Level arr)))
    	| otherwise = (((i, j - 1), East), (moveCell (i, j - 1) East (Level arr)))
