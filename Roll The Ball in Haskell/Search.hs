{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Search where

import ProblemState
{-
    *** TODO ***

    Tipul unei nod utilizat în procesul de căutare. Recomandăm reținerea unor
    informații legate de:

    * stare;
    * acțiunea care a condus la această stare;
    * nodul părinte, prin explorarea căruia a fost obținut nodul curent;
    * adâncime
    * copiii, ce vor desemna stările învecinate
-}

data Node s a = Node {state :: s, action :: Maybe a, parent :: Maybe (Node s a), depth :: Int, succ :: [Node s a]}

{-
    *** TODO ***
    Gettere folosite pentru accesul la câmpurile nodului
-}
nodeState :: Node s a -> s
nodeState (Node s _ _ _ _) = s

nodeParent :: Node s a -> Maybe (Node s a)
nodeParent (Node _ _ p _ _) = p

nodeDepth :: Node s a -> Int
nodeDepth (Node _ _ _ d _) = d

nodeAction :: Node s a -> Maybe a
nodeAction (Node _ a _ _ _) = a

nodeChildren :: Node s a -> [Node s a]
nodeChildren (Node _ _ _ _ succes) = succes

{-
    *** TODO ***

    Generarea întregului spațiu al stărilor
    Primește starea inițială și creează nodul corespunzător acestei stări,
    având drept copii nodurile succesorilor stării curente.
-}


createSucc :: (ProblemState s a, Eq s) => s -> a -> Node s a -> Int -> [Node s a]
createSucc myState a p d = [(Node st (Just ac) (Just (Node myState (Just a) (Just p) d (createSucc myState a p d))) (d + 1) (createSucc st ac (Node myState (Just a) (Just p) d (createSucc myState a p d)) (d + 1))) | (ac, st) <- (successors myState)]

createStateSpace :: (ProblemState s a, Eq s) => s -> Node s a
createStateSpace myState = (Node myState Nothing Nothing 0 [(Node st (Just ac) (Just (createStateSpace myState)) 1 (createSucc st ac (createStateSpace myState) 1)) | (ac, st) <- (successors myState)])

{-
    *** TODO ***
   
    Primește un nod inițial și întoarce un flux de perechi formate din:
    * lista nodurilor adăugate în frontieră la pasul curent
    * frontiera

-}

auxBfs :: (Eq s) => [s] -> [Node s a] -> Node s a -> [([Node s a], [Node s a])]

auxBfs _ _ (Node s a Nothing d c) = ([node], [node]) : ((new, new) : (auxBfs [s] (tail new) (head new)))
    where 
        node = (Node s a Nothing d c)
        new = nodeChildren (Node s a Nothing d c)

auxBfs added predec (Node s a (Just p) d c) = (y, new) : (auxBfs new_added (tail new) (head new))
    where
        node = (Node s a (Just p) d c)
        y = filter (\(Node y _ _ _ _) -> not $ elem y added) (nodeChildren node)
        new = predec ++ y
        new_added = ([y | (Node y _ _ _ _) <- y] ++ added)

bfs :: (Eq s, Ord s) => Node s a -> [([Node s a], [Node s a])]
bfs node = auxBfs [] [] node 



{-
    *** TODO ***
  
    Primește starea inițială și finală și întoarce o pereche de noduri, reprezentând
    intersecția dintre cele două frontiere.
-}

bidirBFS :: Ord s => Node s a -> Node s a -> (Node s a, Node s a)
bidirBFS = undefined


{-
    *** TODO ***

    Pornind de la un nod, reface calea către nodul inițial, urmând legăturile
    către părinți.

    Întoarce o listă de perechi (acțiune, stare), care pornește de la starea inițială
    și se încheie în starea finală.

-}

extractPath :: Node s a -> [(Maybe a, s)]
extractPath (Node s Nothing Nothing _ _) = [(Nothing, s)]
extractPath (Node s a (Just p) _ _) = (extractPath p) ++ [(a, s)]


{-
    *** TODO ***

    Pornind de la o stare inițială și una finală, se folosește de bidirBFS pentru a găsi
    intersecția dintre cele două frontiere și de extractPath pentru a genera calea.

    Atenție: Pentru calea gasită în a doua parcurgere, trebuie să aveți grijă la a asocia
    corect fiecare stare cu acțiunea care a generat-o.

    Întoarce o listă de perechi (acțiune, stare), care pornește de la starea inițială
    și se încheie în starea finală.
-}

solve :: (ProblemState s a, Ord s)
      => s          -- Starea inițială de la care se pornește
      -> s          -- Starea finală la care se ajunge
      -> [(Maybe a, s)]   -- Lista perechilor
solve = undefined
