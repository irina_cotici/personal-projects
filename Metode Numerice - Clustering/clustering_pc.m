% computes the NC centroids corresponding to the given points using K-Means
function centroids = clustering_pc(points, NC)
	centroids = [];
  
[n, m] = size(points);
R = randperm(n);
I = zeros(n, 1);
centroids = zeros(NC, m);
old_cent = [];

for k=1:NC
    centroids(k,:) = points(R(k),:);
end

while ~isequal(centroids, old_cent)
    old_cent = centroids;

    for n=1:n

        index = 1;
        min = norm(points(n,:) - centroids(index,:), 1);
        for j=1:NC
            dist = norm(centroids(j,:) - points(n,:), 1);
            if dist < min
                index = j;
                min = dist;
            end
        end
        
        I(n) = index;
    end
    
    for k=1:NC
        centroids(k, :) = sum(points(find(I == k), :));
        centroids(k, :) = centroids(k, :) / length(find(I == k));
    end
 
end