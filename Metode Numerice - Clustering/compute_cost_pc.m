% computes a clustering solution total cost
function cost = compute_cost_pc(points, centroids)
	cost = 0; 
 centroids;
	% TODO compute clustering solution cost
   %clustering_pc(points, NC);
   for i= 1:size(points, 1)
     x = [];
     for j = 1:size(centroids, 1)
       x = [x, norm(points(i,:)-centroids(j,:))];
     end
     cost += min(x);
     
   end
   
 end
