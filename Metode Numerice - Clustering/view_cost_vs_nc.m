function view_cost_vs_nc(file_points)
	load(file_points);
  clusters = [];
  cost = [];
  x = 1;
  while x <= 10
    centroids = clustering_pc(points, x);
    cost = [cost compute_cost_pc(points, centroids)];
    x += 1;
  endwhile
  plot(cost);
end


