% outputs a graphical representation of the clustering solution
function view_clusters(points, centroids)

  %cmap = jet(size(centroids, 1))
  color = {'c', 'k','r', 'y', 'b'}; 
  cluster = [];
  for i = 1:size(points, 1) 
      dis = [];
      for j = 1:size(centroids, 1)
        dis = [dis, norm(points(i,:) - centroids(j,:))];
      endfor
      [z,idx] = min(dis);
      points(i,:);
      cluster = [cluster; points(i,:) idx];
    endfor 
    cluster;
    for i = 1:size(cluster, 1)
      scatter3(cluster(i,1),cluster(i,2),cluster(i,3), color{cluster(i,4)});
      hold on;
    end
    
end

