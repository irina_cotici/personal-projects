#include <stdio.h>      /* printf, sprintf */
#include <stdlib.h>     /* exit, atoi, malloc, free */
#include <unistd.h>     /* read, write, close */
#include <string.h>     /* memcpy, memset */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h>      /* struct hostent, gethostbyname */
#include <arpa/inet.h>
#include "helpers.h"
#include "requests.h"
#include "parson.c"

#define BUFF_LEN 1000
int main(int argc, char *argv[])
{
    char *message;
    char *response;
    int sockfd;
    char **data = malloc (2 * sizeof(char));
    char *in;
    char *buffer = malloc(sizeof(char) * BUFF_LEN);
    int bytes = 0;
    int len = BUFF_LEN;
    char *token_cookie = NULL;
    char *token_auth = NULL;

    while (1) {
        bytes = getline(&buffer, &len, stdin);
        if (!(strcmp(buffer, "register\n"))){

            // deschiderea sesiunii
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);

            // buffere pentru informatia citita de la taste
            char *username = malloc(sizeof(char) * BUFF_LEN);
            char *password = malloc(sizeof(char) * BUFF_LEN);
            
            getline(&username, &len, stdin);
            char *register_name = strtok(username, "=\n");
            register_name = strtok(NULL, "=\n"); // valoare

            getline(&password, &len, stdin); //cheia password
            char *register_pass = strtok(password, "=\n");
            register_pass = strtok(NULL, "=\n"); // valoare

            // crearea unui obiect JSON
            JSON_Value *root_value = json_value_init_object();
            JSON_Object *root_object = json_value_get_object(root_value);
            char *serialized_string = NULL;
            json_object_set_string(root_object, username, register_name);
            json_object_set_string(root_object, password, register_pass);
            serialized_string = json_serialize_to_string(root_value);

            // solicitarea cererii POST
            message = compute_post_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/auth/register", "application/json", serialized_string, 1, NULL, NULL, 0);
            send_to_server(sockfd, message);

            // primirea raspunsului de la server
            response = receive_from_server(sockfd);

            char *error_reg = strstr(response, "{\"error\"");
            char *final_mess = strtok(response, "\n");
            final_mess = strtok(NULL, "\n");
            puts(response);

            // afisarea erorii
            if (error_reg != NULL) {
                printf("%s\n", error_reg);
            }

            // eliberarea obiectului
            json_free_serialized_string(serialized_string);
            json_value_free(root_value);

        }
        else if (!(strcmp(buffer, "login\n"))) {

            // deschiderea conexiunii
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);

            // buffere pentru informatia citita
            char *log_buf_user = malloc(sizeof(char) * BUFF_LEN);
            char *log_buf_pass = malloc(sizeof(char) * BUFF_LEN);
            
            getline(&log_buf_user, &len, stdin);
            char *login_name = strtok(log_buf_user, "=\n");
            login_name = strtok(NULL, "=\n"); //valoare

            getline(&log_buf_pass, &len, stdin); //cheia login_pass
            char *login_pass = strtok(log_buf_pass, "=\n");
            login_pass = strtok(NULL, "=\n"); //valoare

            // crearea unui obiect JSON
            JSON_Value *root_value = json_value_init_object();
            JSON_Object *root_object = json_value_get_object(root_value);
            char *serialized_string = NULL;
            json_object_set_string(root_object, log_buf_user, login_name);
            json_object_set_string(root_object, log_buf_pass, login_pass);

            serialized_string = json_serialize_to_string(root_value);

            // cererea POST
            message = compute_post_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/auth/login", "application/json", serialized_string, 1, NULL, NULL, 0);
            
            // trimiterea catre server
            send_to_server(sockfd, message);
            response = receive_from_server(sockfd);
            char *error_login = strstr(response, "{\"error\"");

            char *cookie = strstr(response, "Set-Cookie: ");
            char *final_log = strtok(response, "\n");
            final_log = strtok(NULL, "\n");
            puts(response);

            // afisarea cookie-ului daca e cazul
            if (cookie != NULL) {
                char *tmp = malloc(sizeof(char) * (strlen(cookie) + 1));
                memcpy(tmp, cookie, (strlen(cookie) + 1));
                token_cookie = strtok(tmp, ";");
                token_cookie = strtok(NULL, ";");

                token_cookie = NULL;
                token_cookie = strtok(tmp, " ");
                token_cookie = strtok(NULL, " "); //aici se afla cookiul
                if (token_cookie != NULL) {
                    printf("cookie de sesiune: %s\n", token_cookie);
                }
            }

            // afisarea erorii
            if (error_login != NULL) {
                printf("%s\n", error_login);
            }

            // eliberarea obiectului JSON
            json_free_serialized_string(serialized_string);
            json_value_free(root_value);

        }
        else if (!(strcmp(buffer, "enter_library\n"))) {
            // deschiderea conexiunii
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);

            // solicitarea unei cereri GET
            message = compute_get_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/library/access", NULL, NULL, token_cookie, 1);
            send_to_server(sockfd, message);

            // primirea raspunsului de la server
            response = receive_from_server(sockfd);
            char *aux = strstr(response, "token");
            char *error_enter = strstr(response, "{\"error\"");
            char *final_enter = strtok(response, "\n");
            final_enter = strtok(NULL, "\n");
            puts(response);

            // afisarea token-ului de autentificare in biblioteca
            if (aux != NULL) {
                token_auth = strtok(aux, ":\"");
                token_auth = strtok(NULL, ":\""); //token de autentificare
                printf("Token de autentificare: %s\n", token_auth);
            }

            // afisarea erorii
            if (error_enter != NULL) {
                printf("%s\n", error_enter);
            }
        }
        else if (!(strcmp(buffer, "get_books\n"))) {

            // deschiderea conexiunii
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);

            // cererea GET
            message = compute_get_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/library/books", NULL, token_auth,token_cookie, 1);

            // trimiterea mesajului catre server
            send_to_server(sockfd, message);

            // obtinerea raspunsului de la server
            response = receive_from_server(sockfd);
            char *books = strstr(response, "[");
            char *error_books = strstr(response, "{\"error\"");
            char *final_books = strtok(response, "\n");
            final_books = strtok(NULL, "\n");
            puts(response);

            // afisarea listei de carti din biblioteca
            if (books != NULL) {
                printf("%s\n", books);
            }

            // afisarea erorii
            if (error_books != NULL) {
                printf("%s\n", error_books);
            }
        }
        else if (!(strcmp(buffer, "get_book\n"))) {
           
            // deschiderea conexiunii
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);
            char *id_buf = malloc(sizeof(char) * BUFF_LEN);
            getline(&id_buf, &len, stdin);

            char *id_tok = strtok(id_buf, "=\n");
            id_tok = strtok(NULL, "=\n");

            // concatenarea la path a id-ului cartii solicitate
            char str[1000] = "/api/v1/tema/library/books/";
            strcat(str, id_tok);
            strcat(str, "\0");

            // solicitarea unei cereri GET
            message = compute_get_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                str, NULL, token_auth,token_cookie, 1);

            // trimiterea catre server
            send_to_server(sockfd, message);

            // obtinerea raspunsului
            response = receive_from_server(sockfd);

            char *book = strstr(response, "[");
            char *error_book = strstr(response, "{\"error\"");
            char *final_book = strtok(response, "\n");
            final_book = strtok(NULL, "\n");
            puts(response);

            // afisarea cartii solicitate
            if (book != NULL) {
                printf("%s\n", book);
            }

            // afisarea erorii
            if (error_book != NULL) {
                printf("%s\n", error_book);
            }

        }
        else if (!(strcmp(buffer, "add_book\n"))) {

            // deschiderea conexiunii
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);

            // buffere pentru informatia citita
            char *title = malloc(sizeof(char) *BUFF_LEN);
            char *author = malloc(sizeof(char) *BUFF_LEN);
            char *genre = malloc(sizeof(char) *BUFF_LEN);
            char *publisher = malloc(sizeof(char) *BUFF_LEN);
            char *page_count = malloc(sizeof(char) *BUFF_LEN);

            getline(&title, &len, stdin);
            char *title_tok = strtok(title, "=\n");
            title_tok = strtok(NULL, "=\n"); // valoare titlu
        
            getline(&author, &len, stdin);
            char *author_tok = strtok(author, "=\n");
            author_tok = strtok(NULL, "=\n"); // valoare autor

            getline(&genre, &len, stdin);
            char *genre_tok = strtok(genre, "=\n");
            genre_tok = strtok(NULL, "=\n"); // valoare gen

            getline(&publisher, &len, stdin);
            char *publisher_tok = strtok(publisher, "=\n");
            publisher_tok = strtok(NULL, "=\n"); // valoare publisher

            getline(&page_count, &len, stdin);
            char *page_tok = strtok(page_count, "=\n");
            page_tok = strtok(NULL, "=\n"); // valoare pagina

            // crearea unui obiect JSON
            JSON_Value *root_value = json_value_init_object();
            JSON_Object *root_object = json_value_get_object(root_value);
            char *serialized_string = NULL;

            // adaugarea in obiect a cartii introduse
            json_object_set_string(root_object, title, title_tok);
            json_object_set_string(root_object, author, author_tok);
            json_object_set_string(root_object, genre, genre_tok);
            json_object_set_string(root_object, publisher, publisher_tok);
            json_object_set_number(root_object, page_count, atoi(page_tok));

            serialized_string = json_serialize_to_string(root_value);

            // solicitarea unei cereri POST
            message = compute_post_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/library/books", "application/json", serialized_string, 1, token_auth, NULL, 0);
            send_to_server(sockfd, message);
            response = receive_from_server(sockfd);

            char *error_add = strstr(response, "{\"error\"");
            char *final_book = strtok(response, "\n");
            final_book = strtok(NULL, "\n");
            puts(response);

            // afisarea cartii adaugate
            if (token_auth != NULL) {
                puts(serialized_string);
            }

            // afisarea erorii
            if (error_add != NULL) {
                printf("%s\n", error_add);
            }

            // eliberarea obiectului JSON
            json_free_serialized_string(serialized_string);
            json_value_free(root_value);

        }
        else if (!(strcmp(buffer, "delete_book\n"))) {
            
            // deshciderea conexiunii
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);
            char *id = malloc(sizeof(char) * BUFF_LEN);
            getline(&id, &len, stdin);

            char *token_id = strtok(id, "=\n");
            token_id = strtok(NULL, "=\n");

            // concatenarea la path a id-ului cartii dorite
            char string[1000] = "/api/v1/tema/library/books/";
            strcat(string, token_id);
            strcat(string, "\0");
            message = compute_delete_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                string, NULL, token_auth,token_cookie, 1);
            send_to_server(sockfd, message);
            response = receive_from_server(sockfd);

            char *error_delete = strstr(response, "{\"error\"");
            char *final_delete = strtok(response, "\n");
            final_delete = strtok(NULL, "\n");
            puts(response);

            // afisarea erorii daca exista
            if (error_delete != NULL) {
                printf("%s\n", error_delete);
            }
        }
        else if (!(strcmp(buffer, "logout\n"))) {

            // deschiderea conexiunii
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);
            message = compute_get_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/auth/logout", NULL, token_auth, token_cookie, 1);
            send_to_server(sockfd, message);
            response = receive_from_server(sockfd);

            char *error_logout = strstr(response, "{\"error\"");
            char *final_logout = strtok(response, "\n");
            final_logout = strtok(NULL, "\n");
            puts(response);
            
            // afisarea mesajului de eroare
            if (error_logout != NULL) {
                printf("%s\n", error_logout);
            }

            // resetarea token-urilor pentru autentificari
            token_auth = NULL;
            token_cookie = NULL;
        }
        else if (!(strcmp(buffer, "exit\n"))) {
            // inchiderea conexiunii
            return 0;
        }
        else {
            printf("Invalid command\n");
        }
    }
    return 0;
}
