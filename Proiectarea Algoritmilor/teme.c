#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct tema {
	int timp_realizare;
	int punctaj;
	int deadline;
	int id;
}Ttema;

Ttema *teme;
int num_teme;
int last_deadline;

void readInput() {
	FILE *f;
	f = fopen("teme.in", "r");
	int i = 0, j = 0;
	size_t buf_len = 1000000;
	char *buffer = NULL;
	buffer = malloc(sizeof(char) * buf_len);
	int bytes = 0;
	char *token = NULL;
	bytes = getline(&buffer, &buf_len, f);
	token = strtok_r(buffer, " ", &buffer);
	num_teme = atoi(token);
	int timp_realizare = 0;
	int punctaj = 0;
	int deadline = 0;
	int id = 0;
	teme = malloc(sizeof(Ttema) * num_teme);
	for (i = 0; i < num_teme; i++) {
		bytes = getline(&buffer, &buf_len, f);
		id++;
		token = strtok_r(buffer, " ", &buffer);
    	timp_realizare = atoi(token);
    	token = strtok_r(NULL, " ", &buffer);
		punctaj = atoi(token);
		token = strtok_r(NULL, " ", &buffer);
		deadline = atoi(token);
		teme[i].timp_realizare = timp_realizare;
		teme[i].punctaj = punctaj;
		teme[i].deadline = deadline;
		teme[i].id = id;
	}
}
void merge(Ttema *arr, int l, int m, int r) {
	int i, j, k;
	int n1 = m - l + 1;
	int n2 = r - m;
	Ttema L[n1], R[n2];
	/* Copy data to temp arrays L[] and R[] */
	for (i = 0; i < n1; i++)
		L[i] = arr[l + i];
	for (j = 0; j < n2; j++)
		R[j] = arr[m + 1+ j];
	/* Merge the temp arrays back into arr[l..r]*/
	i = 0;  // Initial index of first subarray
	j = 0;  // Initial index of second subarray
	k = l;  // Initial index of merged subarray
	while (i < n1 && j < n2) {
		if (L[i].deadline <= R[j].deadline) {
			arr[k] = L[i];
			i++;
		} else {
			arr[k] = R[j];
			j++;
		}
		k++;
	}

	/* Copy the remaining elements of L[], if there 
	are any */
	while (i < n1) {
		arr[k] = L[i];
		i++;
		k++;
	}

	/* Copy the remaining elements of R[], if there 
	are any */
	while (j < n2) {
		arr[k] = R[j];
		j++;
		k++;
	}
}

/* l is for left index and r is right index of the 
sub-array of arr to be sorted */
void mergeSort(Ttema *arr, int l, int r) {
	if (l < r) {
		// Same as (l+r)/2, but avoids overflow for
		// large l and h
		int m = l+(r-l)/2;

		// Sort first and second halves
		mergeSort(arr, l, m);
		mergeSort(arr, m+1, r);

		merge(arr, l, m, r);
	}
}

int maxim(int a, int b) {
	if (a > b)
		return a;
	else
		return b;
}

void reverse(int arr[], int n) {
	int *aux = malloc(sizeof(int) * n);

	for (int i = 0; i < n; i++) {
		aux[n - 1 - i] = arr[i];
	}

	for (int i = 0; i < n; i++) {
		arr[i] = aux[i];
	}
	free(aux);
}

void writeOutput(int punctaj, int teme_rezolvate, int arr[]) {
	FILE *f;
	f = fopen("teme.out", "w");
	fprintf(f, "%d %d\n", punctaj, teme_rezolvate);
	int i;
	for (i = 0; i < teme_rezolvate; i++) {
		fprintf(f, "%d ", arr[i]);
	}
	fclose(f);
}

int main() {
	readInput();

	mergeSort(teme, 0, num_teme - 1);
	last_deadline = teme[num_teme - 1].deadline;
	int *arr = malloc(sizeof(int) * num_teme);
	int i, j;
	int profit[last_deadline + 1][num_teme + 1];

	for (i = 0; i < last_deadline + 1; i++) {
		profit[i][0] = 0;
	}

	for (j = 1; j <= num_teme; j++) {
		for (i = 0; i < last_deadline + 1; i++) {
			if (teme[j - 1].deadline < i) {
				profit[i][j] = profit[i - 1][j];
			} else if (teme[j - 1].timp_realizare <= i) {
				profit[i][j] = maxim(profit[i][j - 1],
					teme[j - 1].punctaj + profit[i - teme[j - 1].timp_realizare][j - 1]);
			} else {
				profit[i][j] = profit[i][j - 1];
			}
		}
	}

	int max_i;
	int max_j;
	for (i = 0; i < last_deadline + 1; i++) {
		if (profit[i][num_teme] == profit[last_deadline][num_teme]) {
			max_i = i;
			break;
		}
	}

	i = max_i;
	j = num_teme;
	int max_profit = profit[i][j];
	int k = 0;
	while(j > 0) {
		if (profit[i][j] -
			profit[i - teme[j - 1].timp_realizare][j - 1] == teme[j - 1].punctaj) {
			arr[k] = teme[j - 1].id;
			k++;
			j--;
			i -= teme[j].timp_realizare;
		} else {
			j--;
		}
	}

	reverse(arr, k);
	writeOutput(max_profit, k, arr);
	free(arr);
	return 0;
}
