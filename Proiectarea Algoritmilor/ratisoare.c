#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

// structura unei ratuste
typedef struct ratusca {
	int id;
	int linie;
	int coloana;
	char directie;  // dreapta 1, stanga 0
	int cadere;
}TRatusca;

// structura unui nod din coada
typedef struct node {
	int data;
	struct node *next;
}Node;

// structura cozii
typedef struct queue {
	Node *head, *tail;
	int size;
}*Queue;

// functie de initializare a unui nod
Node *initNode(int data) {
	Node *node;
	node = malloc(sizeof(struct node));
	node->data = data;
	node->next = NULL;
	return node;
}

// functie de eliberare memorie pentru un nod
Node *freeNode(Node *node) {
	if (node) {
		free(node);
	}
	return NULL;
}

// verificarea cozii goale
int isEmptyQueue(Queue queue) {
	if (queue == NULL || queue->head == NULL || queue->size == 0)
		return 1;
	return 0;
}

// initializarea unei cozi
Queue initQueue() {
	Queue queue;
	queue = malloc(sizeof(struct queue));
	queue->size = 0;
	return queue;
}

// inserarea elementului in coada
Queue enqueue(Queue queue, int data) {
	Node *node;
	if (isEmptyQueue(queue)) {
		if (queue == NULL) {
			queue = initQueue();
		}
		node = initNode(data);
		queue->tail = node;
		queue->head = node;
		queue->size = 1;
		return queue;
	}
	node = initNode(data);
	queue->tail->next = node;
	queue->tail = node;
	queue->size++;
	return queue;
}

// stergerea primului element 
Queue dequeue(Queue queue) {
	Node *tmp;

	if (!isEmptyQueue(queue)) {
		tmp = queue->head;
		queue->head = queue->head->next;
		tmp = freeNode(tmp);
		queue->size--;
	}
	return queue;
}

// extragerea primului element
int first(Queue queue) {
	if (!isEmptyQueue(queue)) {
		return queue->head->data;
	} else {
		return -1;
	}
}

int *time;  // timpul final al ratelor
TRatusca **rate;  // matricea cu rate
int *index_arr;  // vector in care asociez pozitia
int num_linii;
int num_coloane;
int k;
int num_rate;

// citirea datelor de intrare
void readInput() {
	FILE *f;
	f = fopen("ratisoare.in", "r");
	int i = 0, j = 0;
	char *gb;
	size_t buf_len = 0;
	char *buffer = NULL;
	buffer = malloc(sizeof(char) * buf_len);
	int bytes = 0;
	char *token = NULL;

	bytes = getline(&buffer, &buf_len, f);
	token = strtok_r(buffer, " ", &gb);
	num_linii = atoi(token);
	token = strtok_r(NULL, " ", &gb);
	num_coloane = atoi(token);
	token = strtok_r(NULL, " ", &gb);
	k = atoi(token);
	token = strtok_r(NULL, " ", &gb);
	num_rate = atoi(token);

	index_arr = calloc(num_linii, sizeof(int));
	rate = calloc(num_linii, sizeof(TRatusca *));
	for (i = 0; i < num_linii; i++) {
		rate[i] = calloc(num_coloane, sizeof(TRatusca));
	}
	int id = 0;
	int linie = 0;
	int coloana = 0;
	char directie = 0;
	for (i = 0; i < num_rate; i++) {
		bytes = getline(&buffer, &buf_len, f);
		token = strtok_r(buffer, " ", &gb);
    	id = atoi(token);
    	token = strtok_r(NULL, " ", &gb);
		linie = atoi(token);

		linie--;
		token = strtok_r(NULL, " ", &gb);
		coloana = atoi(token);
		coloana--;
		token = strtok_r(NULL, " ", &gb);
		if (token[0] == 'S') {
			directie = 0;
			rate[linie][index_arr[linie]].cadere = coloana + 1;
		}
		else {

			directie = 1;
			rate[linie][index_arr[linie]].cadere = num_coloane - coloana;
		}
		rate[linie][index_arr[linie]].id = id;
		rate[linie][index_arr[linie]].directie = directie;
		rate[linie][index_arr[linie]].linie = linie;
		rate[linie][index_arr[linie]++].coloana = coloana;

	}

	fclose(f);
}

// scrierea datelor de iesire
void writeOutput(int id) {
	FILE *f;
	f = fopen("ratisoare.out", "w");
	fprintf(f, "%d\n", id);
}

/* functie de sortare a unei linii cu ratuste
	in dependenta de pozitia lor */
void merge(TRatusca *arr, int l, int m, int r) {
	int i, j, k;
	int n1 = m - l + 1;
	int n2 = r - m;
	TRatusca L[n1], R[n2];

	/* completarea vectorilor temporari */
	for (i = 0; i < n1; i++)
		L[i] = arr[l + i];
	for (j = 0; j < n2; j++)
		R[j] = arr[m + 1+ j];
	i = 0;
	j = 0;
	k = l;

	/* sortarea in functie de coloana */
	while (i < n1 && j < n2) {
		if (L[i].coloana <= R[j].coloana) {
			arr[k] = L[i];
			i++;
		} else {
			arr[k] = R[j];
			j++;
		}
		k++;
	}

	while (i < n1) {
		arr[k] = L[i];
		i++;
		k++;
	}

	while (j < n2) {
		arr[k] = R[j];
		j++;
		k++;
	}
}

// algoritmul de sortare MergeSort
void mergeSort(TRatusca *arr, int l, int r) {
	if (l < r) {
		int m = l+(r-l)/2;
		mergeSort(arr, l, m);
		mergeSort(arr, m+1, r);
		merge(arr, l, m, r);
	}
}

/* functie de sortare a ratustelor
	in dependenta de timpul lor de
	cadere */
void mergeTop(TRatusca *arr, int l, int m, int r) {
	int i, j, k;
	int n1 = m - l + 1;
	int n2 = r - m;
	TRatusca L[n1], R[n2];
	for (i = 0; i < n1; i++)
		L[i] = arr[l + i];
	for (j = 0; j < n2; j++)
		R[j] = arr[m + 1+ j];
	i = 0;
	j = 0;
	k = l;
	while (i < n1 && j < n2) {
		// compar timpul de cadere
		if (L[i].cadere < R[j].cadere) {
			arr[k] = L[i];
			i++;
		} 
		else if (L[i].cadere == R[j].cadere) {
			// realizez sortarea in functie de id
			if (L[i].id < R[j].id) {
				arr[k] = L[i];
				i++;
			} else {
				arr[k] = R[j];
				j++;
			}
		} 
		else {
			arr[k] = R[j];
			j++;
		}
		k++;
	}
	while (i < n1) {
		arr[k] = L[i];
		i++;
		k++;
	}
	while (j < n2) {
		arr[k] = R[j];
		j++;
		k++;
	}
}

/* algoritmul mergeSort pentru sortarea finala a ratelor */
void mergeSortTop(TRatusca *arr, int l, int r) {
	if (l < r) {
		int m = l+(r-l)/2;
		mergeSortTop(arr, l, m);
		mergeSortTop(arr, m+1, r);
		mergeTop(arr, l, m, r);
	}
}

/* functia de realizare a coliziunilor 
	pentru ratele cu directie spre stanga */
int coliziune_stanga(int linie, int id) {
	Queue q = initQueue();
	int i;
	int tmp = rate[linie][id].id;
	/* se parcurg ratele incepand cu prima
		dupa curenta, spre stanga liniei */
	for (i = id - 1; i >= 0; i--) {
		// se adauga in coada urmatoarea rata
		q = enqueue(q, rate[linie][i].id);
		// daca ratele au directie opusa
		if (rate[linie][i].directie) {
			// se extrage id-ul ratei
			tmp = first(q);
			q = dequeue(q);
		}
	}
	return tmp;
}

/* functia de realizare a coliziunilor 
	pentru ratele cu directie spre dreapta */
int coliziune_dreapta(int linie, int id) {
	Queue q = initQueue();
	int i;
	int tmp = rate[linie][id].id;
	/* se parcurg ratele incepand cu prima
		dupa curenta, spre dreapta liniei */
	for (i = id + 1; i < index_arr[linie]; i++) {
		q = enqueue(q, rate[linie][i].id);
		if (!rate[linie][i].directie) {
			tmp = first(q);
			q = dequeue(q);
		}
	}
	return tmp;
}

int main(int argc, char const *argv[]) {
	readInput();
	int i, j;
	int rezultat_id;
	// vectorul in care se stocheaza toate ratele
	TRatusca *top = malloc(sizeof(TRatusca) * num_rate);
	time = malloc(sizeof(int) * num_rate);

	// aranjarea ratelor pe linii
	for (i = 0; i < num_linii; i++) {
		mergeSort(rate[i], 0, index_arr[i] - 1);
	}
	// parcurgerea matricii
	for (i = 0; i < num_linii; i++) {
		for (j = 0; j < index_arr[i]; j++) {
			if (rate[i][j].directie) {
				rezultat_id = coliziune_dreapta(i, j);
			}
			else {
				rezultat_id = coliziune_stanga(i, j);
			}
			// completarea ratelor in vectorul final
			top[rate[i][j].id - 1] = rate[i][j];
			// completarea timpului de cadere in vector
			time[rezultat_id - 1] = rate[i][j].cadere;
		}
	}

	// asocierea ratelor cu timpul lor de cadere
	for (i = 0; i < num_rate; i++) {
		top[i].cadere = time[i];
	}
	
	// sortarea vectorului final
	mergeSortTop(top, 0, num_rate - 1);
	// scrierea ratei care va cadea a k-a
	writeOutput(top[k - 1].id);
	return 0;
}