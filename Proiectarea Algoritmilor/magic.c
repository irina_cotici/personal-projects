#include <stdio.h>
#include <stdlib.h>

int N;
int *arr;

// functie de citire input
void readInput() {
	FILE *f;
	f = fopen("magic.in", "r");

	int n, i;
	fscanf(f, "%d", &n);
	N = n;
	arr = malloc(sizeof(int)*N);
	for (i = 0; i < n; i++) {
		fscanf(f, "%d", &arr[i]);
	}
	fclose(f);
}

// functie de scriere output
void writeOutput(int min, int index1, int index2) {
	FILE *f;
	f = fopen("magic.out", "w");
	fprintf(f, "%d\n", min);
	fprintf(f, "%d %d\n", index1, index2);
	fclose(f);
}

int main() {
	readInput();
	int i, j;
	int stg = 0;
	int drp = 0;
	int suma_stg = 0;
	int suma_drp = 0;
	int suma = 0;
	int suma_patrat = 0;
	int diferenta = 0;
	int diferenta_patrat = 0;
	int numar_norocos = -1;
	int min = 1000000;
	int index1 = 0;
	int index2 = 0;
	/* vector in care stochez sumele 
		pana la elemntul i*/
	int sume_partiale[N];
	sume_partiale[0] = 0;
	for (i = 0; i < N - 1; i++) {
		sume_partiale[i + 1] = sume_partiale[i] + arr[i];
	}
	for (i = 0; i < N - 1; i++) {
		suma_stg = sume_partiale[i];
		for (j = i + 1; j < N; j++) {
			suma_drp = sume_partiale[j];
			suma = suma_stg - suma_drp;
			suma_patrat = suma * suma;
			diferenta = i - j;
			diferenta_patrat = diferenta * diferenta;
			numar_norocos = diferenta_patrat + suma_patrat;
			if (diferenta_patrat > min) {
				break;
			}
			if (min > numar_norocos) {
				min = numar_norocos;
				index1 = i + 1;
				index2 = j + 1;
			}
		}
	}
	writeOutput(min, index1, index2);
	return 0;
}
