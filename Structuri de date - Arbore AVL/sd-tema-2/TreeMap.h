#ifndef TREEMAP_H_
#define TREEMAP_H_

#include <stdlib.h>

#define MAX(a, b) (((a) >= (b))?(a):(b))

//-------------------------------------------------------------------------

typedef struct node{
	void* elem; // Node key
	void* info; // Node satellite information
	struct node *pt; // Parent link
	struct node *lt; // left child link
	struct node *rt; // right child link
	struct node* next; // next link in the list of duplicates
	struct node* prev; // prev link in the list of duplicates
	struct node* end; // pointer to the end of the duplicates list
	long height;
}TreeNode;

typedef struct TTree{
	TreeNode *root; // pointer to the root of the tree
	void* (*createElement)(void*); // create element method
	void (*destroyElement)(void*); // destroy element method
	void* (*createInfo)(void*); // create info method
	void (*destroyInfo)(void*); // destroy info method
	int (*compare)(void*, void*); // comparison method between elements
	long size;
}TTree;

//Functie de creare a unui arbore
TTree* createTree(void* (*createElement)(void*), void (*destroyElement)(void*),
		void* (*createInfo)(void*), void (*destroyInfo)(void*),
		int compare(void*, void*)){

	TTree* tree = (TTree*)malloc(sizeof(TTree));
	
	if (tree == NULL){
		return NULL;
	}

	//Initializarea campurilor
	tree->root = NULL;
	tree->createElement = createElement;
	tree->destroyElement = destroyElement;
	tree->createInfo = createInfo;
	tree->destroyInfo = destroyInfo;
	tree->compare = compare;
	tree->size = 0;
	return tree;
}

//Functie care verifica daca un arbore este gol
int isEmpty(TTree* tree){
	//se verifica dimensiunea arborelui
	if (tree->size == 0){
		return 1;
	}

	return 0;
}

//Functie ce identifica un nod in arbore
TreeNode* search(TTree* tree, TreeNode* x, void* elem){
	TreeNode* aux = tree->root;

	if (isEmpty(tree)){
		return NULL;
	}
	//Parcurgerea arborelui
	while(aux != NULL && tree->compare(elem, aux->elem) != 0 ){
		if (tree->compare(elem, aux->elem) == 1){
			aux = aux->rt;
		}

		else 
			aux = aux->lt;
	}

	return aux;
}

//Functie ce returneaza nodul cu cel mai mic element
TreeNode* minimum(TreeNode* x){

	while (x->lt != NULL){
		x = x->lt;
	}

	return x;
}

//Functie ce returneaza nodul cu cel mai mare element
TreeNode* maximum(TreeNode* x){
	
	while (x->rt != NULL){
		x = x->rt;
	}

	return x;
}

//Functie ce returneaza succesorul nodului primit ca parametru
TreeNode* successor(TreeNode* x){

	TreeNode *aux = x;
	TreeNode *temp = x;
	TreeNode *root;
	TreeNode *parent;

	//Parcurgerea arborelui spre radacina
	while (temp->pt != NULL){
		temp = temp->pt;
	}

	//Verificare daca elementul gasit nu e maxim
	root = temp;
	if (x == maximum(root)){
		return NULL;
	}

	if (aux->rt != NULL){

		aux = aux->rt;
		while (aux->lt != NULL){
			aux = aux->lt;
		}
		return aux;
	}

	else {

		parent = aux->pt;
		while (parent->rt == aux){
			parent = parent->pt;
			aux = aux->pt;
		}

		return parent;
	}
}

//Functie ce returneaza predecesorul nodului primit ca parametru
TreeNode* predecessor(TreeNode* x){

	TreeNode *aux = x;
	TreeNode *temp = x;
	TreeNode *root;
	TreeNode *parent;

	while (temp->pt != NULL){
		temp = temp->pt;
	}

	//Verificare daca elementul gasit este minimul din arbore
	root = temp;
	if (x == minimum(root)){
		return NULL;
	}

	if (aux->lt != NULL){

		aux = aux->lt;
		while (aux->rt != NULL){
			aux = aux->rt;
		}
		return aux;
	}

	else {

		parent = aux->pt;
		while (parent->lt == aux){
			parent = parent->pt;
			aux = aux->pt;
		}

		return parent;
	}
}

//Functie de reactualizare a inaltimii arborelui
void updateHeight(TreeNode* x){

	int leftHeight = 0;
	int rightHeight = 0;


	if(x != NULL){

		updateHeight(x->lt);
		updateHeight(x->rt);
		if(x->lt != NULL) leftHeight = x->lt->height;
		if(x->rt != NULL) rightHeight = x->rt->height;
		x->height = MAX(leftHeight, rightHeight) + 1;
	}
}

//Functie de rotire la stanga
void avlRotateLeft(TTree* tree, TreeNode* x){

	TreeNode *pivot = x->rt; //pivotul ce trebuie modificat
	TreeNode *root = x; //pointer catre actuala radacina
	TreeNode *p = x->pt; //parintele radacinii

	pivot->pt = x->pt;
	root->rt = pivot->lt;

	if (pivot->lt != NULL){
		pivot->lt->pt = root;
	}

	pivot->lt = root;
	root->pt = pivot;

	if (p == NULL){
		tree->root = pivot;
	}

	else {

		if (tree->compare(p->elem, pivot->elem) == 1){
			p->lt = pivot;
		}
		else
			p->rt = pivot;
	}

}

//Functie de rotire la stanga
void avlRotateRight(TTree* tree, TreeNode* y){

	TreeNode *pivot = y->lt; //pivotul ce trebuie modificat
	TreeNode *root = y; //pointer catre actuala radacina
	TreeNode *p = y->pt; //parintele actualei radacini

	pivot->pt = y->pt;
	root->lt = pivot->rt;

	if (pivot->rt != NULL){
		pivot->rt->pt = root;
	} 

	pivot->rt = root;
	root->pt = pivot;

	if (p == NULL){
		tree->root = pivot;
	}

	else {
		if (tree->compare(p->elem, pivot->elem) == 1){
			p->lt = pivot;
		}

		else
			p->rt = pivot;
	}
}

/* Get AVL balance factor for node x */
int avlGetBalance(TreeNode *x){

	if (x == NULL)
		return 0;
	if (x->rt == NULL && x->lt == NULL)
		return 0;
	if (x->rt == NULL)
		return x->lt->height;
	if (x->lt == NULL)
		return -x->rt->height;
	return (x->lt->height - x->rt->height);
}

//Functie de stabilire a echilibrului arborelui
void avlFixUp(TTree* tree, TreeNode* y){

	TreeNode *aux = y;

	while (y != NULL){

		//Tratarea cazului stanga-stanga
		if (avlGetBalance(y) > 1 && avlGetBalance(y->lt) >= 0){
			avlRotateRight(tree, y);
			updateHeight(tree->root);
			y = aux;
		}

		//Tratarea cazului dreapta-dreapta
		else if (avlGetBalance(y) < -1 && avlGetBalance(y->rt) <= 0){
			avlRotateLeft(tree, y);
			updateHeight(tree->root);
			y = aux;

		}
		//Tratarea cazului stanga-dreapta
		else if (avlGetBalance(y) > 1 && avlGetBalance(y->lt) < 0){
			avlRotateLeft(tree, y->lt);
			avlRotateRight(tree, y);
			updateHeight(tree->root);
			y = aux;
		}
		//Tratarea cazului dreapta-stanga
		else if (avlGetBalance(y) < -1 && avlGetBalance(y->rt) > 0){
			avlRotateRight(tree, y->rt);
			avlRotateLeft(tree, y);
			updateHeight(tree->root);
			y = aux;
		}
		y = y->pt;	
	}

}


TreeNode* createTreeNode(TTree *tree, void* value, void* info){
	// Allocate node
	TreeNode* newNode = (TreeNode*) malloc(sizeof(TreeNode));

	// Set its element field
	newNode->elem = tree->createElement(value);

	//Set its info
	newNode->info = tree->createInfo(info);

	// Set its tree links
	newNode->pt = newNode->rt = newNode->lt = NULL;

	// Set its list links
	newNode->next = newNode->prev = newNode->end = NULL;

	/*
	 *  The height of a new node is 1,
	 *  while the height of NULL is 0
	 */
	newNode->height = 1;

	return newNode;
}

void destroyTreeNode(TTree *tree, TreeNode* node){
	// Check arguments
	if(tree == NULL || node == NULL) return;

	// Use object methods to de-alocate node fields
	tree->destroyElement(node->elem);
	tree->destroyInfo(node->info);

	// De-allocate the node
	free(node);
}

//Functie auxiliara de inserarea a nodului in lista
void list_insert(TTree* tree, TreeNode *node){
	TreeNode *aux = tree->root;

	node->end = node;
	if (aux == NULL)
		return;

	//Tratarea cazului in care nodul e radacina
	if (tree->size == 1){
		node->next = NULL;
		node->prev = NULL;

		return;
	}

	//Tratarea cazului in care se insereaza elementul minim
	if (node == minimum(aux)){
		node->next = successor(node);
		successor(node)->prev = node;
		node->prev = NULL;
	}

	//Tratarea cazului in care se insereaza elementul maxim
	else if (node == maximum(aux)){

		node->next = NULL;
		node->prev = predecessor(node);
		predecessor(node)->next = node;
	}

	else {

		node->next = successor(node);
		node->prev = predecessor(node);
		successor(node)->prev = node;
		predecessor(node)->next = node;
	}
}

//Functie de inserarea a unui nod
void insert(TTree* tree, void* elem, void* info) {

	TreeNode* parent;
	TreeNode* node  = createTreeNode(tree, elem, info); //Crearea nodului nou
	TreeNode* current_node = tree->root; //Pointer de parcurgere a arborelui
	TreeNode* duplicate = search(tree, node, elem); 

	if (node == NULL)
		return;

	if (isEmpty(tree)){
		tree->root = node;
		tree->size++;

		list_insert(tree, node);
		return;
	}

	//Tratarea cazului in care exista duplicate ale nodului ce se vrea inserat
	if (duplicate != NULL){

		//Inserarea noului nod la sfarsitul secventei de duplicate
		duplicate->end->next = node;
		node->prev = duplicate->end;
		node->end = node;
		duplicate->end = node;

		//Verificarea daca nu e ultimul element din lista
		if (successor(duplicate) != NULL){

			node->end->next = successor(duplicate);
			successor(duplicate)->prev = node->end;
		}

		TreeNode *aux = node;

		//Parcurgerea listei inapoi, pana la primul element din secventa 
		//Restabilirea pointerului de sfarsit
		while (tree->compare(aux->elem, node->elem) == 0 && aux->prev != NULL){
			aux->end = node;
			aux = aux->prev;
		}

		return;
	}
	else { 

		//Inserarea nodului in arbore
		while (current_node != NULL){

			//Identificarea locului de inserare
			parent = current_node;
			if (tree->compare(elem, current_node->elem) == 1){
				current_node = current_node->rt;
			}

			else {
				current_node = current_node->lt;
			}
		}

		//Inserearea la dreapta parintelui
		if (tree->compare(elem, parent->elem) == 1){
			node->pt = parent;
			parent->rt = node;
			tree->size++;
			parent->height++;

		}

		//Inserarea la stanga parintelui
		else {
			node->pt = parent;
			parent->lt = node;
			tree->size++;
			parent->height++;
		}
	}

	//Reechilibrarea arborelui
	avlFixUp(tree, parent);
	//Stabilirea legaturilor din lista
	list_insert(tree, node);
}

//Functia de stergere a unui nod
void delete(TTree* tree, void* elem){

	TreeNode *node;
	TreeNode *aux = tree->root; //Pointer de parcurgere a arborelui
	TreeNode *parent; 
	TreeNode *root;
	TreeNode *pivot;
	TreeNode *parent_del; //Pointer catre parintele nodului ce trebuie sters

	if (tree->root == NULL)
		return;

	aux = search(tree, node, elem);

	//Tratarea cazului in care nu exista niciun element
	if (aux == NULL){
		tree->size--;

		return;
	}

	//Tratarea cazului in care exista duplicate ale unui element
	if (aux->end != aux){

		//Cazul in care stergerea se face la mijlocul listei
		if (aux->end->next != NULL && aux->prev != NULL){

			aux->end->prev->next = successor(aux);
			successor(aux)->prev = aux->end->prev;
			TreeNode *temp = aux;
			TreeNode *current = aux->end->prev;

			//Eliberarea memoriei pentru nodul sters
			destroyTreeNode(tree, aux->end);

			//Restabilirea pointerului de end
			while(tree->compare(temp->elem, aux->elem) == 0 && temp->prev != NULL){
				temp->end = current;
				temp = temp->prev;
			}

		}
		
		///Cazul in care stergerea se face la sfarsitul listei
		else if(aux->end->next == NULL && aux->prev != NULL){
			
			aux->end->prev->next = NULL;
			TreeNode *temp = aux;
			TreeNode *current = aux->end->prev;
			
			destroyTreeNode(tree, aux->end);

			//Restabilirea pointerului de end
			while(tree->compare(temp->elem, aux->elem) == 0 && temp->prev != NULL){
				temp->end = current;
				temp = temp->prev;
			}
		}

		//Cazul in care stergerea se face la inceputul listei
		else if(aux->end->next != NULL && aux->prev == NULL){

			aux = aux->end;
			aux->prev->next = aux->next;

			//Verificarea existentei unui succesor
			if (aux->next != NULL)
				aux->next->prev = aux->prev;

			TreeNode *current = aux->end->prev;
			TreeNode *temp = aux->end->prev;

			destroyTreeNode(tree, aux->end);

			//Restabilirea pointerului de end
			while(temp != NULL && tree->compare(temp->elem, current->elem) == 0){
				temp->end = current;
				temp = temp->prev;
			}
		}

		return;
	}
	//Tratarea cazului in care nu exista duplicate
	else {

		//Cazul in care stergerea se face la mijloc
		if (aux->prev != NULL && aux->next != NULL){
			aux->prev->next = aux->next;
			aux->next->prev = aux->prev;
		}

		//Cazul in care stergerea se face la sfarsit
		else if (aux->prev != NULL && aux->next == NULL){
			aux->prev->next = NULL;
		}

		//Cazul in care stergerea se face la inceput
		else if (aux->prev == NULL && aux->next  != NULL){
			aux->next ->prev = NULL;
		}

	}

	//Stergerea unei frunze
	if (aux->lt == NULL && aux->rt == NULL){

		//Cazul in care se sterge radacina
		if (aux->pt == NULL){

			tree->root = NULL;
			destroyTreeNode(tree, aux);
			tree->size--;
			return;
		}

		//Stergerea unei din stanga parintelui
		parent = aux->pt;
		if (tree->compare(aux->elem, parent->elem) == -1){
			parent->lt = NULL;

		}

		//Stergerea unei frunze din dreapta parintelui
		if (tree->compare(aux->elem, parent->elem) == 1){
			parent->rt = NULL;
		}

		/*Reactualizarea inaltimii arborelui;
		Reechilibrarea arborelui;
		Eliberearea memoriei pentru frunza stearsa*/
		updateHeight(tree->root);
		avlFixUp(tree, parent);
		destroyTreeNode(tree, aux);
		tree->size--;
		return;
	}

	//Stabilirea pivotului ce trebuie mutat
	pivot = aux->rt;
	//Stabilirea parintelui nodului ce trebuie sters
	parent_del = aux->pt;

	//Efectuarea algoritmului de stergere cu pivot la dreapta
	//Tratarea cazului in care pivotul nu are subarbore stang
	if (pivot->lt == NULL){

		pivot->lt = aux->lt;

		//Daca nodul pe care il sterg are ceva in stanga
		if (aux->lt != NULL)
			aux->lt->pt = pivot;

		//Daca pivotul nu se afla in dreapta nodului pe care il sterg
		if (aux->rt != pivot){
			pivot->lt = aux->lt;
			aux->lt->pt = pivot;
		}

		pivot->pt = aux->pt;

		//Daca nodul pe care il sterg e radacina
		if (parent_del == NULL){
			tree->root = pivot;
		}

		//Identificarea pozitiei in care trebuie modificat pivotul
		else if (tree->compare(pivot->elem, parent_del->elem) == 1)
			parent_del->rt = pivot;
		else
			parent_del->lt = pivot;

		/*Reactualizarea inaltimii arborelui;
		Reechilibrarea arborelui;
		Eliberearea memoriei pentru frunza stearsa*/
		updateHeight(tree->root);
		avlFixUp(tree, pivot);
		tree->size--;
		destroyTreeNode(tree, aux);
		return;
	}

	//Identificarea pivotului 
	while(pivot->lt != NULL){
		pivot = pivot->lt; 
	}

	//Stabilirea parintelui pivotului
	parent = pivot->pt;

	//Cazul in care elementul de stergere este radacina
	if (parent_del == NULL){
		tree->root = pivot;
	}

	//Stabilirea locului de inserare a pivotului
	else if (tree->compare(pivot->elem, parent_del->elem) == 1)
		parent_del->rt = pivot;
	else if (tree->compare(pivot->elem, parent_del->elem) == -1)
		parent_del->lt = pivot;

	//Cazul in care nodul pe care il sterg nu este radacina arborelui
	if (parent_del != NULL){
		pivot->pt = parent_del;
		parent->lt = pivot->rt;
	}

	//Crearea legaturilor dintre pivot si parintele nodului sters
	pivot->pt = parent_del;
	parent->lt = pivot->rt;

	//Cazul in care pivotul are un subarbore drept
	if (pivot->rt != NULL)
		pivot->rt->pt = parent;

	//Pozitionarea pivotului
	pivot->rt = aux->rt;
	pivot->lt = aux->lt;

	//Cazul in care nodul pe care il sterg are un subarbore drept
	if (aux->rt != NULL)
		aux->rt->pt = pivot;

	//Cazul in care nodul pe care il sterg are un subarbore stang
	if (aux->lt != NULL)
		aux->lt->pt = pivot;

	/*Reactualizarea inaltimii arborelui;
	Reechilibrarea arborelui;
	Eliberearea memoriei pentru frunza stearsa*/
	destroyTreeNode(tree, aux);
	updateHeight(tree->root);
	avlFixUp(tree, parent);
	tree->size--;
}


//Functia de distrugere a arborelui
void destroyTree(TTree* tree){
	TreeNode *aux = minimum(tree->root);//Pointer catre primul element al listei
	TreeNode *temp; 

	if (tree == NULL)
		return;

	//Parcurgerea listei si distrugerea fiecarui nod
	while(aux != NULL){
		temp = aux->next;
		destroyTreeNode(tree, aux);
		aux = temp;
	}

	//Eliberearea memoriei pentru arbore
	free(tree);

}


#endif /* TREEMAP_H_ */
