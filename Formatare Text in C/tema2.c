#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

// Functie de stergere a spatiilor de la sfarsitul liniilor
void white_space(char original[][1000], int original_line_count) {
  int i, j;

  // Pargurg fiecare caracter a randului de la coada la cap
  for (i = 0; i < original_line_count; i++) {
    for (j = strlen(original[i]) - 1; j >= 0; j--) {
      /*Daca gasesc un whtespace il inlocuiesc cu \n facandu-l ultimul caracter 
      din linie, daca nu e whitespace trec la urmatoarea linie*/
      if (isspace(original[i][j])) {
        original[i][j] = '\n';
        original[i][j + 1] = '\0';
      }
      else  
        break;
    }
  }
}

// Functie de aliniere la stanga
void left_align(char original[][1000], int start_line, int end_line) {
  int i, j, k = 0;

  // Parcurg fiecare liniile si elementele sale
  for (i = start_line; i <= end_line; i++) {
    for (j = 0; j <= strlen(original[i]); j++) {
      //Se numara cate caractere de whitesapce sunt la inceputul liniei
      if (isspace(original[i][j])) { 
        k++;        
      }
      else 
        break;
    }
    // Se realizeaza transferul caracterelor la stanga pentru fiecare linie 
    if (original[i][0] != '\n') {
      for (j = k; j <= strlen(original[i]); j++) {
        original[i][j - k] = original[i][j];
      }
    }
    k = 0;
  }
}

// Functie de aliniere la dreapta
void right_align(char original[][1000], int start_line, int end_line) {
  int i, j, a, max_length = 0;
   
  /*Se parcurge fiecare linie, incepand cu sfarsitul acesteia si se numara cate
  caractere de whitespace sunt la sfarsit*/
  for (i = start_line; i <= end_line; i++) {      
    // Se elimina trailing whitespace
    white_space(original, i);
  }

  //Se identifica cea mai lunga linie
  max_length = strlen(original[0]);
  for (i = start_line; i <= end_line; i++) {
    if (strlen(original[i]) > max_length) {
      max_length = strlen(original[i]);
    }
  }

  /*Se aliniaza liniile la dreapta in raport cu cea mai lunga linie,
  adaugandu-se spatii la stanga*/
  a = 0;
  for (i = start_line; i <= end_line; i++) {
    if (original[i][0] != '\n') {
      while (strlen(original[i]) != max_length) {
        for (j = strlen(original[i]) + 1; j >= 1; j--) {
          original[i][j] = original[i][j - 1]; 
        }
        original[i][a] = ' ';
        a++;
      }
    }
    a = 0;
  }
}

// Functie de aliniere la dreapta
void center_align(char original[][1000], int start_line, int end_line, 
  int original_line_count) {
  int i, j, max_length = 0, a = 0;

  for (i = start_line; i <= end_line; i++) {      
    // Se elimina trailing whitespace
    white_space(original, i);
  }

  max_length = strlen(original[0]);
  for (i = 0; i < original_line_count; i++) {
    if (strlen(original[i]) > max_length) {
      max_length = strlen(original[i]);
    }
  }
  
  /*Se aliniaza liniile la dreapta in raport cu cea mai lunga linie,
  adaugandu-se spatii la stanga pana cand numarul de spatii adaugate la inceput
  nu devine egal cu diferenta dintre lungimea liniei cea mai lungi si linia
  curenta*/
  for (i = start_line; i <= end_line; i++) {
    if (original[i][0] != '\n') {
      while (max_length - strlen(original[i]) > a) {
        for (j = strlen(original[i]) + 1; j > 0; j--) {
          original[i][j] = original[i][j - 1]; 
        }
        a++;
      }
      for (j = 0; j <= a - 1; j++) {
        original[i][j] = ' ';
      }
      a = 0;
    }
  }
}

// Functie de adaugare a paragrafelor
void parag_func(char original[][1000], int par_space, int start_line, 
  int end_line) {
  int i, j, c = 0, a = 0, new_par_sp;

  left_align(original, start_line, end_line);
  
  new_par_sp = par_space;
  for (i = start_line; i <= end_line; i++) {
    //Daca linia este prima din paragraf e mutata la stanga cu par_space unitati
    if (original[i - 1][0] == '\n' || i == 0) {
      for (c = new_par_sp; c > 1; c--) {
        for (j = strlen(original[i]) + 1; j >= 1; j--) {
          original[i][j] = original[i][j - 1]; 
        }
        a++;
        new_par_sp--;          
      }
      if (new_par_sp == 1) {
        for (j = strlen(original[i]) + 1; j >= 1; j--) {
          original[i][j] = original[i][j - 1]; 
        }
        a++;
      }
      for (j = 0; j <= a - 1; j++) {
        original[i][j] = ' ';
      }
      a = 0;
    }
    new_par_sp = par_space;
  }
}

// Functie de indexare a liniilor
void list_func(char original[][1000], char index, char index_ch, int start_line,
  int end_line) {
  int i, count = 0;
  char index1, int_string[1003], elements[4];
  
  left_align(original, start_line, end_line);
  
  for (i = start_line; i <= end_line; i++) {
    /*In functie de variabila index adaug la inceputul randului adaug un anumit 
    caracter (daca nu e b) urmat de index_ch un spatiu*/
    if (index == 'a') {
      index1 = 'a' + count;
      elements[0] = index1;
      elements[1] = index_ch;
      elements[2] = ' ';
      elements[3] = '\0';
      strcat(elements, original[i]);
      strcpy(original[i], elements);
    }

    else if (index == 'A') {
      index1 = 'A' + count;
      elements[0] = index1;
      elements[1] = index_ch;
      elements[2] = ' ';
      elements[3] = '\0';
      strcat(elements, original[i]);
      strcpy(original[i], elements);
    }

    else if (index == 'n') {
      sprintf(int_string, "%d", count + 1);                
      elements[0] = index_ch;
      elements[1] = ' ';
      elements[2] = '\0';
      strcat(int_string, elements);
      strcat(int_string, original[i]);
      strcpy(original[i], int_string);
    }
    
    else if (index == 'b') {
      elements[0] = index_ch;
      elements[1] = ' ';
      elements[2] = '\0';
      strcat(elements, original[i]);
      strcpy(original[i], elements);
    }
    count++;
  }
}

void org_list_func(char original[][1000], char index, char index_ch, 
  char index2, int start_line, int end_line) {
  int m = 0, j;
  char aux_str[1000];

  //Sortare alfabetica directa prin metoda sortarii prin selectie
  if (index2 == 'a') {
    for (m = start_line; m <= end_line - 1; m++) {
      for (j = m + 1; j <= end_line; j++) {
        if (strcmp(original[m], original[j]) > 0) {
          strcpy(aux_str, original[j]);
          strcpy(original[j], original[m]);
          strcpy(original[m], aux_str);
        }
      }
    }
  }
  //Sortare alfabetica inversa prin metoda sortarii prin selectie
  if (index2 == 'z') {
    for (m = start_line; m <= end_line - 1; m++) {
      for (j = m + 1; j <= end_line; j++) {
        if (strcmp(original[m], original[j]) < 0) {
          strcpy(aux_str, original[j]);
          strcpy(original[j], original[m]);
          strcpy(original[m], aux_str);
        }
      }
    }
  }
  
  left_align(original, start_line, end_line);
  list_func(original, index, index_ch, start_line, end_line);
}

// Functia ce realizeaza operatia justify
void justify_func(char original[][1000], int start_line, int end_line) {
  int i, j, max_length = 0;
  char buffer[1000]; 

  left_align(original, start_line, end_line);

  // Identific linia cea mai lunga
  max_length = strlen(original[0]);
  for (i = start_line; i <= end_line; i++) {
    if (strlen(original[i]) > max_length) {
      max_length = strlen(original[i]);
    }
  }
    
  for (i = start_line; i < end_line; i++) {
    // Verific daca linia nu este ultima din paragraf sau una goala
    if ((strlen(original[i]) > 1 && strlen(original[i + 1]) != 1)) {
      while (strlen(original[i]) != max_length) {
        for (j = 0; j <= strlen(original[i]); j++) {
          /*Daca gasesc un spatiu ce are in fata un alt caracter, contcatenez
          sirul de pana la acel caracter cu inca un spatiu, apoi adaug restul
          randului*/
          if (original[i][j - 1] == ' ' && original[i][j] != ' ') {
            strncpy(buffer, original[i], j);
            buffer[j] = '\0';
            strcat(buffer, " ");
            strcat(buffer, original[i] + j);
            memset(original[i], 0, strlen(original[i]));
            strcpy(original[i], buffer);
            if (max_length == strlen(original[i]))
              break;
            j++;
          }
        }
      } 
    }
  }
}

// Functia ce realizeaza operatia wrap 
int wrap_func(char original[1000][1000], int *original_line_count, 
  int limit_length) {
  int i, j, aux = 0, k, row = -1, a = 1, m = 0, new_count = 0, ln_1 = 0, z,
    passedChar = 0;
  char buffer[1000000], copy[1000][1000];

  /*Verifica daca fisierului i se poate aplica operatia wrap si realizeaza o
  compie a fisierului*/
  for (i = 0; i <= *original_line_count; i++) {
    if (strchr(original[i], ' ') == NULL && 
        strlen(original[i]) > limit_length) {
      printf("Cannot wrap!\n");
      return 1;
    }
    memcpy(copy[i], original[i], 1000);
    memset(original[i], 0, 1000);
  }

  while (ln_1 != *original_line_count) {
    k = -1;
    a = 1;
    m++;
    new_count = m;
    memset(buffer, 0, 1000000);
    for (i = ln_1; i < *original_line_count; i++, passedChar++) {
      // Se inscrie, pe rand, fiecare paragraf intr-un vector unidimensional 
      // buffer
      if (i != ln_1) {
        left_align(copy, i, i);
      }
      if (passedChar > limit_length) {
        return 1;
      }
      if (copy[i][0] == '\n' || i == *original_line_count - 1)
        ln_1 = i + 1;
      if (copy[i][0] == '\n') {
        passedChar = 0;
        break;
      }
      for (j = 0; j < strlen(copy[i]); j++) {
        k++;
        if (copy[i][j] == '\n') {
          buffer[k] = ' ';
        }
        else {
          buffer[k] = copy[i][j];
        }
      }
    }

    aux = limit_length;

    /*Se identifica secventele ce se incadreaza in limitele parametrului de la
    wrap si se insriu pe o lini din matricea originala numarul liniei 
    incrementandu-se dupa care aceasta secventa se sterge din buffer*/
    while (a) {
      row++;

      while (!isspace(buffer[aux])) {
        aux--;
      }

      strncpy(original[row], buffer, aux);
      original[row][strlen(original[row]) + 1] = '\0';
      original[row][strlen(original[row])] = '\n';

      for (z = 0; z <= strlen(buffer) - aux - 1; z++) {
        buffer[z] = buffer[z + aux + 1];
        if (buffer[z + aux + 1] == '\0')
          break;
      }

      aux = limit_length;
      if (strlen(buffer) <= aux) {
        row++;
        strncpy(original[row], buffer, strlen(buffer));
        a = 0;
      }
    }

    new_count = row + 1;
    row++;
    strcpy(original[row], "\n");
  }

  row++;
  strcpy(original[row], copy[*original_line_count]);
  *original_line_count = new_count;
  return 0;
}

// Functia ce executa operatiile din comanda
void exec_command(char *command, char original[1000][1000], 
  char result[1000][1000], int *original_line_count, int *flag) {
  char *seq = strtok(command, " ");
  char cmd = seq[0], *p, list_type, special_character, ordering;
  int start_line = 0, end_line = *original_line_count - 1, intend_length = 0, 
    count = 0, value;

  white_space(original, *original_line_count);

  /*In functie de variabila cmd, care indica ce operatie urmeaza sa fie
  executata, sunt inscrisi parametrii in variabile si apelata functia
  corespunzatoare fiecare-i comenzi, tratandu-se si cazul cand parametrii 
  sant gresiti*/
  if (strchr("Ww", cmd)) {
    seq = strtok(NULL, " ");
    value = atoi(seq); 
    seq = strtok(NULL, " ");
    if (seq) {
      printf("Invalid operation!\n");
      *flag = 1;
      return;
    }
    if (wrap_func(original, original_line_count, value)) {
      *flag = 1;
      return;    }
    white_space(original, *original_line_count);
  } 

  else if (strchr("LlRrCcJj", cmd)) {
    p = strtok(NULL, " ");
    if (p) {
      count++;
      start_line = atoi(p);
      p = strtok(NULL, " ");
      if (p) {
        count++;
        end_line = atoi(p);
      }
    }
    p = strtok(NULL, " ");
    if (end_line < start_line || end_line < 0 || start_line < 0 || p) {
      printf("Invalid operation!\n");
      *flag = 1;
      return;
    }

    if (count <= 2 && strchr("Cc", cmd))
      center_align(original, start_line, end_line, *original_line_count);
    else if (count <= 2 && strchr("Ll", cmd))
      left_align(original, start_line, end_line);
    else if (count <= 2 && strchr("Rr", cmd))
      right_align(original, start_line, end_line);
    else if (count <= 2 && strchr("Jj", cmd))
      justify_func(original, start_line, end_line);
    else if (count <= 3 && strchr("Pp", cmd))
      parag_func(original, intend_length, start_line, end_line);
    
  } 

  else if (strchr("Ii", cmd)) {
    p = strtok(NULL, " ");
    if (p && strlen(p) == 1) {
      count++;
      if (strchr("nabA", p[0]) == NULL) {
        printf("Invalid operation!\n");
        *flag = 1;
        return;
      }
      list_type = p[0];
      p = strtok(NULL, " ");
      if (p && strlen(p) == 1) {
        count++;
        special_character = p[0];
        p = strtok(NULL, " ");
        if (p) {
          count++;
          start_line = atoi(p);
          p = strtok(NULL, " ");
          if (p) {
            count++;
            end_line = atoi(p);
          }
        }
      } else {
        printf("Invalid operation!\n");
        *flag = 1;
        return;
      }
    }
    list_func(original, list_type, special_character, start_line, end_line);
  }

  else if (strchr("Oo", cmd)) {
    p = strtok(NULL, " ");
    if (p && strlen(p) == 1) {
      count++;
      list_type = p[0];
      p = strtok(NULL, " ");
      if (p && strlen(p) == 1) {
        count++;
        special_character = p[0];
        p = strtok(NULL, " ");
          if (p && strlen(p) == 1) {
            count++;
            ordering = p[0];
            if (strchr("az", p[0]) == NULL) {
              printf("Invalid operation!\n");
              *flag = 1;
              return;
            }
            p = strtok(NULL, " ");
            if (p) {
              count++;
              start_line = atoi(p);
              p = strtok(NULL, " ");
              if (p) {
                count++;
                end_line = atoi(p);
              }
            }
          } 
          else {
            printf("Invalid operation!\n");
            *flag = 1;
          }
        }
      }
    org_list_func(original, list_type, special_character, ordering, start_line, 
      end_line);
  }

  else if (strchr("Pp", cmd)) {
    p = strtok(NULL, " ");
    if (p) {
      count++;
      intend_length = atoi(p);
      p = strtok(NULL, " ");
      if (p) {
        count++;
        start_line = atoi(p);
        p = strtok(NULL, " ");
        if (p) {
          count++;
          end_line = atoi(p);
        }
      }
    } else {
      printf("Invalid operation!\n");
      *flag = 1;
    }
    parag_func(original, intend_length, start_line, end_line);
  } 
  else if (strchr("QETYUASDFGHKZXVBNM", toupper(cmd))) {
    printf("Invalid operation!\n");
    *flag = 1;
  }
}
    

int main(int argc, char const *argv[]) {
  char buf[1000],               // buffer used for reading from the file
      original[1000][1000],     // original text, line by line
      result[1000][1000],
      copy_original[1000][1000];     // text obtained after applying operations
  int original_line_count = 0,  // number of lines in the input file
      result_line_count = 0,    // number of lines in the output file
      i, copy_count = 0;

  if (argc != 4) {  // invalid number of arguments
    fprintf(stderr,
      "Usage: %s operations_string input_file output_file\n",
      argv[0]);
    return -1;
  }

  // Open input file for reading
  FILE *input_file = fopen(argv[2], "r");

  if (input_file == NULL) {
    fprintf(stderr, "File \"%s\" not found\n", argv[2]);
    return -2;
  }

  // Read data in file line by line
  //dest nr source
  while (fgets(buf, 1000, input_file)) {
    memset(original[original_line_count], 0, 1000);
    strcpy(original[original_line_count], buf);
    original_line_count++;
  }

  fclose(input_file);

  // Se realizeaza o copie a dimensiunii si elementelor matricii originale
  memcpy(copy_original, original, 1000 * 1000 * sizeof(char));
  copy_count = original_line_count;
  char *argv1 = strdup(argv[1]);
  
  int initialLength = strlen(argv1);
  char *seq = strtok(argv1, ",");
  int executed = 0;

  /*Transmit datele din comanda in functia exec_command pentru a fi executate
  operatiile, verificandu-se si ca numarul de operatii indeplinite sa nu
  depaseasca numarul de 10*/
  while (seq) {
    int flag = 0;
    int l = strlen(seq);
    exec_command(seq, original, result, &original_line_count, &flag);
    if (flag) {
      memcpy(original, copy_original, 1000 * 1000 * sizeof(char));
      original_line_count = copy_count;
      break;
    }
    if (seq + l + 1 < initialLength + argv1)
      seq = strtok(seq + l + 1, ",");
    else
      break;
    executed++;
    if (executed == 10) {
      break;
    }
  }
  free(argv1);

  /*Se inscrie in matricea se va transfera in output file datele cu operatiile 
  aplicate*/
  for (i = 0; i < original_line_count; i++) {
    strcpy(result[i], original[i]);
  }
  result_line_count = original_line_count;

  // Open output file for writing
  FILE *output_file = fopen(argv[3], "w");

  if (output_file == NULL) {
    fprintf(stderr, "Could not open or create file \"%s\"\n", argv[3]);
  return 0;
    return -2;
  }

  // Write result in output file
  for (i = 0; i < result_line_count; i++) {
    fputs(result[i], output_file);
  }

  fclose(output_file);
  return 0;
}
