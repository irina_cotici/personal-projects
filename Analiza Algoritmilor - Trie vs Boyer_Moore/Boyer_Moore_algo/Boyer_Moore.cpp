#include <bits/stdc++.h>
#include "Boyer_Moore.h"
using namespace std;
#include <iostream>
#include <fstream>
#define N 256

void BoyerMoore( string text, string pattern) {
   
    int patternLength = pattern.size() - 1;
    int textLength = text.size();
    int i = 0;
    int j = 0;
    int out[N];
    int s = 0;

    while ( i < N ) {
        out[i] = -1;
        i++;
    }

    while (j < patternLength) {
        out[(int) pattern[j]] = j;
        j++;
    }

    while(s <= (textLength - patternLength)) {

        int k = patternLength - 1;

        while(k >= 0 && pattern[k] == text[s + k]) {

            k--;
        }

        if(k < 0) {
            std::cout << "Cuvantul a fost gasit pe pozitia <" <<  s << "> in textul cautat."<< endl;

            if (s + patternLength < textLength)
                s += patternLength-out[text[s + patternLength]];
            else
                s += 1;
        }

        else
            s += max(1, k - out[text[s + k]]);
    }
}
