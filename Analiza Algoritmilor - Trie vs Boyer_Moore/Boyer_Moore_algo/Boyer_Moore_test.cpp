#include <bits/stdc++.h>
#include "Boyer_Moore.h"
using namespace std;
#include <iostream>
#include <fstream>
#include <time.h> 
int main() {

    string text;
    string pattern;
    std::ifstream file("comon_tests/Test_10.in");
    std::string str;

    if (getline(file, str)) {
            pattern = str;
    }

    while (std::getline(file, str)) {
            text = str;
    }

    BoyerMoore(text, pattern);
    return 0; 
}