								= Tema 1 =
===============================================================================
 Nume, prenume: COTICI Irina
 Grupa: 321 CD
 Materie: Analiza Algoritmilor
 
 In aceasta etapa, arhiva incarcata contine fisierele ce constituie 
 implementarea celor doi algoritmi alesi. Asadar, aici sunt continute doua
 foldere:
 	- Boyer_Moore_algo
 	- Trie_algo
In aceste doua directoare se afla fisierele necesare rezolvarii temei:
- Boyer_Moore_algo: 

		* Boyer_Moore.h - in acest fisier este mentionata semnatura functiei
		Boyer_Moore.

		* Boyer_Moore.cpp - reprezinta fisierul implementarii functiei 
		mentionate mai sus. Aceasta primeste ca parametru doua stringuri: 
		secventa cautata si textul in care are loc cautarea. In cazul in
		care este gasita secventa, are loc afisarea la ecran pozitia de pe
		care incepe secventa cautata. 

		* Boyer_Moore_test.cpp - aici se regaseste functia main in care are
		loc citirea din fisier a datelor de intrare, mai exact, a pattern-ului
		cautat si a textului in care are loc cautarea. De asemenea, in main 
		are loc apelarea functiei din fisierul precedent.

		* in - un folder in care se afla 9 fisiere de intrare. Acestea contin 
		pe prima linie cuvantul dat spre cautare, iar in continuare - textul.

		* out - un folder ce contine 9 fisiere cu output-ul in urma rularii
		programului pe fisierele de intrare.

		* Makefile - fisier in care exista o regula build a obiectelor 
		Boyer_Moore_test.o Boyer_Moore.o.

- Trie_algo: 
		
		* Trie.h - acest fisier contine antetul functiilor "adauga", "preiaNod"
		si "cauta".

		* Trie.cpp - aici are loc implementarea functiilor de mai sus. In
		primul rand, fisierul contine definirea structurii mele ce reprezinta 
		radacina. Functia "preiaNod" creeaza un nod nou. Functia "adauga" 
		primeste ca parametru radacina si unul dintre cuvintele din textul 
		in care are loc cautarea, iar in interiorul acesteia are loc inserarea 
		cuvantului in arbore. In functia "cauta", dupa primirea ca parametru a 
		radacinii si a cheii cautate, are loc identificarea cheii in arbore.
		Daca cheia a fost gasita, functia returneaza true.

		* Trie_test.cpp - incadreaza acest fisier functia main, care realizeaza
		citirea din input si face apelul functiilor anterioare.

		* in - intruneste 9 teste de input.

		* out - reprezinta un folder cu 9 rezultate ale testelor anterioare.

		* Makefile - regaseste regula de build a fisierelor Trie_test.o Trie.o.