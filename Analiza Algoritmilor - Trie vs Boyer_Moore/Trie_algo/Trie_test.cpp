#include <bits/stdc++.h>
#include "Trie.h"
#define N 100000
#define alpha 30
int main() {

    ifstream file;
    file.open ("comon_tests/Test_10.in");
    if (!file.is_open()) return 1;

    string chei[N];
    string word;
    string pattern;
    int i = 0;
    file >> pattern;

    while (file >> word) {   
        chei[i] = word;
        i++;
    }
    struct Nodul *radacina = preiaNod();
    for (int i = 0; i < N; i++) {
        if (chei[i] != ""){
            adauga(radacina, chei[i]);
        }
    }
    cauta(radacina, pattern)? std::cout << "Cuvantul <" << pattern <<"> e gasit\n" :
                         cout << "Cuvantul <" << pattern << "> nu e gasit\n";
    return 0;                     
}
