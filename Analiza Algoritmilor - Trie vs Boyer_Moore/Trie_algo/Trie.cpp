#include <bits/stdc++.h>
#include "Trie.h"
using namespace std;
#define N 100000
#define alpha 30

struct Nodul
{
    struct Nodul *copil[alpha];
    bool sfarsitCuvant;
};

struct Nodul *preiaNod(void)
{
    struct Nodul *start =  new Nodul;

    start->sfarsitCuvant = false;
    int i = 0;
    while (i < alpha)
    {
        start->copil[i] = NULL;
        i++;
    }
    return start;
}

void adauga(struct Nodul *radacina, string cheia)
{
    struct Nodul *smerge = radacina;
    int i = 0;
    while ( i < cheia.length())
    {
        int index = cheia[i] - 'a';
        if (!smerge->copil[index])
            smerge->copil[index] = preiaNod();
        smerge = smerge->copil[index];
        i++;
    }

    smerge->sfarsitCuvant = true;
}

bool cauta(struct Nodul *radacina, string cheia)
{
    struct Nodul *smerge = radacina;
    int i = 0;
    while (i < cheia.length())
    {
        int index = cheia[i] - 'a';
        if (!smerge->copil[index])
            return false;
        smerge = smerge->copil[index];
        i++;
    }
    return (smerge != NULL && smerge->sfarsitCuvant);
}
