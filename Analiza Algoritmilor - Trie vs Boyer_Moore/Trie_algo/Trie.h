#ifndef TRIE_H
#define TRIE_H
#include <bits/stdc++.h>
using namespace std;
#define N 100000
struct Nodul *preiaNod(void);
void adauga(struct Nodul *radacina, string cheia);
bool cauta(struct Nodul *radacina, string cheia);
#endif