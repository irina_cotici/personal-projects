#ifndef FOOTBALL_CLUB_H_D
#define FOOTBALL_CLUB_H_
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// List node which contains information about one football player.
typedef struct Player {
	char *name;					// player's name
	char *position;				// player's game position
	int score;					// player's score
	int injured;				// indicator for injury (1 true, 0 false)
	struct Player *next;		// next list node
	struct Player *prev;		// previous list node
} Player;

// Structure which contains information about one football club.
typedef struct FootballClub {
	char *name;					// club's name
	Player *players;			// list with players in good shape
	Player *injured_players;	// list with injured players
	struct FootballClub *next;	// next list node
} FootballClub;

//Antetul functiilor implementate de mine, pentru modularizarea codului.
int club_list_length(FootballClub *list);
int players_list_length(Player *list);
void print_players(Player *player);
FootballClub *get_club(FootballClub *club, char *name);
Player *get_player(Player *player, char *name);
void rem(Player **list, Player **player);

//Functie ce determina marimea unei liste de cluburi.
int club_list_length(FootballClub *list){
	int length = 0;

	if(list == NULL) {
		return 0;
	}

	FootballClub *aux = list;

	while (aux != NULL) {
		length++;
		aux=aux->next;
	}

	return length;
}

//Functie ce determina marimea unei liste de jucatori.
int players_list_length(Player *list){
	int length = 0;
	if (list == NULL) {
		return 0;
	}

	Player *aux = list;
	//printf("hahahhaah luzerilor\n");

	while (aux != NULL) {
		length++;
		aux = aux->next;
	}
	//printf("tat norm\n");
	return length;
}

//Functie auxuliara ce permite extragerea unui club specific.
FootballClub *get_club(FootballClub *club, char *name){
	int i, j = club_list_length(club);

	for (i = 0; i < j; i++){

		if (strcmp(club->name, name) == 0){
			return club;
		}

		club = club->next;
	}
	return NULL;
}

//Functie auxiliara ce permite extragerea unui jucator specific.
Player *get_player(Player *player, char *name) {

	if (!player) {
		return NULL;
	}

	while (player != NULL && strcmp(player->name, name) != 0 ) {

		// if (player->next == NULL && strcmp(player->name, name) != 0) {
		// 	return NULL;
		// }
		player = player->next;
	}

	return player;
}
//Functie ce sterge un jucator din lista acestora.
void rem(Player **list, Player **player){

	Player *aux = *player;
	if (*list == NULL)	 					/*Cazul in care nu exista niciun 
											jucator*/
		return;
	
	if (players_list_length((*list)) == 1){		/*Cazul in care exista un singur
											jucator*/
		*list = NULL;
	}
	else if (aux->prev == NULL){			/*Cazul in care se sterge primul
											jucator*/
		aux->next->prev = NULL;
		*list = aux->next;

		
	}
	else if (aux->next == NULL){			/*Cazul in care se sterge ultimul
											jucator*/
		aux->prev->next = NULL;
	}
	else {
		aux->prev->next = aux->next;		/*Cazul in care se sterge un
											jucator aflat in interiorul listei*/
		aux->next->prev = aux->prev;
	}

	return;
}

//Functia de initializare a cluburilor.
FootballClub *initialize_clubs(int clubs_no, char **names) {
	int i;
	FootballClub *clubs = NULL, *temp, *head;

	if (clubs_no == 0)
		return NULL;
	for (i = 0; i < clubs_no; i++) {
		if (clubs == NULL) {
			clubs = (FootballClub *)malloc(sizeof(FootballClub));
			if (clubs == NULL)
				return NULL;
			head = clubs;
			temp = clubs;
		}
		else {
			clubs = clubs->next;
			clubs = (FootballClub *)malloc(sizeof(FootballClub));
			if (clubs == NULL)
				return NULL;
			
			temp->next = clubs;
			temp = temp->next;
		}	
		clubs->players = NULL;	
		clubs->injured_players = NULL;
		clubs->name = (char*)malloc((strlen(names[i]) + 1) * sizeof(char));
		if (clubs->name == NULL)
			return NULL;

		strcpy(clubs->name, names[i]);

	}
	clubs->next = NULL;
	clubs->players = NULL;	
	clubs->injured_players = NULL;
	
	return head;

}
//Functia de adaugare a unui club.
FootballClub *add_club(FootballClub *clubs, char *name) {
	if (clubs == NULL){
		clubs = initialize_clubs(1, &name);
		return clubs;
	}
	else { 
		FootballClub *aux, *temp = clubs;
		aux = initialize_clubs(1, &name);

		while (temp->next != NULL){
			temp = temp->next;
		}
		temp->next = aux;
		aux->next = NULL;

		return clubs;
	}
}

//Functie auxiliara care adauga un jucator intr-o lista.
void add(Player **list, Player **player){
	int i, count = 0;
	Player *aux = *list, *temp;

		//printf("acest caz levii\n");						
	int x = players_list_length(*list);
	if (*list == NULL){				
		(*player)->next = NULL;
		(*player)->prev = NULL;
		*list = *player;

		return;
	}
	else {			
											/*Se parcurge lista in toate cele 
											trei cazuri care nu satisfac 
											conditia de sortare.*/

		while (aux != NULL && strcmp(aux->position,
			(*player)->position) < 0 ){

			count++;
			aux = aux->next;
		}

		while (aux != NULL && aux->score > (*player)->score){
			if (strcmp(aux->position,(*player)->position) != 0) {
				break;
			}
			count++;
			aux = aux->next;
		}

		while (aux != NULL && strcmp(aux->name,
			(*player)->name) < 0 ){
			
			if (strcmp(aux->position,(*player)->position) != 0 ||
				aux->score != (*player)->score) {
				
				break;
			}

			count++;
			aux = aux->next;
		}									/*Cazul in care jucatorul devine 
											singurul element*/
		temp = *list;

		if (count == 0){
			(*player)->next = temp;
			(*player)->prev = NULL;
			temp->prev = *player;
			*list = *player;
			return;

		}

		for (i = 0; i < count - 1; i++){
			temp = temp->next;
		}									/*Cazul in care jucatorul se 
											insereaza ca ultim element*/
		if (count == x){
			(*player)->prev = temp;
			(*player)->next = NULL;
			temp->next = *player;
			return;
		}
		else {								/*Cazul in care jucatorul se 
											insereaza in interiorul listei*/

			(*player)->prev = temp;
			(*player)->next = temp->next; 
			temp->next->prev =  (*player);
			temp->next = (*player);
			return;
		}



	}
}

//Functia de adaugare a unui jucator intr-o lista.
void add_player(FootballClub *clubs, char *club_name, 
				char *player_name, char *position, int score) {
											/*Se verifica existenta clubului*/
	if (get_club(clubs, club_name) == NULL)
		return;

	FootballClub *aux = get_club(clubs,club_name);
	Player *temp = (Player *)malloc(sizeof(Player));
	if (temp == NULL)
		return;
											/*Se initializeaza informatia din 
											lista*/
	temp->name = (char*)malloc((strlen(player_name) + 1) * sizeof(char));
	if (temp->name == NULL)
		return;

	memcpy(temp->name, player_name, strlen(player_name) + 1);
	
	temp->position = (char*)malloc((strlen(position) + 1) * sizeof(char));
	if (temp->position == NULL)
		return;

	memcpy(temp->position, position, strlen(position) + 1);
	temp->score = score;
	temp->injured = 0;

	temp->next = NULL;
	temp->prev = NULL;
	add(&aux->players, &temp);
}

//Functia de transferare a unui jucator dintr-un club in altul.
void transfer_player(FootballClub *clubs, char *player_name, 
					char *old_club, char *new_club) {
	FootballClub *old_c, *new_c;	

	if (get_club(clubs, old_club) == NULL || get_club(clubs, new_club) == NULL){
		return;
	}

	old_c = get_club(clubs, old_club);
	new_c = get_club(clubs, new_club);


	Player *player = get_player(old_c->players, player_name);
	if (player == NULL){
		player = get_player(old_c->injured_players, player_name);
		if (player == NULL) {
			return;
		}
		rem(&old_c->injured_players, &player);	
		add(&new_c->injured_players, &player);
		return;
	}
	rem(&old_c->players, &player);	
	add(&new_c->players, &player);

		


}

//Functia de stergere a unui jucator dintr-un club.
void remove_player(FootballClub *clubs, char *club_name, char *player_name) {
	FootballClub *club = get_club(clubs, club_name);

	if (club == NULL)
		return;

	Player *player = get_player(club->players, player_name);
	
	if (player == NULL){
		player = get_player(club->injured_players, player_name);
		if (player == NULL){
			
			return;
		}
		rem(&club->injured_players, &player);
		free(player->position);
		free(player->name);
		free(player);
		
		return;
	}

	if (player != NULL ){

		rem(&club->players, &player);
		free(player->position);
		free(player->name);
		free(player);
	}
}

//Functia de modificare a scorului.
void update_score(FootballClub *clubs, char *club_name, 
					char *player_name, int score) {
	FootballClub *club = get_club(clubs, club_name);

	if (club == NULL)
		return;

	Player *player = get_player(club->players, player_name);

	if (player == NULL){
		player = get_player(clubs->injured_players, player_name);
		if (player == NULL) {
			return;
		}
		player->score = score;
		rem(&clubs->injured_players, &player);	
		add(&clubs->injured_players, &player);
		return;
	}

	player->score = score;
										/*Se sterge jucatorul cu vechiul scor, 
										adaugandu-se, din nou, cu scor 
										reinitializat*/
	rem(&club->players, &player);
	add(&club->players, &player);

}

void update_game_position(FootballClub *clubs, char *club_name, 
							char *player_name, char *position, int score) {
	FootballClub *club = get_club(clubs, club_name);
	if (club == NULL)
		return;
	Player *player = get_player(club->players, player_name);
	if (player == NULL)
		return;
	free(player->position);
	player->position = (char *)calloc(strlen(position) + 1, sizeof(char));
	memcpy(player->position, position, strlen(position));
	player->score = score;
	rem(&club->players, &player);
	add(&club->players, &player);

}

void add_injury(FootballClub *clubs, char *club_name,
				char *player_name, int days_no) {
	FootballClub *club = get_club(clubs, club_name);
	if (club == NULL)
		return;

	Player *player = get_player(club->players, player_name);
	if (player == NULL)
		return;

	rem(&club->players, &player);
	if (player->score -0.1*days_no > -100){
		player->score = player->score - 0.1*days_no;
	}
	else{
		player->score = -100;
	}
	player->injured = 1;
	add(&club->injured_players, &player);
}

void recover_from_injury(FootballClub *clubs, char *club_name, 
							char *player_name) {
	FootballClub *club = get_club(clubs, club_name);
	if (club == NULL)
		return;

	Player *player = get_player(club->injured_players, player_name);
	if (player == NULL)
		return;

	rem(&club->injured_players, &player);
	player->injured = 0;
	add(&club->players, &player);


}

// Frees memory for a list of Player.
void destroy_player_list(Player *player) {
	Player *aux = player;
	while (aux != NULL) {
		aux = aux->next;
		free(player->name);
		free(player->position);
		free(player);
		player = aux;
	}
}

// Frees memory for a list of FootballClub.
void destroy_club_list(FootballClub *clubs) {
	FootballClub *aux = clubs;
	while (aux != NULL) {
		aux = aux->next;
		destroy_player_list(clubs->players);
		destroy_player_list(clubs->injured_players);
		free(clubs->name);
		free(clubs);
		clubs = aux;
	}
}
// Displays a list of players.
void show_list(FILE *f, Player *players, int free_memory) {
	fprintf(f, "P: ");
	Player *player = players;
	while (player) {
		fprintf(f, "(%s, %s, %d, %c) ", 
			player->name,
			player->position,
			player->score,
			player->injured ? 'Y' : '_');
		player = player->next;
	}
	if (free_memory) {
		destroy_player_list(players);
	}
	fprintf(f, "\n");
}

// Displays a list of players in reverse.
void show_list_reverse(FILE *f, Player *players, int free_memory) {
	fprintf(f, "P: ");
	Player *player = players;
	if (player) {
		while (player->next) {
			player = player->next;
		}
		while (player) {
			fprintf(f, "(%s, %s, %d, %c) ", 
				player->name,
				player->position,
				player->score,
				player->injured ? 'Y' : '_');
			player = player->prev;
		}
	}
	if (free_memory) {
		destroy_player_list(players);
	}
	fprintf(f, "\n");
}


// Displays information about a football club.
void show_clubs_info(FILE *f, FootballClub *clubs) {
	fprintf(f, "FCs:\n");
	while (clubs) {
		fprintf(f, "%s\n", clubs->name);
		fprintf(f, "\t");
		show_list(f, clubs->players, 0);
		fprintf(f, "\t");
		show_list(f, clubs->injured_players, 0);
		clubs = clubs->next;
	}
}

#endif // FOOTBALL_CLUB_H_INCLUDED