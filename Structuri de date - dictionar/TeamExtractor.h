#ifndef TEAM_EXTRACTOR_H_D
#define TEAM_EXTRACTOR_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "FootballClub.h"

//Antetul functiilor auxiliare.
Player *get_max_players(Player *list, int num);
Player *copy_list(Player *list);
void add_best(Player **list, Player **player);

//Functie auxiliara ce returneaza un numar specific de jucatori dintr-o lista.
Player *get_max_players(Player *list, int num) {
	Player *new_node1, *new_node2, *new_list1 = NULL;

	new_node1 = list;
	while (new_node1 != NULL && num != 0){

		new_node2 = copy_list(new_node1);
		add_best(&new_list1, &new_node2);
		new_node2 = new_node2->next;;
		new_node1 = new_node1->next;
		num--;
	}

	return new_list1;
}


//Functie auxiliara ce copiaza informatia intr-un nod nou.
Player *copy_list(Player *list){

	if (list == NULL)
		return NULL;

	Player *aux = (Player *)malloc(sizeof(Player));
	if (aux == NULL)
		return NULL;

	aux->name = (char *)malloc((strlen(list->name) + 1)*sizeof(char));
		
	if (aux->name == NULL)
		return NULL;
	aux->position = (char *)malloc((strlen(list->position) + 1)*sizeof(char));
		
	if (aux->position == NULL)
		return NULL;
	
	memcpy(aux->name, list->name, strlen(list->name) + 1);
	memcpy(aux->position, list->position, strlen(list->position) + 1);
	aux->score = list->score;
	aux->injured = list->injured;
	aux->next = NULL;
	aux->prev = NULL;

	return aux;

}

/*Functie auxiliara ce adauga un jucator intr-o lista, fara a se lua in 
consideratie criteriul de pozitie*/
void add_best(Player **list, Player **player){
	int count = 0;
	Player *aux = *list;

	if (*list == NULL){

		(*player)->next = NULL;
		(*player)->prev = NULL;
		*list = *player;
		return;
	}
	else {
		//Se identifica pozitia in care sa se adauge, in mod ordonat, jucatorul.
		while (aux != NULL && aux->score > (*player)->score){
			count++;
			aux = aux->next;
		}

		while (aux != NULL && strcmp(aux->name,
			(*player)->name) < 0 && aux->score == (*player)->score){

			count++;
			aux = aux->next;
		}
		if (aux == NULL) {
			aux = *list;
			while (aux->next) {
				aux = aux->next;
			}

			aux->next = *player;
			(*player)->prev = aux;
			(*player)->next = NULL;
		} else if (aux == *list) {
			(*player)->next = *list;
			(*player)->prev = NULL;
			(*list)->prev = (*player);
			*list = *player;
		} else {
			(*player)->prev = aux->prev;
			(*player)->next = aux;
			aux->prev->next = (*player);
			aux->prev = *player;
		} 
	}
}

/*Functie ce intoarce lista jucatorilor neaccidentati din cele doua cluburi pe
care le primeste ca parametri*/
Player *union_teams(FootballClub *clubs, char *club_A, char *club_B) {
	FootballClub *club1, *club2, *aux1, *aux2;
	Player *new_node, *new_node1, *new_node2, *new_list = NULL;

	if (get_club(clubs, club_A) == NULL || get_club(clubs, club_B) == NULL)
		return NULL;
	//Se identifica cluburile.
	club1 = get_club(clubs, club_A);
	club2 = get_club(clubs, club_B);

	if (club1->players == NULL && club2->players == NULL )
		return NULL;

	aux1 = club1;
	if ( aux1 != NULL){
		new_node = aux1->players;
		
		while (new_node!= NULL){				/*Crearea listei noi cu jucatori 
												neaccidentati din club_A*/
			new_node1 = copy_list(new_node);
			add(&new_list, &new_node1);
			new_node1 = new_node1->next;
			new_node = new_node->next;
		}
		aux1 = aux1->next;
	}

	aux2 = club2;

	if ( aux2 != NULL){
		new_node2 = club2->players;
		while (new_node2!= NULL){				/*Adaugarea jucatorilor (club_B)
												neaccidentati in lista*/
			new_node1 = copy_list(new_node2);
			add(&new_list, &new_node1);
			new_node1 = new_node1->next;
			new_node2 = new_node2->next;
		}
	}

	return new_list;
}

//Functie ce returneaza cel mai bun jucator (ca scor), in dependenta de pozitie.
Player *get_best_player(FootballClub *clubs, char *position) {
	if (clubs == NULL){
		return NULL;
	}

	Player *max, *temp;
	FootballClub *aux = clubs;
	while (clubs != NULL && clubs->players == NULL) {
		clubs = clubs->next;
	}

	max = clubs->players;
	/*Se identifica jucatorul cu cel mai mare scor, in cazul in care corespunde
	pozitia, utilizand metoda de identificare a elementului maxim prin 
	comparatia cu fiecare element*/
	while (aux != NULL){
	temp = aux->players;
		while (temp != NULL){
			if (strcmp(temp->position, position) == 0 &&
				strcmp(max->position, position) != 0){
				max = temp;
			}
			if ((strcmp(temp->position, position) == 0 && 
				max->score < temp->score) || (max->score == temp->score && 
				strcmp(temp->position, max->position) > 0 && 
				strcmp(temp->position, position) == 0 )) {
				max = temp;
			}
			temp = temp->next;
		}
		aux = aux->next;
	}
	if (strcmp(position, max->position) != 0){
		return NULL;
	}
	else {
		return copy_list(max);
	}
}
//Functie ce returneaza topul celor mai buni N jucatori (ca scor).
Player *get_top_players(FootballClub *clubs, int N) {
	Player *new_node, *new_node1; 
	Player *help, *new_list1 = NULL, *new_list2 = NULL;
	FootballClub *aux = clubs;
	int x;
	
	if (clubs == NULL){
		return NULL;
	}

	while (aux != NULL){
		x = N;
		new_node1 = aux->players;
		//Se copiaza, pe rand, fiecare nod din lista jucatorilor.
		while(new_node1 != NULL){
			new_node = copy_list(new_node1);
			if(new_node == NULL) {
				return NULL;
			}
			add_best(&new_list1, &new_node);
			new_node = new_node->next;
			new_node1 = new_node1->next;
		}
		/*Se copiaza intr-o lista noua primii N jucatori ai listei formate 
		anterior*/
		while (new_list1 != NULL && x != 0){
			help = new_list1->next;
			add_best(&new_list2, &new_list1);
			new_list1 = help;
			x--;
		}
		//Se dezaloca lista initiala.
		destroy_player_list(new_list1);
		new_list1 = NULL;
		aux = aux->next;
	}

	return new_list2;	
}
//Functie ce returneaza toti jucatorii cu scorul cel putin egal cu score.
Player *get_players_by_score(FootballClub *clubs, int score) {
	Player *new_node1, *new_node2, *new_node3, *new_list1 = NULL;
	FootballClub *aux = clubs;

	if (clubs == NULL){
		return NULL;
	}

	while (aux != NULL){
		new_node1 = aux->players;
		while (new_node1 != NULL){
			//Se copiaza intr-o noua lista jucatorii ai caror scor corespunde.
			if (new_node1->score >= score){
				new_node2 = copy_list(new_node1);
				add_best(&new_list1, &new_node2);
				new_node2 = new_node2-> next;
			}
			new_node1 = new_node1->next;
		}
		new_node3 = aux->injured_players;
		//Se verifica si scorul jucatorilor accidentati.
		while (new_node3 != NULL){
			if (new_node3->score >= score){
				new_node2 = copy_list(new_node3);
				add_best(&new_list1, &new_node2);
				new_node2 = new_node2-> next;;
			}
			new_node3 = new_node3->next;
		}


		aux = aux->next;
	}
	
	return new_list1;
}


//Functie ce returneaza toti jucatorii in rolul specificat.
Player *get_players_by_position(FootballClub *clubs, char *position) {
	Player *new_node1, *new_node2, *new_node3, *new_list1 = NULL;
	FootballClub *aux = clubs;
	
	if (clubs == NULL){
		return NULL;
	}
	while (aux != NULL){
		new_node1 = aux->players;

		while (new_node1 != NULL){
			//Se copiaza intr-o lista noua jucatroii ai caror pozitie corespunde
			if (strcmp(new_node1->position, position) == 0){
				new_node2 = copy_list(new_node1);
				add_best(&new_list1, &new_node2);
			}
			new_node1 = new_node1->next;
		}
		//Se verifica jucatorii si in lista celor accidentati.
		new_node3 = aux->injured_players;
		while (new_node3 != NULL){

			if (strcmp(new_node3->position,position) == 0){
				new_node2 = copy_list(new_node3);
				add_best(&new_list1, &new_node2);
			}
			new_node3 = new_node3->next;
		}


		aux = aux->next;
	}
	return new_list1;
}

/*Functie ce intoarce lista cu cel mai bun portar, 4 cei mai buni fundasi,
3 cei mai buni atacanti si 3 cei mai buni mijlocasi (ca scor)*/
Player *get_best_team(FootballClub *clubs) {
	Player *new_list1, *new_list11, *new_list2, *new_list3, *new_list4;
	Player *new_list21, *new_list31, *new_list41, *temp;
	FootballClub *aux = clubs;
	
	if (clubs == NULL)
		return NULL;

	new_list1 = get_players_by_position(aux, "portar"); //Lista potarilor
	new_list2 = get_players_by_position(aux, "fundas"); //Lista fundasilor
	new_list3 = get_players_by_position(aux, "mijlocas"); //Lista mijlocasilor
	new_list4 = get_players_by_position(aux, "atacant"); //Lista atacantilor
	new_list11 = get_max_players(new_list1, 1); //Lista cu un singur portar
	new_list21 = get_max_players(new_list2, 4); //Lista cu 4 fundasi
	new_list31 = get_max_players(new_list3, 3); //Lista cu 3 mijlocasi
	new_list41 = get_max_players(new_list4, 3); //Lista cu 3 atacanti

	//Adaug fundasii in aceeasi lista cu portarul.
	while (new_list21 != NULL){
		temp = new_list21->next;
		add_best(&new_list11, &new_list21);
		new_list21 = temp;
	}
	//Adaug si mijlocasii in lista precedenta.
	while (new_list31 != NULL){
		temp = new_list31->next;
		add_best(&new_list11, &new_list31);
		new_list31 = temp;
	}
	//Adaug si atacantii in lista de mai sus.
	while (new_list41 != NULL){
		temp = new_list41->next;
		add_best(&new_list11, &new_list41);
		new_list41 = temp;
	}
	//Dezaloc memoria pentru listele noi formate.
	temp = new_list1;
	while (temp) {
		Player *aux2 = temp;
		temp = temp->next;
		free(aux2->name);
		free(aux2->position);
		free(aux2);
	}
	temp = new_list2;
	while (temp) {
		Player *aux2 = temp;
		temp = temp->next;
		free(aux2->name);
		free(aux2->position);
		free(aux2);
	}
	temp = new_list3;
	while (temp) {
		Player *aux2 = temp;
		temp = temp->next;
		free(aux2->name);
		free(aux2->position);
		free(aux2);
	}
	temp = new_list4;
	while (temp) {
		Player *aux2 = temp;
		temp = temp->next;
		free(aux2->name);
		free(aux2->position);
		free(aux2);
	}

	return new_list11;


}

#endif // TEAM_EXTRACTOR_H_INCLUDED