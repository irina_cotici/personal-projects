function [A_k S] = task3(image, k)
  
  A = double(imread(image));
  a = [];
  b = [];
  m = size(A, 1);
  n = size(A, 2);
  j = 1;
  miu = [];
  sum = 0;
  Z = [];
  W = [];
   
  for i = 1 : m
    for j = 1 : n
      sum = sum + A(i,j);
    endfor
    miu = [miu, ;sum/n];
    sum = 0;
  endfor
  
  for i = 1 : m
    A(i, 1 : n) = A(i, 1 : n) - miu(i);
  endfor
  Z = A'/sqrt(n - 1);
  [u, s, v] = svd(Z);
  Z = u*s*v';
  
  for i = 1 : m 
    W(i, j = 1:k) = v(i, j = 1:k);
  endfor
  
  Y = W'*A;
  A_k = W*Y + miu;
  S = s;
endfunction