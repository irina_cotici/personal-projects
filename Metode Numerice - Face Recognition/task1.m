function A_k = task1(image, k)
  I_matrix = double(imread(image));
  [u, s, v] = svd(I_matrix);
  s2 = s;
  s2((k + 1):end, :) = 0;
  s2(:, (k + 1):end) = 0;
  A_k = u*s2*v';
  imshow(uint8(A_k));
 end