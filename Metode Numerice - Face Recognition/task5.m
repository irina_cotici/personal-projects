function task5()
 A = double(imread('in/images/image2.gif'));
  m = size(A, 1);
  n = size(A, 2);
  x1 = [];
  x2 = [];
  y2 = [];
  x3 = [];
  y3 = [];
  x4 = [];
  y4 = [];
  
  figure(1);
  for (k = [1:19 20:20:99 100:30:min(m,n)])
    [A_k S] = task3('in/images/image2.gif', k);
  endfor
  x1 = [x1, diag(S)];
  plot(x1);
  
  figure(2);
  for (k = [1:19 20:20:99 100:30:min(m,n)])
    [A_k S] = task3('in/images/image2.gif', k);
    S1 = 0;
    S2 = 0;
    for i = 1:k
      S1 = S1 + S(i,i);
    endfor
    for i = 1:min(m,n)
      S2 = S2 + S(i,i);
    endfor
    info = S1/S2;
    x2 = [x2, k];
    y2 = [y2, info];
  endfor
  plot(x2, y2);
  
  figure(3);
  for (k = [1:19 20:20:99 100:30:min(m,n)])
    [A_k S] = task3('in/images/image2.gif', k);
    er = 0;
    for i =1:m
      for j = 1:n
        er = er + ((A(i,j) - A_k(i,j))^2)/(m*n);
      endfor
    endfor
    x3 = [x3, k];
    y3 = [y3, er];
   endfor
   plot(x3, y3);
   
   figure(4);
   for (k = [1:19 20:20:99 100:30:min(m,n)])
     rank = (2*k + 1)/n;
     x4 = [x4, k];
     y4 = [y4, rank];
   endfor
   plot(x4, y4);
  
end