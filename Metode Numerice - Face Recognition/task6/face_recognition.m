function [min_dist output_img_index] = face_recognition(image_path, m, A, eigenfaces, pr_img)
 
    buffer = [];
    file = strcat(image_path);
    I = double(rgb2gray(imread(file)));
    [x, y] = size(I);
    
    for i = 1:y
      buffer = [buffer I(i,:)];
    end
    
    buffer = buffer';

    T = buffer - m;
    tmp = eigenfaces' * T;

    [x y] = size(pr_img);
    min_dist = norm(pr_img(:,1) - tmp);
    output_img_index = 1;
    
    for j = 1:x
      if min_dist > norm(pr_img(:,j) - tmp)
        min_dist = norm(pr_img(:,j) - tmp);
        output_img_index = j;
    end
  
end