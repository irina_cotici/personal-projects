function [m A eigenfaces pr_img] = eigenface_core(database_path)
  
  T = [];
  for k = 1:10
    
    I = [];
    buffer = [];
    file = strcat(database_path,'/',int2str(k),'.jpg');
    I = double(rgb2gray(imread(file)));
    
    [x, y] = size(I);
    
    for i = 1:y
      buffer = [buffer I(i,:)];
    end
    
    buffer = buffer';
        
    T = [T buffer];
  endfor
  
  [x, y] = size(T);
  m = [];
  
  for i = 1:x
    m(i) = mean(T(i,:));
  end
  
  m = m';
  A = T - m;
   
  [V S] = eig(A' * A); 
  [x, y] = size(V);
  dV = diag(S);
  new_V = [];
  
  for i = 1:x
    if dV(i) > 1
      new_V = [new_V V(:,i)];
    end
  end

  eigenfaces = A * new_V;
   
  pr_img = eigenfaces' * A;
end