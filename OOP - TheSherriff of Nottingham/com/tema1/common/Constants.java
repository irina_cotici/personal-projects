package com.tema1.common;

public final class Constants {

    //Constantele utilizate frecvent in tema
    public static final int STARTING_WEALTH = 80;
    public static final int STARTING_CARDS = 10;
    public static final int NUM_LEGAL_GOODS = 10;
    public static final int MAX_CARDS_IN_BAG = 8;
    public static final int MIN_WEALTH = 16;
    public static final int BASE_BRIBE = 0;
    public static final int FIRST_BRIBE = 5;
    public static final int SECOND_BRIBE = 10;
    public static final int FIRST_POSITION = 0;
    //Id-urile bunurilor
    public static final int CHEESE_NUM = 3;
    public static final int CHICKEN_NUM = 2;
    public static final int BREAD_NUM = 2;
    public static final int WINE_NUM = 4;
    public static final int TOMATO_NUM = 2;
    public static final int POTATO_NUM = 3;
    public static final int APPLE_ID = 0;
    public static final int CHEESE_ID = 1;
    public static final int BREAD_ID = 2;
    public static final int CHICKEN_ID = 3;
    public static final int TOMATO_ID = 4;
    public static final int POTATO_ID = 6;
    public static final int WINE_ID = 7;
    public static final int SILK_ID = 20;
    public static final int PEPPER_ID = 21;
    public static final int BARREL_ID = 22;
    public static final int BEER_ID = 23;
    public static final int SEAFOOD_ID = 24;



}
