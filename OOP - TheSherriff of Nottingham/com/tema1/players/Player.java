package com.tema1.players;

import com.tema1.common.Constants;
import com.tema1.goods.Bag;
import com.tema1.goods.Goods;
import com.tema1.goods.GoodsFactory;
import java.util.LinkedList;

public class Player {
    protected GoodsFactory f = GoodsFactory.getInstance();
    protected int roundCount;
    private int kingBonus;
    private int queenBonus;
    private int cardsNumber;
    protected int wealth;
    protected int playerId;
    protected Bag bag;
    protected String strategy;
    private boolean isSheriff;
    protected LinkedList<Goods> cardsInHands;
    protected LinkedList<Goods> finalGoods = new LinkedList<>();
    protected Constants constants = new Constants();

    public final void setRoundCount(int roundCount) {
        this.roundCount = roundCount;
    }

    public final int getWealth() {
        return wealth;
    }

    public final boolean isSheriff() {
        return isSheriff;
    }

    public final void setSheriff(boolean sheriff) {
        isSheriff = sheriff;
    }

    public Player() {
    }

    public Player(int playerId) {
        this.roundCount = 0;
        this.kingBonus = 0;
        this.queenBonus = 0;
        this.playerId = playerId;
        this.cardsNumber = constants.STARTING_CARDS;
        this.wealth = constants.STARTING_WEALTH;
        this.cardsInHands = new LinkedList<>();
    }

    public void createBag() {
    }
    /**/
    public boolean isSuspicious(Player player) {
        return true;
    }
    public void inspects(Player comerciant, LinkedList<Goods> goodsList) {
    }

    public final void takeOutGoods() {
        bag.calculateBagProfit();
        for (int i = 0; i < bag.getBagGoods().size(); i++) {
            finalGoods.add(bag.getBagGoods().get(i));
            bag.getBagGoods().remove(bag.getBagGoods().get(i));
            i--;
        }

    }

    /*Metoda in care are loc resetarea averii finale*/
    public final void sellGoods() {
        for (Goods goods : finalGoods) {
            this.wealth += f.getGoodsById(goods.getId()).getProfit();
        }
    }

    /*Metoda in care are loc completarea celor zece carti in mana*/
    public final LinkedList<Goods> takeCards(LinkedList<Goods> goodsList) {
        cardsInHands.clear();
        int i = cardsNumber;
        while (i != 0) {
            cardsInHands.add(goodsList.getFirst());
            goodsList.removeFirst();
            i--;
        }
        return cardsInHands;
    }

    /*Adaugarea bonusului King*/
    public final void addKingBonus(final int id) {
        this.wealth += this.f.getGoodsById(id).getKingBonus();
    }
    /*Adaugarea bonusului Queen*/
    public final void addQueenBonus(final int id) {
        this.wealth += this.f.getGoodsById(id).getQueenBonus();
    }

    public final int findHowMany(final int id) {
        int count = 0;
        if (!finalGoods.isEmpty()) {
            for (int i = 0; i < finalGoods.size(); i++) {
                if (id == finalGoods.get(i).getId()) {
                    count++;
                }
            }
        }
        return count;
    }

    @Override
    public final String toString() {
        return this.playerId + " " + this.strategy + " " + this.wealth;
    }
}

