package com.tema1.players;

import com.tema1.common.Constants;
import com.tema1.goods.Goods;
import com.tema1.goods.GoodsType;
import java.util.LinkedList;

public class GreedyPlayer extends BasePlayer {

    public  GreedyPlayer(int playerId) {
        super(playerId);
        this.strategy = "GREEDY";
    }

    @Override
    /*Metoda in care are loc selectia cartilor in functie de strategie*/
    public LinkedList<Goods> selectBagCards() {
        LinkedList<Goods> bagCards = super.selectBagCards();
        if (bagCards.size() < Constants.MAX_CARDS_IN_BAG
                && !illegalCards.isEmpty() && this.roundCount % 2 != 0) {
            Goods maxIllegalProfit = illegalCards.get(0);
            for (int i = 0; i < illegalCards.size(); i++) {
                if (illegalCards.get(i).getProfit() > maxIllegalProfit.getProfit()) {
                    maxIllegalProfit = illegalCards.get(i);
                }
            }
            bagCards.add(maxIllegalProfit);
            illegalCards.remove(maxIllegalProfit);
        }
        return bagCards;
    }

    @Override
    /*Are loc verificarea doar a jucatorilor ce nu dau mita*/
    public boolean isSuspicious(Player player) {
        if (player.bag.getBribe() == 0) {
            for (int i = 0; i < player.bag.getGoodsNumber(); i++) {
                if (player.bag.getGoodsId() != player.bag.getBagGoods().get(i).getId()
                        || player.bag.getBagGoods().get(i).getType().equals(GoodsType.Illegal)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    /*Metoda serifului de inspectare*/
    public void inspects(Player player, LinkedList<Goods> goodsList) {
        int bagCardsNumber = player.bag.getGoodsNumber();
        int bribe = player.bag.getBribe();
        /*Luarea mitei*/
        if (bribe != 0) {
            this.wealth += bribe;
        } else if (!isSuspicious(player) && this.wealth >= Constants.MIN_WEALTH) {
            player.wealth += bagCardsNumber
                    * player.bag.getBagGoods().get(Constants.APPLE_ID).getPenalty();
            this.wealth -= bagCardsNumber
                    * player.bag.getBagGoods().get(Constants.APPLE_ID).getPenalty();
        } else if (isSuspicious(player) && this.wealth >= Constants.MIN_WEALTH) {
            for (int i = 0; i < player.bag.getBagGoods().size(); i++) {
                if (player.bag.getGoodsId() != player.bag.getBagGoods().get(i).getId()
                        || player.bag.getBagGoods().get(i).getType().equals(GoodsType.Illegal)) {
                    int penalty = player.bag.getBagGoods().get(i).getPenalty();
                    player.wealth -= penalty;
                    this.wealth += penalty;
                    goodsList.addLast(player.bag.getBagGoods().get(i));
                    player.bag.getBagGoods().remove(i);
                    i--;
                }
            }
        }
    }
}
