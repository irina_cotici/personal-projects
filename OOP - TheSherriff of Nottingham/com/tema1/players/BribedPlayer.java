package com.tema1.players;

import com.tema1.common.Constants;
import com.tema1.goods.Bag;
import com.tema1.goods.Goods;
import com.tema1.goods.GoodsType;
import java.util.LinkedList;

public final class BribedPlayer extends BasePlayer {
    private int numOfPlayers;

    public BribedPlayer(int playerId, int numOfPlayers) {
        super(playerId);
        this.strategy = "BRIBED";
        this.numOfPlayers = numOfPlayers;
    }

    /*Metoda in care are loc sortarea cartilor dupa profit*/
    private void sortCardsbyProfit(LinkedList<Goods> cardsList) {
        for (int i = 1; i < cardsList.size(); i++) {
            Goods mostProfitable = cardsList.get(i);
                int j = i - 1;
                while ((j >= 0 && cardsList.get(j).getProfit() < mostProfitable.getProfit())
                        || (j >= 0 && cardsList.get(j).getProfit() == mostProfitable.getProfit()
                        && cardsList.get(j).getId() < mostProfitable.getId())) {
                    cardsList.set(j + 1, cardsList.get(j));
                    j = j - 1;
                }
                cardsList.set(j + 1, mostProfitable);
        }
    }

    /*Metoda in care se creeaza lista de carti potrivita pentru
    * strategia bribed*/
     public LinkedList<Goods> selectBribedCards() {
        LinkedList<Goods> bagCards = new LinkedList<>();

        if (!illegalCards.isEmpty()) {
            sortCardsbyProfit(illegalCards);
            for (int j = 0; j < constants.MAX_CARDS_IN_BAG; j++) {

                bagCards.add(illegalCards.poll());

                if (illegalCards.isEmpty()) {
                    break;
                }

            }

        }
        if (!legalCards.isEmpty()) {
            sortCardsbyProfit(legalCards);
            int legalCardsCompleter = constants.MAX_CARDS_IN_BAG - bagCards.size();
            for (int j = 0; j < legalCardsCompleter; j++) {
                bagCards.add(legalCards.poll());
            }
        }

        return bagCards;
    }

    @Override
    /*Metoda in care se creeaza sacul de bunuri*/
    public void createBag() {
        chooseCards();
        LinkedList<Goods> bagCards = selectBribedCards();
        int illegalCount = 0;
        int pseudoPenalty = 0;

        for (int i = 0; i < bagCards.size(); i++) {
            if (this.wealth > pseudoPenalty + bagCards.get(i).getPenalty()) {
                pseudoPenalty += bagCards.get(i).getPenalty();
            } else {
                bagCards.remove(i);
                i--;
            }
        }

            for (int j = 0; j < bagCards.size(); j++) {
                if (bagCards.get(j).getType().equals(GoodsType.Illegal)) {
                    illegalCount++;
                }
            }

            if (illegalCount == 0 || this.wealth <= Constants.FIRST_BRIBE) {
                super.createBag();
            } else if (illegalCount <= 2) {
                this.wealth -= constants.FIRST_BRIBE;
                bag = new Bag(bagCards, Constants.FIRST_BRIBE, Constants.APPLE_ID);
            } else if (illegalCount > 2) {
                this.wealth -= Constants.SECOND_BRIBE;
                bag = new Bag(bagCards, Constants.SECOND_BRIBE, Constants.APPLE_ID);
            } else if (illegalCount == 0) {
                super.createBag();
            }
    }

    @Override
    /*Metoda serifului de inspectare a comerciantului*/
    public void inspects(Player comerciant, LinkedList<Goods> goodsList) {
        /*Se verifica daca venitul ii permite inspectia*/
        if (this.wealth < constants.MIN_WEALTH) {
            this.isBankrupt = true;
        }
        /*Are loc inspectarea doar a jucatorilor din dreapta si stanga*/
        if (comerciant.playerId == this.playerId - 1
                || comerciant.playerId == this.playerId + 1
                || (this.playerId == numOfPlayers - 1 && comerciant.playerId == 0)
                || (comerciant.playerId == numOfPlayers - 1 && this.playerId == 0)) {
            super.inspects(comerciant, goodsList);
        } else {
            this.wealth += comerciant.bag.getBribe();
        }
    }
}
