package com.tema1.players;

import com.tema1.common.Constants;
import com.tema1.goods.Bag;
import com.tema1.goods.Goods;
import com.tema1.goods.GoodsFactory;
import com.tema1.goods.GoodsType;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/*Clasa ce defineste functionalitatea strategiei de baza*/
public class BasePlayer extends Player {

    protected LinkedList<Goods> illegalCards = new LinkedList<>();
    protected LinkedList<Goods> legalCards = new LinkedList<>();
    protected boolean isBankrupt;

    public BasePlayer() {
    }

    public BasePlayer(int playerId) {
        super(playerId);
        this.strategy = "BASIC";
    }

    public final boolean isBankrupt() {
        return isBankrupt;
    }

    /*Metoda ce defineste alegerea cartilor pentru strategie*/
    public final void chooseCards() {
        illegalCards.clear();
        legalCards.clear();
        for (int i = 0; i < cardsInHands.size(); i++) {
            if (cardsInHands.get(i).getType().equals(GoodsType.Illegal)) {
                illegalCards.add(cardsInHands.get(i));
            } else {
                legalCards.add(cardsInHands.get(i));
            }
        }
    }

    /*Metoda prin ce identifica cartea de frecventa maxima*/
    public final int findOccurence() {
        Map<Integer, Integer> occurence = new HashMap<>();
        Map.Entry<Integer, Integer> maxEntry = null;

        /*Se completeaza un hashMap cu frecventele cartilor pe post
        * de valoare si id-ul lor pe post de cheie*/
        for (Goods good : legalCards) {

            if (occurence.containsKey(good.getId())) {
                occurence.replace(good.getId(),  occurence.get(good.getId()) + 1);
            } else {
                occurence.put(good.getId(), 1);
            }
        }

        Map<Integer, Integer> sameFrequency = new HashMap<>();
        Map<Integer, Integer> sameProfit = new HashMap<>();

        /*Se identifica cartea cu frecventa maxima*/
        for (Map.Entry<Integer, Integer> entry : occurence.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }
        /*Daca exista carti cu aceeasi frecventa, acestea se adauga
        * intr-un alt hashMap*/
        for (Map.Entry<Integer, Integer> entry : occurence.entrySet())  {
            if (entry.getValue().compareTo(maxEntry.getValue()) == 0) {

                sameFrequency.put(entry.getKey(), entry.getValue());

            }
        }

        int maxProfit = 0;
        GoodsFactory factory = GoodsFactory.getInstance();

        /*Se identifica cartea cu profit maxim*/
        for (Map.Entry<Integer, Integer> entry : sameFrequency.entrySet()) {
            if (factory.getGoodsById(entry.getKey()).getProfit() > maxProfit) {
                maxProfit = factory.getGoodsById(entry.getKey()).getProfit();
            }
        }

        /*Daca exista mai multe carti cu acelasi profit, ele se adauga
        * intr-un alt hashMap*/
        for (Map.Entry<Integer, Integer> entry : sameFrequency.entrySet()) {
            if (factory.getGoodsById(entry.getKey()).getProfit() == maxProfit) {
                sameProfit.put(entry.getKey(), entry.getValue());
            }
        }
        int maxId = 0;
        /*Se identifica cartea cu Id maxim*/
        for (Map.Entry<Integer, Integer> entry : sameProfit.entrySet()) {
            if (entry.getKey() > maxId) {
                maxId = entry.getKey();
            }
        }

        return maxId;
    }

    /*Metoda in care are loc definirea bunurilor ce urmeaza
    * a fi adaugate in sac*/
    public LinkedList<Goods> selectBagCards() {
        chooseCards();
        LinkedList<Goods> bagCards = new LinkedList<>();
        int maxId = findOccurence();

        /*Daca nu exista carti legale, se adauga cartea ilegala
        * de profit maxim*/
        if (legalCards.isEmpty()) {
            Goods maxIllegalProfit = illegalCards.get(0);
            for (int i = 0; i < illegalCards.size(); i++) {
                if (illegalCards.get(i).getProfit() > maxIllegalProfit.getProfit()) {
                    maxIllegalProfit = illegalCards.get(i);
                }

            }
            bagCards.add(maxIllegalProfit);
            illegalCards.remove(maxIllegalProfit);

        } else {
            /*Se adauga cartile cu frecventa maxima, in functie
            * de algoritmul de sortare a acestora*/
            for (int i = 0; i < legalCards.size(); i++) {
                if (legalCards.get(i).getId() == maxId) {
                    bagCards.add(legalCards.get(i));
                    legalCards.remove(legalCards.get(i));
                    i--;
                }
            }
        }
        return bagCards;
    }

    @Override
    /*Metoda de creare a sacului cu bunuri*/
    public void createBag() {
        LinkedList<Goods> bagCards = selectBagCards();
        if (bagCards.get(0).getType().equals(GoodsType.Legal)) {
            bag = new Bag(bagCards, Constants.BASE_BRIBE, bagCards.get(0).getId());
        } else {
            bag = new Bag(bagCards, Constants.BASE_BRIBE, Constants.APPLE_ID);
        }
    }


    @Override
    /*Metoda a serifului ce verifica sacul jucatorilor*/
    public boolean isSuspicious(Player player) {
        for (int i = 0; i < player.bag.getGoodsNumber(); i++) {
            /*Daca ramane fara bani, acesta nu mai verifica*/
            if (isBankrupt()) {
                break;
            }
            if (player.bag.getGoodsId() != player.bag.getBagGoods().get(i).getId()
                    || player.bag.getBagGoods().get(i).getType().equals(GoodsType.Illegal)) {
                return true;
            }
        }
        return false;
    }

    @Override
    /*Metoda in care are loc inspectia propriu-zisa*/
    public void inspects(Player comerciant, LinkedList<Goods> goodsList) {
        int bagCardsNumber = comerciant.bag.getGoodsNumber();
        int bribe = comerciant.bag.getBribe();

        /*Odata cu verificarea sacului, mita trece inapoi in
        * averea comerciantului*/
        comerciant.wealth += bribe;

        if (comerciant.bag.getBagGoods().isEmpty() || isBankrupt()) {
            return;
        }

        if (!isSuspicious(comerciant) && this.wealth >= Constants.MIN_WEALTH) {
            int penalty = comerciant.bag.getBagGoods().getFirst().getPenalty();
            comerciant.wealth += bagCardsNumber * penalty;
            this.wealth -= bagCardsNumber * penalty;
        } else if (isSuspicious(comerciant) && this.wealth >= Constants.MIN_WEALTH) {

            for (int i = 0; i < comerciant.bag.getBagGoods().size(); i++) {
                if (comerciant.bag.getGoodsId() != comerciant.bag.getBagGoods().get(i).getId()
                        || comerciant.bag.getBagGoods().get(i).getType().equals(GoodsType.Illegal)) {
                    int penalty = comerciant.bag.getBagGoods().get(i).getPenalty();
                    comerciant.wealth -= penalty;
                    this.wealth += penalty;
                    goodsList.addLast(comerciant.bag.getBagGoods().get(i));
                    comerciant.bag.getBagGoods().remove(i);
                    i--;
                }
            }
        }
    }
}
