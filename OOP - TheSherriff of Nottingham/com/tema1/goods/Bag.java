package com.tema1.goods;

import com.tema1.common.Constants;
import java.util.LinkedList;

/*Clasa ce inglobeaza proprietatile si intrebuintarea unei genti cu bunuri.*/
public final class Bag {

    private int bribe;
    private int goodsId;
    private LinkedList<Goods> bagGoods;

    public Bag(LinkedList<Goods> bagGoods, int bribe, int goodsId) {
        this.bagGoods = bagGoods;
        this.bribe = bribe;
        this.goodsId = goodsId;
    }

    // Getter-ii
    public int getBribe() {
        return bribe;
    }

    public int getGoodsNumber() {
        return bagGoods.size();
    }

    public int getGoodsId() {
        return goodsId;
    }

    public LinkedList<Goods> getBagGoods() {
        return bagGoods;
    }

    /*Calculeaza profitul ce l-ar aduce bunurile dintr-o geanta.
    * In interiorul metodei se parcurge lista de bunuri si se
    * identifica tipul lor. Daca acestea sunt legale, pur si simplu
    * se adauga la venitul total profitul bunurilor respective.
    * In caz contrar, intr-un switch se verifica care e anume bunul din
    * geanta si in functie de acesta are loc adaugarea profitului, dar
    * si a altor carti legale in geanta pe post de bonus.*/
    public void calculateBagProfit() {
        int totalGain = 0;
        GoodsFactory f = GoodsFactory.getInstance();
        for (int j = 0; j < bagGoods.size(); j++) {
            if (bagGoods.get(j).getType().equals(GoodsType.Legal)) {
                totalGain += bagGoods.get(j).getProfit();
            } else {
                switch (bagGoods.get(j).getId()) {
                    case Constants.SILK_ID :
                        for (int i = 0; i < Constants.CHEESE_NUM; i++) {
                            bagGoods.add(f.getGoodsById(Constants.CHEESE_ID));
                        }
                        totalGain += bagGoods.get(j).getProfit();
                        break;
                    case Constants.PEPPER_ID :
                        for (int i = 0; i < Constants.CHICKEN_NUM; i++) {
                            bagGoods.add(f.getGoodsById(Constants.CHICKEN_ID));
                        }
                        totalGain += bagGoods.get(j).getProfit();
                        break;
                    case Constants.BARREL_ID :
                        for (int i = 0; i < Constants.BREAD_NUM; i++) {
                            bagGoods.add(f.getGoodsById(Constants.BREAD_ID));
                        }
                        totalGain += bagGoods.get(j).getProfit();
                        break;
                    case Constants.BEER_ID :
                        for (int i = 0; i < Constants.WINE_NUM; i++) {
                            bagGoods.add(f.getGoodsById(Constants.WINE_ID));
                        }
                        totalGain += bagGoods.get(j).getProfit();
                        break;
                    case Constants.SEAFOOD_ID :
                        for (int i = 0; i < Constants.TOMATO_NUM; i++) {
                            bagGoods.add(f.getGoodsById(Constants.TOMATO_ID));
                        }
                        for (int i = 0; i < Constants.POTATO_NUM; i++) {
                            bagGoods.add(f.getGoodsById(Constants.POTATO_ID));
                        }
                        bagGoods.add(f.getGoodsById(Constants.CHICKEN_ID));
                        totalGain += bagGoods.get(j).getProfit();
                        break;
                    default :
                        return;
                }
            }
        }
    }
}
