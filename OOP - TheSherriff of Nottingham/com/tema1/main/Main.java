package com.tema1.main;

import com.tema1.common.Constants;
import com.tema1.goods.Goods;
import com.tema1.goods.GoodsFactory;
import com.tema1.players.BasePlayer;
import com.tema1.players.BribedPlayer;
import com.tema1.players.GreedyPlayer;
import com.tema1.players.Player;
import java.util.LinkedList;

public final class Main {

   /* private Main() {
        // just to trick checkstyle
    }
*/
    public static void main(final String[] args) {
        GameInputLoader gameInputLoader = new GameInputLoader(args[0], args[1]);
        GameInput gameInput = gameInputLoader.load();
        GoodsFactory f = GoodsFactory.getInstance();
        Constants constants = new Constants();

        //TODO implement homework logic
        LinkedList<Integer> cardsList =  gameInput.getAssetIds();

        LinkedList<Goods> goodsList = new LinkedList<>();
        /*Completarea listei de bunuri*/
        for (int i = 0; i < cardsList.size(); i++) {
            goodsList.add(f.getGoodsById(cardsList.get(i)));
        }
        LinkedList<String> playersNames = gameInput.getPlayerNames();
        LinkedList<Player> players = new LinkedList<>();

        /*Definirea jucatorilor si a strategiei*/
        for (int i = 0; i < playersNames.size(); i++) {

            switch (playersNames.get(i)) {
                case "basic" :
                    players.add(new BasePlayer(i));
                    break;
                case "greedy" :
                    players.add(new GreedyPlayer(i));
                    break;
                case "bribed" :
                    players.add(new BribedPlayer(i, playersNames.size()));
                    break;
                default :
                    return;
            }
        }
        /*Desfasurarea jocului*/
        for (int i = 0; i < gameInput.getRounds(); i++) {
            for (int sherriff = 0; sherriff < players.size(); sherriff++) {
                players.get(sherriff).setSheriff(true);
                for (int comerciant = 0; comerciant < players.size(); comerciant++) {
                    players.get(comerciant).setRoundCount(i);
                    if (!players.get(comerciant).isSheriff()) {

                        players.get(comerciant).takeCards(goodsList);
                        players.get(comerciant).createBag();
                        players.get(sherriff).inspects(players.get(comerciant), goodsList);
                        players.get(comerciant).takeOutGoods();
                    }
                }
                players.get(sherriff).setSheriff(false);
            }
        }

        /*Completarea averii finale a jucatorilor*/
        for (Player player : players) {
            player.sellGoods();
        }

        BonusManager manager = new BonusManager();
        manager.findKingsQueens(players);
        /*Adaugarea bonusurilor King & Queen*/
        for (int j = Constants.FIRST_POSITION; j < manager.getNum(); j++) {
            if (manager.getKing().get(j) != null) {
                manager.getKing().get(j).addKingBonus(j);
            }
            if (manager.getQueen().get(j) != null) {
                manager.getQueen().get(j).addQueenBonus(j);
            }
        }

        /*Sortarea finala a jucatorilor in functie
        * de scorul final*/
        for (int i = 1; i < players.size(); i++) {
            Player firstPlayer = players.get(i);
            int j = i - 1;
            while (j >= 0 && players.get(j).getWealth() < firstPlayer.getWealth()) {
                players.set(j + 1, players.get(j));
                j = j - 1;
            }
            players.set(j + 1, firstPlayer);
        }

        /*Afisarea jucatorilor*/
        for (int i = 0; i < players.size(); i++) {
            System.out.println(players.get(i));
        }
    }
}


