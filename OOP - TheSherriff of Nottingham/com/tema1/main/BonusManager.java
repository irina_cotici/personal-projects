package com.tema1.main;

import com.tema1.common.Constants;
import com.tema1.players.Player;
import java.util.LinkedList;

/*Clasa este auxiliara si functionalitatea pe care o
* conserva este limitata la identificarea jucatorilor ce
* trebuie sa primeasca bonusul de king si queen.*/
public class BonusManager {
    private LinkedList<Player> king = new LinkedList<>();
    private LinkedList<Player> queen = new LinkedList<>();
    private int num = Constants.NUM_LEGAL_GOODS;

    //Getter-ii
    public final int getNum() {
        return num;
    }

    public final LinkedList<Player> getKing() {
        return king;
    }

    public final LinkedList<Player> getQueen() {
        return queen;
    }

    /*Metoda aceasta completeaza cu elementele listele king si queen,
    * permitand, apoi, accesarea lor in main si setarea bonusurilor.*/
    public final void findKingsQueens(LinkedList<Player> players) {

        /*Initializeaza cu null listele de lungimea numarului
        * total de elemente legale ce pot genera bonusul*/
        for (int i = 0; i < num; i++) {
            king.add(null);
            queen.add(null);
        }

        for (int j = 0; j < players.size(); j++) {
            for (int i = 0; i < num; i++) {

                /*Daca nu a fost setat deja un element in king si bunul
                * identificat prin Id-ul i se gaseste de mai multe ori
                * in geanta comerciantului, atunci jucatorul este plasat
                * pe pozitia ce corespunde Id-ului cu frecventa mare.*/
                if (king.get(i) == null && players.get(j).findHowMany(i) > 0) {
                    king.set(i, players.get(j));

                    /*Se verifica, in mod analog, si pentru lista queen.*/
                } else if ((queen.get(i) == null && players.get(j).findHowMany(i) > 0)
                        && king.get(i).findHowMany(i) >= players.get(j).findHowMany(i)) {
                    queen.set(i, players.get(j));
                    /*Se identifica jucatorul ce are frecventa maxima de bunuro*/
                } else if (king.get(i) != null && king.get(i).findHowMany(i)
                        < players.get(j).findHowMany(i)) {
                    queen.set(i, king.get(i));
                    king.set(i, players.get(j));
                } else if (queen.get(i) != null && queen.get(i).findHowMany(i)
                        < players.get(j).findHowMany(i)) {
                    queen.set(i, players.get(j));
                }
            }
        }

    }
}
