package com.tema1.main;

import java.util.LinkedList;

public class GameInput {
    // DO NOT MODIFY
    private final LinkedList<Integer> mAssetOrder;
    private final LinkedList<String> mPlayersOrder;
    private int mRounds;

    public GameInput() {
        mAssetOrder = null;
        mPlayersOrder = null;
        mRounds = -1;
    }

    public GameInput(final int rounds, final LinkedList<Integer> assets,
                     final LinkedList<String> players) {
        mAssetOrder = assets;
        mPlayersOrder = players;
        mRounds = rounds;
    }

    public final LinkedList<Integer> getAssetIds() {
        return mAssetOrder;
    }

    public final LinkedList<String> getPlayerNames() {
        return mPlayersOrder;
    }

    public final int getRounds() {
        return mRounds;
    }

    public final boolean isValidInput() {
        boolean membersInstantiated = mAssetOrder != null && mPlayersOrder != null;
        boolean membersNotEmpty = mAssetOrder.size() > 0 && mPlayersOrder.size() > 0 && mRounds > 0;

        return membersInstantiated && membersNotEmpty;
    }
}
