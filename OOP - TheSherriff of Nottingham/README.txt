								= Tema 1 =
===============================================================================
 Nume, prenume: COTICI Irina
 Grupa: 321 CD
 Materie: Programarea orientata pe obiecte

 	Tema 1 a acestei materii a inclus reproducerea board game-ului "TheSherriff of Nottingham".Pentru realizarea temei,
 	am structurat datele in mai multe clase. Astfel, jocul creat de mine include:

 1) Pachetul common -> aici se afla clasa Constants in care am initializat toate constantele utilizate in joc. Le-am
 delimitat in doua categorii: constantele generale, cum ar fi STARTING_WEALTH = 80, ce reprezinta averea de inceput a
 fiecarui jucator sau MAX_CARDS_IN_BAG, ce reflecta cele 8 carti ce ar putea constitui bunurile unei genti. Pe de alta
 parte, cea de-a doua categorie include proprietati legate doar de bunuri: Id-ul acestora si numarul de carti de un
 anumit tip ce se adauga in geanta pentru obtinerea king/queen bonusurilor.

 2) Pachetul goods -> include clasele ce conserveaza functionalitatile "bunurilor" sau a cartilor de joc. 
 	a. Clasa Bag - in interiorul ei exista 4 campuri: bribe, numarul de carti, Id-ul cartilor si lista acestora.
 	Metodele din aceasta clasa sunt, in primul rand, getter-ii. De asemenea, o metoda void numita calculateBagProfit
 	calculeaza profitul adus de cartile din geanta comerciantului.
 	b. Clasa Goods - contine campurile constante cum ar fi Id-ul bunurilor, tipul, profitul si penalitatea acestora.
 	De asemenea, aici se afla getter-ii, inclusiv ai bonusurilor. In aceasta clasa este suprascrisa si metoda toString
 	ce imi permite vizualizarea listelor de bunuri.
 	c. Clasa GoodsFactory e clasa in care se genereaza cartile.
 	d. Clasa GoodsType contine un enum cu cele doua tipuri de carti: legale si ilegale.
 	e. Clasa IllegalGoods extinde clasa Goods si un constructor.
 	f. Clasa LegalGoods extinde, de asemenea, Goods si contine doua campuri ce reprezinta King si Queen bonusul pentru
 	acest tip de carti.

 3) Pachetul players -> include o clasa Player si altele trei care evoca strategia unui jucator.
 	a. Clasa Player - contine campurile: roundCount, wealth, Id, un bag, strategia, un indicator boolean care il
 	defineste ca fiind serif, lista cartilor ce le ia in mana jucatorul, lista finala a bunurilor ce sunt aduse la
 	taraba. In aceasta clasa exista metoda de creare a unei genti, metoda ce suspecteaza un alt comerciant, cea cara il
 	inspecteaza, takeOutGoods scoate pe rand elementele din geanta si calculeaza profitul acestora, iar sellGoods adauga
 	la valoarea wealth, profitul obtinut in urma vanzarii. Similar, prin metodele addKing/QueenBonus, se adauga la avere
 	si bonusurile de pe bunuri. Tot in aceasta clasa se afla si takeCards, o metoda care permite jucatorilor luarea a
 	cate 10 carti in fiecare runda de joc.
 	b. Clasa BasePlayer extinde clasa Player, mostenindu-i toate metodele. Campurile acesteia reprezinta doua liste de
 	bunuri, cele legale si cele ilegale, separate in urma primirii cartilor in maini. De asemenea, variabila isBankrupt
 	este utilizata pentru determinarea comportamentului jucatorului in momentul in care ramane fara bani. Aici se
 	regaseste o metoda numita chooseCards care initializeaza cele doua campuri ale listelor de bunuri mentionate mai
 	sus. Metoda findOccurence gaseste cartea  de frecventa maxima, mergand pana la valoarea Id-ului acesteia, in cazul
 	in care exista frecvente sau profit similar. Metoda selectBagCards returneaza lista de bunuri potrivite pentru a fi
 	puse in geanta, iar in createBag are loc exact acest lucru, pentru care, se creeaza o noua instanta a unui obiect de
 	tip Bag. In aceasta clasa sunt implementate si metodele necesare jucatorului cu aceasta strategie in momentul in
 	care e serif. Astfel, metodele isSuspicious si inspects sunt suprascrise din clasa parinte.
 	c. Intrucat Bribed strategy include strategia de baza, clasa BribedPlayer extinde clasa BasePlayer, suprascriindu-i
 	metoda de creare a sacului de bunuri si cea de inspectare. De asemenea, aici se regasesc metode specifice prin care
 	jucatorul isi selecteaza cartile pe care urmeaza sa si le puna in geanta, numindu-se sortCardsByProfit si
 	selectBribedCards.
 	d. Clasa GreedyPlayer este si ea mostenita din BasePlayer, suprascriindu-i metoda de selectie a cartilor pentru
 	geanta, cea de suspectie si inspectie.

4) Pachetul main -> include trei clase din scheletul temei, dar si o clasa auxiliara pe care mi-am creat-o, numita
BonusManager. In aceasta clasa are determinarea jucatorilor ce urmeaza sa primeasca King si Queen bonusurile. Aceeasta
contine doua campuri ce reprezinta liste de cate 15 elemente, unde pe o anumita pozitie este reprezentat bonusul
specific id-ului ce corespunde.
Totusi, activitatea jocului se petrece in main, acolo unde are loc parcurgerea listei de jucatori, determinarea
serifului si sunt intreprinse toate actiunile necesare.

    Cea mai dificila parte din implementarea acestei teme a constituit-o intelegerea, in primul rand, a enuntului. O
buna parte din timp am alocat-o procesului de intelegere a problemei. Tema mi s-a parut foarte complexa si plina de o
multime de detalii nesugetive pe care nu le-am intuit la inceputul implemetarii acesteia. De asemenea, a fost necesara
scrierea unei bune parti din tema pentru a putea testa functionalitatea. Astfel, a fost necesara printarea momentelor
din joc la care ma aflam, dar si rezolvarea prealabila a strategiilor. Urmarirea fiecarui pas si compararea lui cu
rezolvarea manuala mi-a permis sa identific logica pe care trebuie sa o implementez.