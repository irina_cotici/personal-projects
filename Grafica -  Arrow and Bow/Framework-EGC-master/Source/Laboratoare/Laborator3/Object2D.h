#pragma once

#include <string>

#include <include/glm.h>
#include <Core/GPU/Mesh.h>

namespace Object2D
{

	// Create square with given bottom left corner, length and color
	Mesh* CreateSquare(std::string name, glm::vec3 leftBottomCorner, float length, glm::vec3 color, bool fill = false);
	Mesh* CreateTriangle(std::string name, glm::vec3 leftBottomCorner, float x, glm::vec3 color, bool fill = false);
	
	
	Mesh* CreatePhineas(std::string name);
	Mesh* CreateShuriken(std::string name);
	Mesh* CreateArrow(std::string name);
	Mesh* CreateBalloon(std::string name, glm::vec3 color);
	Mesh* CreateBrokenBalloon(std::string name, glm::vec3 color);
	Mesh* CreateBow(std::string name);
	Mesh* CreateScene(std::string name);
	Mesh* CreatePowerBar(std::string name);
	Mesh* CreateLifeBar(std::string name);

}

