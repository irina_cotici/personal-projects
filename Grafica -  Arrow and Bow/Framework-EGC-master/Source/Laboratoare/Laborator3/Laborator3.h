#pragma once

#include <Component/SimpleScene.h>
#include <string>
#include <Core/Engine.h>
#include <iostream>
using namespace std;

struct Point {
	int x, y;
	float scale;
	bool dead;
	bool hurt;
};

struct Parameter {
	float x, y;
	float angle;
};

class Laborator3 : public SimpleScene
{
	public:
		Laborator3();
		~Laborator3();

		void Init() override;

	private:
		void FrameStart() override;
		void Update(float deltaTimeSeconds) override;
		void FrameEnd() override;

		void OnInputUpdate(float deltaTime, int mods) override;
		void OnKeyPress(int key, int mods) override;
		void OnKeyRelease(int key, int mods) override;
		void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
		void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) override;
		void OnWindowResize(int width, int height) override;

		void arrowUpdate(float deltaTimeSeconds);
		bool haveCollision(float x1, float y1, float r1, float x2, float y2, float r2);
		void checkCollisionArrow();
		void checkCollisionPlayer();
protected:
		glm::mat3 modelMatrix;

		int score = 0;
		bool redCollision = false;
		bool yellowCollision = false;
		bool shurikenCollision = false;
		bool playerCollision = false;
		bool isDead = false;
		bool increasePower = false;
		float power;
		bool subLife = false;
		float lifeSc = 1;
		int lives = 3;

		float angularStep = 1;
		float x, y;
		float shurikenSize = 5.5;
		float shurikenRadius = 3.7;
		float playerInitial = 50;
		int playerSpeed = 150;
		int playerPosition = 200;

		float shurikenX, shurikenY;
		int shurikenSpeed = 5;
		int shurikenStep = 100;
		float shurikenInit;
		
		int redBalloonStep = 70;
		int yellowBalloonStep = 60;

		int arrowLength = 18;
		int arrowHeight = 1.5;
		float arrowMove;
		float arrowStep = 600;
		float offsetTime = 1.5;
		float arrowCollisionY;
		float arrowCollisionX;

		float sceneY;
		float sceneX;

		float targetMoveX;
		float targetMoveY;
		bool isClicked = false;
		float alpha = 0;

		int maxNumberShurikens = 5;
		std::vector <Point> shurikens;

		int maxNumberBalloons = 3;
		std::vector <Point> balloons;

		int maxNumberYellowBalloons = 2;
		std::vector <Point> yellowBalloons;

		int numberArrows = 10;
		std::vector <Parameter> arrows;

};
