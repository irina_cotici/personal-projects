#include "Laborator3_Vis2D.h"

#include <vector>
#include <iostream>

#include <Core/Engine.h>
#include "Transform2D.h"
#include "Object2D.h"

using namespace std;
int x, y;

Laborator3_Vis2D::Laborator3_Vis2D()
{
}

Laborator3_Vis2D::~Laborator3_Vis2D()
{
}

void Laborator3_Vis2D::Init()
{
	auto camera = GetSceneCamera();
	glm::ivec2 resolution = window->GetResolution();
	camera->SetOrthographic(0, (float)resolution.x, 0, (float)resolution.y, 0.01f, 400);
	camera->SetPosition(glm::vec3(0, 0, 50));
	camera->SetRotation(glm::vec3(0, 0, 0));
	camera->Update();
	GetCameraInput()->SetActive(false);

	glm::vec3 origin = glm::vec3(5, 5, 5);
	length = 0.99f;

	Mesh* grassMargin = Object2D::CreateTriangle("grassMargin", origin, resolution.y / 10, glm::vec3(1, 0, 0.4), true);
	AddMeshToList(grassMargin);
}

// 2D visualization matrix
glm::mat3 Laborator3_Vis2D::VisualizationTransf2D(const LogicSpace & logicSpace, const ViewportSpace & viewSpace)
{
	float sx, sy, tx, ty;
	sx = viewSpace.width / logicSpace.width;
	sy = viewSpace.height / logicSpace.height;
	tx = viewSpace.x - sx * logicSpace.x;
	ty = viewSpace.y - sy * logicSpace.y;

	return glm::transpose(glm::mat3(
		sx, 0.0f, tx,
		0.0f, sy, ty,
		0.0f, 0.0f, 1.0f));
}

// uniform 2D visualization matrix (same scale factor on x and y axes)
glm::mat3 Laborator3_Vis2D::VisualizationTransf2DUnif(const LogicSpace & logicSpace, const ViewportSpace & viewSpace)
{
	float sx, sy, tx, ty, smin;
	sx = viewSpace.width / logicSpace.width;
	sy = viewSpace.height / logicSpace.height;
	if (sx < sy)
		smin = sx;
	else
		smin = sy;
	tx = viewSpace.x - smin * logicSpace.x + (viewSpace.width - smin * logicSpace.width) / 2;
	ty = viewSpace.y - smin * logicSpace.y + (viewSpace.height - smin * logicSpace.height) / 2;

	return glm::transpose(glm::mat3(
		smin, 0.0f, tx,
		0.0f, smin, ty,
		0.0f, 0.0f, 1.0f));
}

void Laborator3_Vis2D::SetViewportArea(const ViewportSpace & viewSpace, glm::vec3 colorColor, bool clear)
{
	glViewport(viewSpace.x, viewSpace.y, viewSpace.width, viewSpace.height);

	glEnable(GL_SCISSOR_TEST);
	glScissor(viewSpace.x, viewSpace.y, viewSpace.width, viewSpace.height);

	// Clears the color buffer (using the previously set color) and depth buffer
	glClearColor(colorColor.r, colorColor.g, colorColor.b, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_SCISSOR_TEST);

	GetSceneCamera()->SetOrthographic((float)viewSpace.x, (float)(viewSpace.x + viewSpace.width), (float)viewSpace.y, (float)(viewSpace.y + viewSpace.height), 0.1f, 400);
	GetSceneCamera()->Update();
}

void Laborator3_Vis2D::FrameStart()
{
	// Clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0.6, 1, 0.7, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glm::ivec2 resolution = window->GetResolution();
	glViewport(0, 0, resolution.x, resolution.y);
}

void Laborator3_Vis2D::Update(float deltaTimeSeconds)
{

	// Sets the screen area where to draw - the left half of the window
	/*viewSpace = ViewportSpace(0, 0, resolution.x / 2, resolution.y);
	SetViewportArea(viewSpace, glm::vec3(0), true);*/
	
	// Compute the 2D visualization matrix
	visMatrix = glm::mat3(1);
	//visMatrix *= VisualizationTransf2D(logicSpace, viewSpace);
	modelMatrix = Transform2D::Scale(10, 10) * modelMatrix;
	modelMatrix = Transform2D::Translate(100, 100) * modelMatrix;
	RenderMesh2D(meshes["grassMargin"], shaders["VertexColor"], modelMatrix);
	//DrawScene(visMatrix);

	// The viewport is now the right half of the window

	/*viewSpace = ViewportSpace(resolution.x / 2, 0, resolution.x / 2, resolution.y);
	SetViewportArea(viewSpace, glm::vec3(0.5f), true);*/

	// Compute uniform 2D visualization matrix

}

void Laborator3_Vis2D::FrameEnd()
{

}

void Laborator3_Vis2D::DrawScene(glm::mat3 visMatrix)
{
	
	modelMatrix = Transform2D::Scale(10, 10) * modelMatrix;
	modelMatrix = Transform2D::Translate(700, 300) * modelMatrix;
	RenderMesh2D(meshes["grassMargin"], shaders["VertexColor"], modelMatrix);

	/*modelMatrix = visMatrix * Transform2D::Translate(0, 0);
	RenderMesh2D(meshes["square1"], shaders["VertexColor"], modelMatrix);

	modelMatrix = visMatrix * Transform2D::Translate(3, 0);
	RenderMesh2D(meshes["square1"], shaders["VertexColor"], modelMatrix);

	modelMatrix = visMatrix * Transform2D::Translate(1.5, 1.5);
	RenderMesh2D(meshes["square1"], shaders["VertexColor"], modelMatrix);

	modelMatrix = visMatrix * Transform2D::Translate(0, 3);
	RenderMesh2D(meshes["square1"], shaders["VertexColor"], modelMatrix);

	modelMatrix = visMatrix * Transform2D::Translate(3, 3);
	RenderMesh2D(meshes["square1"], shaders["VertexColor"], modelMatrix);*/
}

void Laborator3_Vis2D::OnInputUpdate(float deltaTime, int mods)
{
	//TODO move the logic window with W, A, S, D (up, left, down, right)
	if (window->KeyHold(GLFW_KEY_A)) {
		logicSpace.x += 5*deltaTime;
	}
	else if (window->KeyHold(GLFW_KEY_D)) {
		logicSpace.x -= 5*deltaTime;
	}
	else if (window->KeyHold(GLFW_KEY_W)) {
		logicSpace.y -= 5*deltaTime;
	}
	else if (window->KeyHold(GLFW_KEY_S)) {
		logicSpace.y += 5*deltaTime;
	}
	else if (window->KeyHold(GLFW_KEY_Z)) {
		logicSpace.width += 5 * deltaTime;
		logicSpace.height += 5 * deltaTime;
	}
	else if (window->KeyHold(GLFW_KEY_X)) {
		logicSpace.width -= 5 * deltaTime;
		logicSpace.height -= 5 * deltaTime;
	}
	

	//TODO zoom in and zoom out logic window with Z and X
}

void Laborator3_Vis2D::OnKeyPress(int key, int mods)
{
}

void Laborator3_Vis2D::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Laborator3_Vis2D::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
}

void Laborator3_Vis2D::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
}

void Laborator3_Vis2D::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator3_Vis2D::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}