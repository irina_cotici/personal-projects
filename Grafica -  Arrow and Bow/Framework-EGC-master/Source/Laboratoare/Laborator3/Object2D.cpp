#include "Object2D.h"
#include <math.h>
#include <iostream>
#include <Core/Engine.h>
using namespace std;

Mesh* Object2D::CreateSquare(std::string name, glm::vec3 leftBottomCorner, float length, glm::vec3 color, bool fill)
{
	glm::vec3 corner = leftBottomCorner;

	std::vector<VertexFormat> vertices =
	{
		VertexFormat({2, 0, 1}, color),
		VertexFormat({4, 0, 1}, color),
		VertexFormat({2, 2, 1}, color)

	};

	Mesh* square = new Mesh(name);
	std::vector<unsigned short> indices = { 0, 1, 2, 2, 1, 0};
	
	square->InitFromData(vertices, indices);
	return square;
}

Mesh* Object2D::CreateTriangle(std::string name, glm::vec3 leftBottomCorner, float x, glm::vec3 color, bool fill) {

	glm::vec3 corner = leftBottomCorner;

	std::vector<VertexFormat> vertices = {
		VertexFormat({0,0,1}, color),
		VertexFormat({1,0,1}, color),
		VertexFormat({0,1,1}, color)
	};

	Mesh* triangle = new Mesh(name);
	std::vector<unsigned short> indices = { 0, 1, 2
	, 2, 1, 0};

	triangle->InitFromData(vertices, indices);
	return triangle;
}

Mesh* Object2D::CreatePhineas(std::string name) {
	glm::vec3 brown = glm::vec3(0.40, 0.20, 0);
	glm::vec3 blue = glm::vec3(0, 0.3, 0.58);
	glm::vec3 orange = glm::vec3(0.95, 0.59, 0);
	glm::vec3 hair = glm::vec3(0.86, 0.27, 0.01);
	glm::vec3 skin = glm::vec3(0.988, 0.886, 0.78);
	glm::vec3 grey = glm::vec3(0.75, 0.75, 0.75);



	std::vector<VertexFormat> vertices = {
			// left shoe
			VertexFormat({3, 0, 1}, brown),
			VertexFormat({3, 1, 1}, brown), 
			VertexFormat({2, 1, 1}, brown),

			// left leg
			VertexFormat({2, 1, 1}, blue),
			VertexFormat({3, 1, 1}, blue),
			VertexFormat({2, 5, 1}, blue),
			VertexFormat({3, 5, 1}, blue),

			// right shoe
			VertexFormat({5, 0, 1}, brown),
			VertexFormat({5, 1, 1}, brown),
			VertexFormat({4, 1, 1}, brown),

			// right leg
			VertexFormat({4, 1, 1}, blue),
			VertexFormat({5, 1, 1}, blue),
			VertexFormat({4, 5, 1}, blue),
			VertexFormat({5, 5, 1}, blue),

			// hand
			VertexFormat({3, 8, 1}, skin),
			VertexFormat({8, 8, 1}, skin),
			VertexFormat({3, 9, 1}, skin),
			VertexFormat({8, 9, 1}, skin),

			// head
			VertexFormat({2.5, 11, 1}, skin),
			VertexFormat({7, 13, 1}, skin),
			VertexFormat({1, 17, 1}, skin),

			// ear
			VertexFormat({1, 14, 1}, skin),
			VertexFormat({2.1, 13, 1}, skin),
			VertexFormat({2, 14.5, 1}, skin),

			// body
			VertexFormat({2, 5, 1}, orange),
			VertexFormat({2, 11, 1}, orange),
			VertexFormat({5, 5, 1}, orange),
			VertexFormat({5, 11, 1}, orange),

			// hair
			VertexFormat({0, 15, 1}, hair),
			VertexFormat({1.2, 18, 1}, hair),
			VertexFormat({3, 15.7, 1}, hair),
			VertexFormat({0, 17, 1}, hair),
			VertexFormat({4.2, 17, 1}, hair),
			VertexFormat({1.2, 15, 1}, hair),

			VertexFormat({8, 15.3, 1}, grey), 
			VertexFormat({8, 3, 1}, grey),
			VertexFormat({8.3, 15.3, 1}, grey),
			VertexFormat({8.3, 3, 1}, grey),


	};

	Mesh* player = new Mesh(name);
	std::vector<unsigned short> indices = {
		28, 30, 29,
		31, 33, 32,
		0, 1, 2,
		3, 4, 5,
		4, 6, 5,
		7, 8, 9,
		10, 11, 12,
		11, 13, 12,
		14, 15, 16,
		16, 15, 17,
		18, 19, 20,
		21, 22, 23,
		24, 26, 25,
		26, 27, 25,

		34, 35, 37,
		37, 36, 34
	};

	player->InitFromData(vertices, indices);
	return player;
}


Mesh* Object2D::CreateShuriken(std::string name) {
	glm::vec3 black = glm::vec3(0, 0, 0);
	glm::vec3 red = glm::vec3(0.75, 0.18, 0.05);

	std::vector<VertexFormat> vertices = {
		// cross
		VertexFormat({3, 5, 1}, black),
		VertexFormat({3, 6, 1}, black),
		VertexFormat({8, 5, 1}, black),
		VertexFormat({8, 6, 1}, black),

		VertexFormat({5, 8, 1}, black),
		VertexFormat({6, 8, 1}, black),
		VertexFormat({5, 3, 1}, black),
		VertexFormat({6, 3, 1}, black),

		// middle
		VertexFormat({4.1, 5.5, 1}, red),
		VertexFormat({5.5, 4.1, 1}, red),
		VertexFormat({5.5, 7.1, 1}, red),
		VertexFormat({7.1, 5.5, 1}, red),

		// up
		VertexFormat({4.3, 7.5, 1}, black),
		VertexFormat({6.7, 7.5, 1}, black),
		VertexFormat({5.5, 9.2, 1}, black),

		// down
		VertexFormat({4.3, 3.5, 1}, black),
		VertexFormat({6.7, 3.5, 1}, black),
		VertexFormat({5.5, 1.8, 1}, black),

		// right
		VertexFormat({7.5, 4.3, 1}, black),
		VertexFormat({7.5, 6.7, 1}, black),
		VertexFormat({9.2, 5.5, 1}, black),

		// left
		VertexFormat({3.5, 4.3, 1}, black),
		VertexFormat({3.5, 6.7, 1}, black),
		VertexFormat({1.8, 5.5, 1}, black),

	};

	Mesh* shuriken = new Mesh(name);
	std::vector<unsigned short> indices = {
		8, 9, 10,
		10, 9, 11,

		0, 2, 1,
		1, 3, 2,
		4, 6, 7,
		7, 5, 4,

		12, 13, 14,
		15, 17, 16,
		18, 20, 19,
		21, 22, 23
	};
	shuriken->InitFromData(vertices, indices);
	return shuriken;
}

Mesh* Object2D::CreateArrow(std::string name) {
	glm::vec3 brown = glm::vec3(0.48, 0.45, 0.45);
	glm::vec3 grey = glm::vec3(0.75, 0.75, 0.75);



	std::vector<VertexFormat> vertices = {
		VertexFormat({0, 1, 1}, brown),
		VertexFormat({14, 1, 1}, brown),
		VertexFormat({0, 2, 1}, brown),
		VertexFormat({14, 2, 1}, brown),

		VertexFormat({14, 0, 1}, grey),
		VertexFormat({14, 3, 1}, grey),
		VertexFormat({18, 1.5, 1}, grey),

		VertexFormat({1, 0, 1}, brown),
		VertexFormat({3, 1, 1}, brown),
		VertexFormat({2, 1, 1}, brown),

		VertexFormat({2, 2, 1}, brown),
		VertexFormat({3, 2, 1}, brown),
		VertexFormat({1, 3, 1}, brown),


	};

	Mesh* arrow = new Mesh(name);
	std::vector<unsigned short> indices = {
		0, 1, 2,
		1, 3, 2,
		4, 6, 5,
		7, 8, 9,
		10, 11, 12
	};
	arrow->InitFromData(vertices, indices);
	return arrow;
}


Mesh* Object2D::CreateBalloon(std::string name, glm::vec3 color) {

	const int sides = 40;  // The amount of segment to create the circle
	const double radius = 5; // The radius of the circle
	std::vector<VertexFormat> vertices = {};
	std::vector<unsigned short> indices = {};
	for (int a = 0; a < 360; a += 360 / sides)
	{
		double heading = a * 3.14 / 180;
		vertices.push_back(
			VertexFormat({cos(heading) * radius, sin(heading) * radius, 1 }, color)
		);
	}

	for (int i = 1; i < vertices.size(); i++) {
		if (i % 2) {
			indices.push_back(0);
			indices.push_back(i);
		}
		else if (i % 2 == 0) {
			indices.push_back(i);
		}
	}

	for (int i = 1; i < vertices.size(); i++) {
		if (i % 2 == 0) {
			indices.push_back(0);
			indices.push_back(i);
		}
		else if (i % 2) {
			indices.push_back(i);
		}
	}

	vertices.push_back(VertexFormat({0, -3, 1 }, color));
	vertices.push_back(VertexFormat({-1, -6, 1 }, color));
	vertices.push_back(VertexFormat({1, -6, 1 }, color));

	indices.push_back(vertices.size() - 1);
	indices.push_back(vertices.size() - 2);
	indices.push_back(vertices.size() - 3);


	///////////////////////////////////////////////////////////////////////////////////
	vertices.push_back(VertexFormat({ -0.2, -6, 1 }, glm::vec3(0, 0, 0))); //a -4
	vertices.push_back(VertexFormat({ -0.7, -7, 1 }, glm::vec3(0, 0, 0))); //b -3 -5
	vertices.push_back(VertexFormat({ 0.2, -6, 1 }, glm::vec3(0, 0, 0))); //c -2 -4
	vertices.push_back(VertexFormat({ -0.3, -7, 1 }, glm::vec3(0, 0, 0))); //d -1 -3

	indices.push_back(vertices.size() - 2);
	indices.push_back(vertices.size() - 4);
	indices.push_back(vertices.size() - 3);

	indices.push_back(vertices.size() - 3);
	indices.push_back(vertices.size() - 1);
	indices.push_back(vertices.size() - 2);
	////////////////////////////////////////////////////////////////////////////////////

	vertices.push_back(VertexFormat({ 1.2, -9, 1 }, glm::vec3(0, 0, 0))); //e -2 -4
	vertices.push_back(VertexFormat({ 0.8, -9, 1 }, glm::vec3(0, 0, 0))); //f -1 -3

	indices.push_back(vertices.size() - 5);
	indices.push_back(vertices.size() - 1);
	indices.push_back(vertices.size() - 2);

	indices.push_back(vertices.size() - 2);
	indices.push_back(vertices.size() - 3);
	indices.push_back(vertices.size() - 5);

	vertices.push_back(VertexFormat({ -0.7, -11, 1 }, glm::vec3(0, 0, 0))); //g -2
	vertices.push_back(VertexFormat({ -0.5, -11.2, 1 }, glm::vec3(0, 0, 0))); //h -1

	indices.push_back(vertices.size() - 3);
	indices.push_back(vertices.size() - 2);
	indices.push_back(vertices.size() - 4);

	indices.push_back(vertices.size() - 4);
	indices.push_back(vertices.size() - 2);
	indices.push_back(vertices.size() - 1);

	Mesh* balloon = new Mesh(name);
	balloon->InitFromData(vertices, indices);
	return balloon;
}

Mesh* Object2D::CreateBrokenBalloon(std::string name, glm::vec3 color) {

	const int sides = 40; 
	const double radius = 5; 
	std::vector<VertexFormat> vertices = {};
	std::vector<unsigned short> indices = {};

	for (int a = 0; a < 360; a += 360 / sides)
	{
		double heading = a * 3.14 / 180;
		vertices.push_back(
			VertexFormat({ cos(heading) * radius, sin(heading) * radius, 1 }, color)
		);
		for (int i = 0; i < vertices.size(); i++) {
			indices.push_back((unsigned short)i);
			//indices.push_back((unsigned short)i + a);
			indices.push_back((unsigned short)i);

			std::vector<unsigned short> indices = {};
		}
		indices.push_back((unsigned short)a);

		
	}

	Mesh* balloon = new Mesh(name);
	balloon->InitFromData(vertices, indices);
	return balloon;
}

Mesh* Object2D::CreateBow(std::string name) {

	const int sides = 40;  // The amount of segment to create the circle
	const double radius = 8; // The radius of the circle
	const double inner_radius = 6; // The radius of the circle

	std::vector<VertexFormat> vertices = {};
	std::vector<unsigned short> indices = {};
	glm::vec3 red = glm::vec3(0.75, 0.18, 0.05);
	glm::vec3 inner = glm::vec3(0.5, 0.7, 0.6);
	
	for (int a = 0; a < 360; a += 360 / sides)
	{
		double heading = a * 3.14 / 180;
		vertices.push_back(
			VertexFormat({ cos(heading) * radius, sin(heading) * radius, 1 }, red)
		);
	}

	for (int a = 0; a < 360; a += 360 / sides)
	{
		double heading = a * 3.14 / 180;
		vertices.push_back(
			VertexFormat({ cos(heading) * inner_radius + 1, sin(heading) * inner_radius, 1 }, inner)
		);
	}

	for (int i = vertices.size() / 2 + 1; i < vertices.size(); i++) {
		if (i % 2) {
			indices.push_back(vertices.size() / 2);
			indices.push_back(i);
		}
		else if (i % 2 == 0) {
			indices.push_back(i);
		}
	}

	for (int i = vertices.size() / 2 + 1; i < vertices.size(); i++) {
		if (i % 2 == 0) {
			indices.push_back(vertices.size() / 2);
			indices.push_back(i);
		}
		else if (i % 2) {
			indices.push_back(i);
		}
	}

	for (int i = 1; i < vertices.size() / 4; i++) {
		if (i % 2 == 0) {
			indices.push_back(0);
			indices.push_back(i);
		}
		else if (i % 2) {
			indices.push_back(i);
		}
	}

	for (int i = 1; i < vertices.size() / 4; i++) {
		if (i % 2) {
			indices.push_back(0);
			indices.push_back(i);
		}
		else if (i % 2 == 0) {
			indices.push_back(i);
		}
	}

	Mesh* bow = new Mesh(name);
	//bow->SetDrawMode(GL_LINE_LOOP);
	bow->InitFromData(vertices, indices);
	return bow;
}

Mesh* Object2D::CreateScene(std::string name) {
	glm::vec3 color = glm::vec3(0.28, 0.46, 0.32);
	glm::vec3 background = glm::vec3(0.15, 0.29, 0.18);

	std::vector<VertexFormat> vertices = { 
		VertexFormat({0, 0, 1}, color),
		VertexFormat({ 0, 80, 1 }, color),
		VertexFormat({ 1500, 0, 1 }, color),
		VertexFormat({ 1500, 80, 1 }, color),

		VertexFormat({ 20, 20, 1 }, background),
		VertexFormat({ 20, 30, 1 }, background),
		VertexFormat({ 600, 20, 1 }, background), 
		VertexFormat({ 600, 30, 1 }, background),

		VertexFormat({ 20, 50, 1 }, background),
		VertexFormat({ 20, 60, 1 }, background),
		VertexFormat({ 600, 50, 1 }, background),
		VertexFormat({ 600, 60, 1 }, background)
	};
	std::vector<unsigned short> indices = {

		5, 4, 6,
		6, 7, 5, 

		9, 8, 10,
		10, 11, 9,

		0, 2, 1,
		1, 2, 3

	};

	Mesh* bar = new Mesh(name);
	bar->InitFromData(vertices, indices);
	return bar;
}
Mesh* Object2D::CreatePowerBar(std::string name) {
	glm::vec3 background = glm::vec3(0.4, 0.74, 0.91);

	std::vector<VertexFormat> vertices = {
		VertexFormat({ 20, 50, 1 }, background),
		VertexFormat({ 20, 60, 1 }, background),
		VertexFormat({ 600, 50, 1 }, background),
		VertexFormat({ 600, 60, 1 }, background)

	};
	std::vector<unsigned short> indices = {

		0, 2, 1,
		1, 2, 3

	};

	Mesh* power = new Mesh(name);
	power->InitFromData(vertices, indices);
	return power;
}
Mesh* Object2D::CreateLifeBar(std::string name) {
	glm::vec3 color = glm::vec3(0.92, 0.16, 0.07);

	std::vector<VertexFormat> vertices = {
		VertexFormat({ 20, 20, 1 }, color),
		VertexFormat({ 20, 30, 1 }, color),
		VertexFormat({ 600, 20, 1 }, color),
		VertexFormat({ 600, 30, 1 }, color),
	};
	std::vector<unsigned short> indices = {

		0, 2, 1,
		1, 2, 3
	};

	Mesh* power = new Mesh(name);
	power->InitFromData(vertices, indices);
	return power;
}
