#include "Laborator3.h"

#include <vector>
#include <iostream>

#include <Core/Engine.h>
#include "Transform2D.h"
#include "Object2D.h"

using namespace std;
int sc;
int tr;
bool enoughShuriken;

Laborator3::Laborator3()
{
}

Laborator3::~Laborator3()
{
}

void Laborator3::Init()
{
	glm::ivec2 resolution = window->GetResolution();
	auto camera = GetSceneCamera();
	camera->SetOrthographic(0, (float)resolution.x, 0, (float)resolution.y, 0.01f, 400);
	camera->SetPosition(glm::vec3(0, 0, 50));
	camera->SetRotation(glm::vec3(0, 0, 0));
	camera->Update();
	GetCameraInput()->SetActive(false);

	sceneY = (float)resolution.y;
	sceneX = (float)resolution.x;


	shurikenX = (float)resolution.x + shurikenSize;
	shurikenY = 100 + shurikenSize;

	shurikens.push_back({ (int)sceneX + rand() % 1000, rand() % ((int)sceneY - 140) + 160, 1, false, false});

	balloons.push_back({ rand() % ((int)sceneX - 50) + 220,  -(rand() % 1000 + (int)sceneY), 1, false});

	yellowBalloons.push_back({ rand() % ((int)sceneX - 50) + 220, -(rand() % 1000 + (int)sceneY), 1, false});

	arrows.push_back({0, 0, 0.0f});

	arrowMove = 0;

	targetMoveX = (int)playerInitial + 100;
	targetMoveY = (int)playerPosition + 107;

	power = 0.1;
	lifeSc = 1;

	Mesh* player = Object2D::CreatePhineas("player");
	AddMeshToList(player);

	Mesh* shuriken = Object2D::CreateShuriken("shuriken");
	AddMeshToList(shuriken);

	Mesh* arrow = Object2D::CreateArrow("arrow");
	AddMeshToList(arrow);
	Mesh* arrow1 = Object2D::CreateArrow("arrow1");
	AddMeshToList(arrow1);

	glm::vec3 red = glm::vec3(0.75, 0.18, 0.05);
	Mesh* redBalloon = Object2D::CreateBalloon("red_balloon", red);
	AddMeshToList(redBalloon);

	glm::vec3 yellow = glm::vec3(1, 0.76, 0);
	Mesh* yellowBalloon = Object2D::CreateBalloon("yellow_balloon", yellow);
	AddMeshToList(yellowBalloon);

	Mesh* broken_balloon = Object2D::CreateBrokenBalloon("balloon_broken", red);
	AddMeshToList(broken_balloon);

	Mesh* broken_balloon1 = Object2D::CreateBrokenBalloon("balloon_broken1", yellow);
	AddMeshToList(broken_balloon1);

	Mesh* bow = Object2D::CreateBow("bow");
	AddMeshToList(bow);

	Mesh* bar = Object2D::CreateScene("bar");
	AddMeshToList(bar);

	Mesh* life = Object2D::CreateLifeBar("life");
	AddMeshToList(life);

	Mesh* power = Object2D::CreatePowerBar("power");
	AddMeshToList(power);
}

void Laborator3::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0.5, 0.7, 0.6, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}

void Laborator3::arrowUpdate(float deltaTime) {
	for (int i = 0; i < arrows.size(); i++) {
		arrows[i].x += cos(arrows[i].angle) * arrowStep * deltaTime;
		arrows[i].y += sin(arrows[i].angle) * arrowStep * deltaTime;
	}
}

bool Laborator3::haveCollision(float x1, float y1, float r1, float x2, float y2, float r2) {
	float dx = x1 - x2;
	float dy = y1 - y2;
	float distance = sqrt(dx * dx + dy * dy);
	if (distance < r1 + r2) {
		return true;
	}
	return false;
}

void Laborator3::checkCollisionArrow() {
	for (int i = 0; i < arrows.size(); i++) {
		arrowCollisionY = sin(arrows[i].angle) * arrowLength * 8 + arrowHeight * 2 + arrows[i].y;
		arrowCollisionX = cos(arrows[i].angle) * arrowLength * 8 + arrows[i].x;
		float r1 = 9 * 2;
		for (int j = 0; j < balloons.size(); j++) {

			float balloonCollisionX = balloons[j].x;
			float balloonCollisionY = balloons[j].y;
			float r2 = 5 * 5;

			redCollision = false;
			if (haveCollision(arrowCollisionX, arrowCollisionY, r1, balloonCollisionX, balloonCollisionY, r2) &&
				!balloons[j].dead) {
				balloons[j].dead = true;

				score += 10;
				cout << "YOUR SCORE: " << score << "\n";
			}
		}
		for (int k = 0; k < yellowBalloons.size(); k++) {
			float balloonCollisionX = yellowBalloons[k].x;
			float balloonCollisionY = yellowBalloons[k].y; 
			float r2 = 5 * 5;

			if (haveCollision(arrowCollisionX, arrowCollisionY, r1, balloonCollisionX, balloonCollisionY, r2) &&
				!yellowBalloons[k].dead) {
				yellowBalloons[k].dead = true;
				if (score > 0) {
					score -= 10;
				}
			}
		}
		for (int w = 0; w < shurikens.size(); w++) {
			float shurikemCollisionX = shurikens[w].x;
			float shurikemCollisionY = shurikens[w].y;
			float r2 = shurikenRadius * 7;

			if (haveCollision(arrowCollisionX, arrowCollisionY, r1, shurikemCollisionX, shurikemCollisionY, r2) &&
				!shurikens[w].dead) {
				shurikens[w].dead = true;
			}
		}
	}
}

void Laborator3::checkCollisionPlayer() {
	for (int i = 0; i < shurikens.size(); i++) {
		float shurikemCollisionX = shurikens[i].x + shurikenSize * 7;
		float shurikemCollisionY = shurikens[i].y + shurikenSize * 7;
		float r2 = shurikenSize * 7;

		float r1 = 8 * 10;
		float bowCollisionX = playerInitial + 100 + r1;
		float bowCollisionY = playerPosition + 128;
	

		if (haveCollision(shurikemCollisionX, shurikemCollisionY, r2, bowCollisionX, bowCollisionY, r1) &&
			!shurikens[i].dead) {
			shurikens[i].dead = true;
			if (lifeSc > 0 && lives > 0) {
				lifeSc -= 0.33;
				lives--;
				cout << "You have " << lives << " lives remained!" << "\n";
			}

			if (lives == 0) {
				window->Close();
				cout << "GAME OVER"<< "\n";
				cout << "YOUR SCORE: " << score << "\n";
			}
		}
	}
}
void Laborator3::Update(float deltaTimeSeconds)
{	
	// ==================================== SCENE ========================================================= //
	if (!increasePower) {
		power = 0.1;
	}
	else if (increasePower && power < 1) {
		power += deltaTimeSeconds * 0.5;
		arrowStep += deltaTimeSeconds * 500;
	}
	modelMatrix = glm::mat3(1);
	modelMatrix = Transform2D::Translate(-20, -60) * modelMatrix;
	modelMatrix = Transform2D::Scale(power, 1) * modelMatrix;
	modelMatrix = Transform2D::Translate(20, 60) * modelMatrix;
	RenderMesh2D(meshes["power"], shaders["VertexColor"], modelMatrix);

	modelMatrix = glm::mat3(1);
	modelMatrix = Transform2D::Translate(-20, -30) * modelMatrix;
	modelMatrix = Transform2D::Scale(lifeSc, 1) * modelMatrix;


	modelMatrix = Transform2D::Translate(20, 30) * modelMatrix;
	RenderMesh2D(meshes["life"], shaders["VertexColor"], modelMatrix);

	modelMatrix = glm::mat3(1);
	RenderMesh2D(meshes["bar"], shaders["VertexColor"], modelMatrix);
	
	// ==================================== Player ========================================================= //

	checkCollisionPlayer();
	modelMatrix = glm::mat3(1);
	modelMatrix = Transform2D::Scale(13, 13) * modelMatrix;
	modelMatrix = Transform2D::Translate(playerInitial, playerPosition) * modelMatrix;
	RenderMesh2D(meshes["player"], shaders["VertexColor"], modelMatrix);

	// ==================================== Shurikens ========================================================= //

	angularStep += shurikenSpeed * deltaTimeSeconds;

	for (int i = 0; i < shurikens.size(); i++) {

		shurikens[i].x -= shurikenStep * deltaTimeSeconds;

		if (shurikens.size() < maxNumberShurikens) {
			shurikens.push_back({(int)sceneX  + rand() % 1000, rand() % ((int)sceneY - 160) + 140, 1, false, false});
		}

		modelMatrix = glm::mat3(1);
		modelMatrix = Transform2D::Translate(-shurikenSize, -shurikenSize) * modelMatrix;

		if (shurikens[i].dead) {
			shurikens[i].scale -= 2 * deltaTimeSeconds;
			modelMatrix = Transform2D::Scale(7 * shurikens[i].scale, 7 * shurikens[i].scale) * modelMatrix;
		}
		else {
			modelMatrix = Transform2D::Scale(7, 7) * modelMatrix;
		}

		if (shurikens[i].x + 100 < 0 || (shurikens[i].dead && shurikens[i].scale <= 0)) {
			shurikens[i].x = (int)sceneX + rand() % 1000;
			shurikens[i].y = rand() % ((int)sceneY - 160) + 140;
			shurikens[i].dead = false;
			shurikens[i].scale = 1;
		}
		modelMatrix = Transform2D::Rotate(angularStep) * modelMatrix;
		modelMatrix = Transform2D::Translate(shurikens[i].x, shurikens[i].y) * modelMatrix;
		RenderMesh2D(meshes["shuriken"], shaders["VertexColor"], modelMatrix);
	}	

	// ==================================== Arrow ========================================================= //
	if (offsetTime <= 0) {
		modelMatrix = glm::mat3(1);
		modelMatrix = Transform2D::Translate(0, -1) * modelMatrix;
		modelMatrix = Transform2D::Scale(8, 2) * modelMatrix;
		modelMatrix = Transform2D::Rotate(arrowMove) * modelMatrix;
		modelMatrix = Transform2D::Translate((int)playerInitial + 100, (int)playerPosition + 107) * modelMatrix;
		RenderMesh2D(meshes["arrow1"], shaders["VertexColor"], modelMatrix);
	}
	else {
		offsetTime -= deltaTimeSeconds;
	}

	checkCollisionArrow();
	arrowUpdate(deltaTimeSeconds);
	
	for (int i = 0; i < arrows.size(); i++) {
			modelMatrix = glm::mat3(1);
			modelMatrix = Transform2D::Translate(0, -1) * modelMatrix;
			modelMatrix = Transform2D::Scale(8, 2) * modelMatrix;
			modelMatrix = Transform2D::Rotate(arrows[i].angle) * modelMatrix;
			modelMatrix = Transform2D::Translate(arrows[i].x, arrows[i].y) * modelMatrix;
			RenderMesh2D(meshes["arrow"], shaders["VertexColor"], modelMatrix);

	}
	// ==================================== Red Balloon ========================================================= //
	for (int i = 0; i < balloons.size(); i++) {

		if (balloons.size() < maxNumberBalloons) {
			balloons.push_back({ rand() % ((int)sceneX - 50) + 220, -(rand() % 1000 + (int)sceneY), 1, false });
		}
		
		if (balloons[i].y > (int)sceneY + 50 || (balloons[i].dead && balloons[i].scale <= 0)) {
			balloons[i].x = rand() % ((int)sceneX - 270) + 220;
			balloons[i].y = -(rand() % 1000 + (int)sceneY);
			balloons[i].dead = false;
			balloons[i].scale = 1;
		}


		modelMatrix = glm::mat3(1);
		if (balloons[i].dead) {
			balloons[i].y -= 2 * redBalloonStep * deltaTimeSeconds;
			modelMatrix = Transform2D::Scale(10 * balloons[i].scale, 10 * balloons[i].scale) * modelMatrix;
			modelMatrix = Transform2D::Translate(balloons[i].x, balloons[i].y) * modelMatrix; 
			RenderMesh2D(meshes["balloon_broken"], shaders["VertexColor"], modelMatrix);
		}
		else {
			balloons[i].y += redBalloonStep * deltaTimeSeconds;
			modelMatrix = Transform2D::Scale(5, 8) * modelMatrix;
			modelMatrix = Transform2D::Translate(balloons[i].x, balloons[i].y) * modelMatrix;
			RenderMesh2D(meshes["red_balloon"], shaders["VertexColor"], modelMatrix);
		}
	

	}
	// ==================================== Yellow Balloon ========================================================= //
	for (int i = 0; i < yellowBalloons.size(); i++) {
		yellowBalloons[i].y += redBalloonStep * deltaTimeSeconds;

		if (yellowBalloons.size() < maxNumberYellowBalloons) {
			yellowBalloons.push_back({rand() % ((int)sceneX - 50) + 220, -(rand() % 1000 + (int)sceneY), 1, false});
		}
		modelMatrix = glm::mat3(1);
		if (yellowBalloons[i].y > (int)sceneY + 50 || (yellowBalloons[i].dead && yellowBalloons[i].scale <= 0)) {

			if (yellowBalloons[i].dead) {
				if (score > 0) {
					score -= 10;
				}
				cout << "YOUR SCORE: " << score << "\n";
			}
			yellowBalloons[i].x = rand() % ((int)sceneX - 270) + 220;
			yellowBalloons[i].y = -(rand() % 1000 + (int)sceneY);
			yellowBalloons[i].dead = false;
			yellowBalloons[i].scale = 1;
		}
		if (yellowBalloons[i].dead) {
			yellowBalloons[i].scale -= 3 * deltaTimeSeconds;
			modelMatrix = Transform2D::Scale(yellowBalloons[i].scale*5, yellowBalloons[i].scale*8) * modelMatrix;
			modelMatrix = Transform2D::Translate(yellowBalloons[i].x, yellowBalloons[i].y) * modelMatrix;
			RenderMesh2D(meshes["balloon_broken1"], shaders["VertexColor"], modelMatrix);

		}
		else {
			modelMatrix = Transform2D::Scale(5, 8) * modelMatrix;
			modelMatrix = Transform2D::Translate(yellowBalloons[i].x, yellowBalloons[i].y) * modelMatrix;
			RenderMesh2D(meshes["yellow_balloon"], shaders["VertexColor"], modelMatrix);
		}
	}

	// ==================================== Bow ========================================================= //
	modelMatrix = glm::mat3(1);
	modelMatrix = Transform2D::Scale(10, 10) * modelMatrix;
	modelMatrix = Transform2D::Rotate(-1.5) * modelMatrix;
	modelMatrix = Transform2D::Translate(playerInitial + 100, playerPosition + 120) * modelMatrix;
	RenderMesh2D(meshes["bow"], shaders["VertexColor"], modelMatrix);
}


void Laborator3::FrameEnd()
{

}

void Laborator3::OnInputUpdate(float deltaTime, int mods)
{
	if (window->KeyHold(GLFW_KEY_W)) {
		if (playerPosition + 13*18 < sceneY) {
			playerPosition += playerSpeed * deltaTime;
		}
	}
	else if (window->KeyHold(GLFW_KEY_S)) {
		if (playerPosition > 80) {
			playerPosition -= playerSpeed * deltaTime;
		}
	}
}

void Laborator3::OnKeyPress(int key, int mods)
{
	// add key press event
}

void Laborator3::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Laborator3::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
	float moveY = (sceneY - mouseY - playerPosition - 106);
	float moveX = (mouseX - playerInitial - 100);
	arrowMove = atan(moveY / moveX);
}

void Laborator3::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
	if (button == GLFW_MOUSE_BUTTON_2) {

		if (offsetTime <= 0) {
			isClicked = true;
			increasePower = true;
			arrowStep = 600;
		}
	}
}

void Laborator3::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{	

	if (button == GLFW_MOUSE_BUTTON_2) {
		isClicked = false;
		increasePower = false;

		if (offsetTime <= 0) {
			offsetTime = 1.5;
			targetMoveX = (int)playerInitial + 100;
			targetMoveY = (int)playerPosition + 107;

			arrows.push_back({ targetMoveX, targetMoveY, arrowMove });
		}
	}
	// add mouse button release event
}

void Laborator3::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Laborator3::OnWindowResize(int width, int height)
{
}
