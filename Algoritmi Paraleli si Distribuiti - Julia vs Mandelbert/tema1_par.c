/*
 * APD - Tema 1
 * Octombrie 2020
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
#define min(a,b) (((a)<(b))?(a):(b))

char *in_filename_julia;
char *in_filename_mandelbrot;
char *out_filename_julia;
char *out_filename_mandelbrot;

// structura pentru un numar complex
typedef struct _complex {
	double a;
	double b;
} complex;

// structura pentru parametrii unei rulari
typedef struct _params {
	int is_julia, iterations;
	double x_min, x_max, y_min, y_max, resolution;
	complex c_julia;
} params;

pthread_barrier_t barrier1;

pthread_mutex_t m;
int P;

int **result1;
int **result2;

int height1, width1;
int height2, width2;
params par1, par2;

// citeste argumentele programului
void get_args(int argc, char **argv)
{
	if (argc < 6) {
		printf("Numar insuficient de parametri:\n\t"
				"./tema1 fisier_intrare_julia fisier_iesire_julia "
				"fisier_intrare_mandelbrot fisier_iesire_mandelbrot\n");
		exit(1);
	}

	in_filename_julia = argv[1];
	out_filename_julia = argv[2];
	in_filename_mandelbrot = argv[3];
	out_filename_mandelbrot = argv[4];
	P = atoi(argv[5]);
}

// citeste fisierul de intrare
void read_input_file(char *in_filename, params* par)
{
	FILE *file = fopen(in_filename, "r");
	if (file == NULL) {
		printf("Eroare la deschiderea fisierului de intrare!\n");
		exit(1);
	}

	fscanf(file, "%d", &par->is_julia);
	fscanf(file, "%lf %lf %lf %lf",
			&par->x_min, &par->x_max, &par->y_min, &par->y_max);
	fscanf(file, "%lf", &par->resolution);
	fscanf(file, "%d", &par->iterations);

	if (par->is_julia) {
		fscanf(file, "%lf %lf", &par->c_julia.a, &par->c_julia.b);
	}

	fclose(file);
}

void *thread_function(void *arg) {
	long thread_id = *(long*) arg;
	int start = thread_id * width1 / P;
  	int end = min((thread_id + 1) * width1 / P, width1);
  	int w, h, i;

  	// algoritmul Julia paralelizat
  	for (w = start; w < end; w++) {
		for (h = 0; h < height1; h++) {
			int step = 0;
			complex z = { .a = w * par1.resolution + par1.x_min,
							.b = h * par1.resolution + par1.y_min };

			while (sqrt(pow(z.a, 2.0) + pow(z.b, 2.0)) < 2.0 && step < par1.iterations) {
				complex z_aux = { .a = z.a, .b = z.b };

				z.a = pow(z_aux.a, 2) - pow(z_aux.b, 2) + par1.c_julia.a;
				z.b = 2 * z_aux.a * z_aux.b + par1.c_julia.b;

				step++;
			}

			result1[h][w] = step % 256;
		}
	}

	// algoritmul Mandelbrot paralelizat
	start = thread_id * width2 / P;
  	end = min((thread_id + 1) * width2 / P, width2);
	for (w = start; w < end; w++) {
		for (h = 0; h < height2; h++) {
			complex c = { .a = w * par2.resolution + par2.x_min,
							.b = h * par2.resolution + par2.y_min };
			complex z = { .a = 0, .b = 0 };
			int step = 0;

			while (sqrt(pow(z.a, 2.0) + pow(z.b, 2.0)) < 2.0 && step < par2.iterations) {
				complex z_aux = { .a = z.a, .b = z.b };

				z.a = pow(z_aux.a, 2.0) - pow(z_aux.b, 2.0) + c.a;
				z.b = 2.0 * z_aux.a * z_aux.b + c.b;

				step++;
			}

			result2[h][w] = step % 256;
		}
	}

	// transforma rezultatul din coordonate matematice in coordonate ecran
  	pthread_barrier_wait(&barrier1);
	start = thread_id * (height1/2) / P;
  	end = min((thread_id + 1) * (height1/2) / P, (height1/2));
	for (i = start; i < end; i++) {
		int *aux1 = result1[i];
		result1[i] = result1[height1 - i - 1];
		result1[height1 - i - 1] = aux1;
	}

	start = thread_id * (height2/2) / P;
  	end = min((thread_id + 1) * (height2/2) / P, (height2/2));
	for (i = start; i < end; i++) {
		int *aux2 = result2[i];
		result2[i] = result2[height2 - i - 1];
		result2[height2 - i - 1] = aux2;
	}

	pthread_exit(NULL);
}

// scrie rezultatul in fisierul de iesire
void write_output_file(char *out_filename, int **result, int width, int height)
{
	int i, j;

	FILE *file = fopen(out_filename, "w");
	if (file == NULL) {
		printf("Eroare la deschiderea fisierului de iesire!\n");
		return;
	}

	fprintf(file, "P2\n%d %d\n255\n", width, height);
	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			fprintf(file, "%d ", result[i][j]);
		}
		fprintf(file, "\n");
	}

	fclose(file);
}

// aloca memorie pentru rezultat
int **allocate_memory(int width, int height)
{
	int **result;
	int i;

	result = malloc(height * sizeof(int*));
	if (result == NULL) {
		printf("Eroare la malloc!\n");
		exit(1);
	}

	for (i = 0; i < height; i++) {
		result[i] = malloc(width * sizeof(int));
		if (result[i] == NULL) {
			printf("Eroare la malloc!\n");
			exit(1);
		}
	}

	return result;
}

// elibereaza memoria alocata
void free_memory(int **result, int height)
{
	int i;

	for (i = 0; i < height; i++) {
		free(result[i]);
	}
	free(result);
}

int main(int argc, char *argv[])
{
	// se citesc argumentele programului
	get_args(argc, argv);

    long arguments[P];
    pthread_t threads[P];
    void *status;
    long id;
    int r;
	
	read_input_file(in_filename_julia, &par1);
	width1 = (par1.x_max - par1.x_min) / par1.resolution;
	height1 = (par1.y_max - par1.y_min) / par1.resolution;
	result1 = allocate_memory(width1, height1);
	
	read_input_file(in_filename_mandelbrot, &par2);
	width2 = (par2.x_max - par2.x_min) / par2.resolution;
	height2 = (par2.y_max - par2.y_min) / par2.resolution;
	result2 = allocate_memory(width2, height2);

    if (pthread_barrier_init(&barrier1, NULL, P) != 0) {
		printf("Eroare la crearea barierei-ului.\n"); 
        return 1; 
	}
	for (id = 0; id < P; id++) {
  		arguments[id] = id;
		r = pthread_create(&threads[id], NULL, thread_function, &arguments[id]);

		if (r) {
	  		printf("Eroare la crearea thread-ului %ld\n", id);
	  		exit(-1);
		}
  	}
  	
	for (id = 0; id < P; id++) {
		r = pthread_join(threads[id], &status);
		if (r) {
	  		printf("Eroare la asteptarea thread-ului %ld\n", id);
	  		exit(-1);
		}
  	}
	write_output_file(out_filename_julia, result1, width1, height1);
	write_output_file(out_filename_mandelbrot, result2, width2, height2);
	free_memory(result1, height1);
	free_memory(result2, height2);

	pthread_barrier_destroy(&barrier1);
	return 0;
}
