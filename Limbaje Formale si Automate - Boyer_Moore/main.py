import string
import sys
import numpy as mat

#lista caracterelor valide pentru input
alp = list(string.ascii_uppercase) 

#functie de verificare a restrictiilor
#de input
def checkStrings(string1, string2):
	if (string1.isupper() 
		and string1.isalpha()
		and string2.isupper() 
		and string2.isalpha()
		and len(string1) < 40
		and len(string2) < 15000000 ):
		return True
	else:
		return False

#functie de completare a matricei delta
def createDelta(string1, delta):
	#initializarea sirului vid pentru primul caracter	
	delta[0][alp.index(string1[0])] = 1
	step = 1
	this_state = 0

	for i in range(1, len(string1) + 1):
		for j in range (0, 26):
			#copierea pe coloane a starii anterioare
			delta[i][j] = delta[this_state][j]

		step += 1

		if (i != len(string1)):
			#setarea starii in care s-a ajuns cu caracterul de pe pos i
			delta[i][alp.index(string1[i])] = step
			#actualizarea starii ce contine cel mai prefix al sufixului
			this_state = int(delta[this_state][alp.index(string1[i])])

def main():

	#citirea inputului
	f = open(sys.argv[1], "r")
	string1 = f.readline().rstrip("\n")
	string2 = f.readline().rstrip("\n")
	
	#verificarea inputului citit
	if (not checkStrings):
		exit(-1)	

	#initializarea matricei si completarea ei
	delta = mat.zeros(((len(string1) + 1), 26))
	createDelta(string1, delta)

	state = 0
	out = []
	#parsarea pattern-ului prin matrice
	for i in range(0, len(string2)):
		#identificarea starilor
		state = int(delta[state][alp.index(string2[i])])
		if (state == len(string1)):
			out.append(str(i + 1 - len(string1)))
	
	#salvarea indicilor
	out.append('\n')
	out = ' '.join(map(str, out))

	#scrierea in fisierul de iesire
	g = open(sys.argv[2], "w")
	g.write(out)

	f.close()
	g.close()


if __name__ == '__main__':
	main()